//
//  RegistrationViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 28/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GetRecipeViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <QuartzCore/QuartzCore.h>
#import <GooglePlus/GooglePlus.h>
@interface RegistrationViewController : UIViewController<UITextFieldDelegate,NSXMLParserDelegate,UIAlertViewDelegate,GPPSignInDelegate>
{
    NSMutableData *webData;
    NSXMLParser *xmlParser;
    NSMutableString *soapResults;
    BOOL *recordResults;
    CGPoint scroll;
    GPPSignIn *signIn;
}
@property(nonatomic,strong)NSString *strGooglePlusId;
@property(nonatomic,strong)NSString *strFacebookId;
@property(nonatomic,strong)NSString *strTwitterId;
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (strong, nonatomic) IBOutlet UITextField *mTxtName;
@property (strong, nonatomic) IBOutlet UITextField *mTxtEmailid;
@property (strong, nonatomic) IBOutlet UITextField *mTxtPassword;
@property (strong, nonatomic) IBOutlet UITextField *mTxtRepeatPassword;
@property(nonatomic, retain) NSXMLParser *xmlParser;
@property(nonatomic, retain) NSMutableString *soapResults;
@property (strong, nonatomic) IBOutlet UIScrollView *mScrollRegi;



- (IBAction)btnRegiSubmit:(id)sender;
- (IBAction)btnBacktoStart:(id)sender;
- (IBAction)btnWithoutRegistration:(id)sender;

- (IBAction)btnFacebookClk:(id)sender;
- (IBAction)btnTwitterClk:(id)sender;
- (IBAction)btnGooglePlus:(id)sender;
@end
