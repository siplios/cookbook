//
//  FormViewController.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 14/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "FormViewController.h"
#import "AppDelegate.h"
AppDelegate *appdel;

@interface FormViewController ()

@end

@implementation FormViewController
@synthesize mlblUsernam;
@synthesize mPeopleAry;
@synthesize view1;
@synthesize mypicker;
@synthesize btnTitlePeople;
@synthesize mTimeAry;
@synthesize whichBtn;
@synthesize mTitleTimeBtn;
@synthesize btnSelect1;
@synthesize btnSelect2;
@synthesize btnSelect3;
@synthesize btnSelect4;
@synthesize btnSelect5;
@synthesize btnSelect6;
@synthesize btnCrossKids;
@synthesize btnYesKids;
@synthesize btnNoDishPrepare;
@synthesize btnYesDisPrepare;
@synthesize btnMon;
@synthesize btnTues;
@synthesize btnWed;
@synthesize btnThurs;
@synthesize btnFri;
@synthesize btnSat;
@synthesize btnSun;
@synthesize btnAll;
@synthesize xmlParser;
@synthesize soapResults;
@synthesize btnTitleAssistant;
@synthesize mUserImage;
@synthesize mImgPicNotSet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appdel=[[UIApplication sharedApplication] delegate];
    mImgPicNotSet.hidden=TRUE;
   
    self.scrollView.contentSize=CGSizeMake(320, 590);
    
    if([appdel.mLoginUserName length]==0)
    {
        mlblUsernam.text=[NSString stringWithFormat:@"Bienvenido invitado"];
        UIImage *image = [UIImage imageNamed: @"pic.png"];
        [mUserImage setImage:image];

    }
    else
    {
        mlblUsernam.text=[NSString stringWithFormat:@"Bienvenido %@",appdel.mLoginUserName];
        if([appdel.mImagePath isEqualToString:@""])
        {
            NSLog(@"yes nil");
            UIImage *image = [UIImage imageNamed:@"pic.png"];
            // [mUserImg setImage:image];
            mUserImage.image = image;
            mImgPicNotSet.hidden=FALSE;
        }
        //  mImgPicNotSet.hidden=TRUE;
        
        
        UIImage *graphImage = [[UIImage alloc] initWithContentsOfFile: appdel.mImagePath];
        mUserImage.image = graphImage;
    }
    [btnTitleAssistant setImage:[UIImage imageNamed:@"help_disable.png"] forState:UIControlStateNormal];
    
	// Do any additional setup after loading the view.
    mPeopleAry=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"Mas de 12", nil];
    mTimeAry=[[NSMutableArray alloc] initWithObjects:@"0.00 hs.",@"0.30 hs.",@"1.00 hs.",@"1.30 hs.",@"2.00 hs.",@"2.30 hs.",@"3.00 hs.",@"3.30 hs.",@"4.00 hs.",@"4.30 hs.",@"5.00 hs.",@"5.30 hs.",@"6.00 hs.",@"6.30 hs.",@"7.00 hs.",@"7.30 hs.",@"8.00 hs.",@"8.30 hs.",@"9.00 hs.",@"9.30 hs.",@"10.00 hs.",@"10.30 hs.",@"11.00 hs.",@"11.30 hs.",@"12.00 hs.",@"12.30 hs.",@"13.00 hs.",@"13.30 hs.",@"14.00 hs.",@"14.30 hs.",@"15.00 hs.",@"15.30 hs.",@"16.00 hs.",@"16.30 hs.",@"17.00 hs.",@"17.30 hs.",@"18.00 hs.",@"18.30 hs.",@"19.00 hs.",@"19.30 hs.",@"20.00 hs.",@"20.30 hs.",@"21.00 hs.",@"21.30 hs.",@"22.00 hs.",@"22.30 hs.",@"23.00 hs.",@"23.30 hs.", nil];
//    btnVal1=0;
//    btnVal2=0;
//    btnVal3=0;
//    btnVal4=0;
//    btnVal5=0;
//    btnVal6=0;
//    yesKids=0;
//    noKids=0;
    
    [self methodSelectFromTable];
    
    
    NSLog(@"btnVal1=%d,,btnVal2=%d,,btnVal3=%d,,btnVal4=%d,,btnVal5=%d,,btnVal6=%d,,childrenRecipe=%d,,easyRecipe=%d,moderateRecipe=%d,complexRecipe=%d,,mon=%d,,tue=%d,,wed=%d,,thu=%d,,fri=%d,,sat=%d,,sun=%d,dateTime=%@,howManyEating=%d",btnVal1,btnVal2,btnVal3,btnVal4,btnVal5,btnVal6,childrenRecipe,easyRecipe,moderateRecipe,complexRecipe,mon,tue,wed,thu,fri,sat,sun,dateTime,howManyEating);
    
    
    if(btnVal1==1)
    {
        [btnSelect1 setImage:[UIImage imageNamed:@"beef_active_ic.png"] forState:UIControlStateNormal];
        btnVal1=1;
        beefRecipe=1;
    }
    else if(btnVal1==0)
    {
        [btnSelect1 setImage:[UIImage imageNamed:@"beef_ic.png"] forState:UIControlStateNormal];
        btnVal1=0;
        beefRecipe=0;
    }
    if(btnVal2==1)
    {
        [btnSelect2 setImage:[UIImage imageNamed:@"pork_active_ic.png"] forState:UIControlStateNormal];
        btnVal2=1;
        porkRecipe=1;
    }
    else if(btnVal2==0)
    {
        [btnSelect2 setImage:[UIImage imageNamed:@"pork_ic.png"] forState:UIControlStateNormal];
        btnVal2=0;
        porkRecipe=0;
    }
    if(btnVal3==1)
    {
        [btnSelect3 setImage:[UIImage imageNamed:@"pollo_active_ic.png"] forState:UIControlStateNormal];
        btnVal3=1;
        chickenRecipe=1;
    }
    else if(btnVal3==0)
    {
        [btnSelect3 setImage:[UIImage imageNamed:@"pollo_ic.png"] forState:UIControlStateNormal];
        btnVal3=0;
        chickenRecipe=0;
    }
    if(btnVal4==1)
    {
        [btnSelect4 setImage:[UIImage imageNamed:@"fish_new_active_ic.png"] forState:UIControlStateNormal];
        btnVal4=1;
        fishRecipe=1;
    }
    else if(btnVal4==0)
    {
        [btnSelect4 setImage:[UIImage imageNamed:@"fish_new_ic.png"] forState:UIControlStateNormal];
        btnVal4=0;
        fishRecipe=0;
    }
    if(btnVal5==1)
    {
        [btnSelect5 setImage:[UIImage imageNamed:@"pasta_active_ic.png"] forState:UIControlStateNormal];
        btnVal5=1;
        pastaRecipe=1;
    }
    else if(btnVal5==0)
    {
        [btnSelect5 setImage:[UIImage imageNamed:@"pasta_ic.png"] forState:UIControlStateNormal];
        btnVal5=0;
        pastaRecipe=0;
    }
    if(btnVal6==1)
    {
        [btnSelect6 setImage:[UIImage imageNamed:@"veg_active_ic.png"] forState:UIControlStateNormal];
        btnVal6=1;
        vegRecipe=1;
    }
    else if(btnVal6==0)
    {
        [btnSelect6 setImage:[UIImage imageNamed:@"veg_ic.png"] forState:UIControlStateNormal];
        btnVal6=0;
        vegRecipe=0;
    }
    if(childrenRecipe==1)
    {
        [btnYesKids setImage:[UIImage imageNamed:@"yes_ic_hov.png"] forState:UIControlStateNormal];
        [btnCrossKids setImage:[UIImage imageNamed:@"no_ic.png"] forState:UIControlStateNormal];
        childrenRecipe=1;
    }
    else if(childrenRecipe==0)
    {
        [btnYesKids setImage:[UIImage imageNamed:@"yes_ic.png"] forState:UIControlStateNormal];
        [btnCrossKids setImage:[UIImage imageNamed:@"no_ic_hov.png"] forState:UIControlStateNormal];
        childrenRecipe=0;
    }
    if(complexRecipe==1)
    {
        [btnYesDisPrepare setImage:[UIImage imageNamed:@"yes_ic_hov.png"] forState:UIControlStateNormal];
        [btnNoDishPrepare setImage:[UIImage imageNamed:@"no_ic.png"] forState:UIControlStateNormal];
        complexRecipe=1;
        easyRecipe=0;
        moderateRecipe=0;
    }
    else if(complexRecipe==0)
    {
        [btnYesDisPrepare setImage:[UIImage imageNamed:@"yes_ic.png"] forState:UIControlStateNormal];
        [btnNoDishPrepare setImage:[UIImage imageNamed:@"no_ic_hov.png"] forState:UIControlStateNormal];
        complexRecipe=0;
        easyRecipe=1;
        moderateRecipe=1;
    }
    if(mon==1)
    {
        [btnMon setImage:[UIImage imageNamed:@"monday_ic_hover.png"] forState:UIControlStateNormal];
        mon=1;
        monDay=1;
    }
    else if(mon==0)
    {
        [btnMon setImage:[UIImage imageNamed:@"monday_ic.png"] forState:UIControlStateNormal];
        mon=0;
        monDay=0;
    }
    if(tue==1)
    {
        [btnTues setImage:[UIImage imageNamed:@"tuesday_ic_hover.png"] forState:UIControlStateNormal];
        tue=1;
        tueDay=1;
    }
    else if(tue==0)
    {
        [btnTues setImage:[UIImage imageNamed:@"tuesday_ic.png"] forState:UIControlStateNormal];
        tue=0;
        tueDay=0;
    }
    if(wed==1)
    {
        [btnWed setImage:[UIImage imageNamed:@"wednesday_ic_hover.png"] forState:UIControlStateNormal];
        wed=1;
        wedDay=1;
    }
    else if(wed==0)
    {
        [btnWed setImage:[UIImage imageNamed:@"wednesday_ic.png"] forState:UIControlStateNormal];
        wed=0;
        wedDay=0;
    }
    if(thu==1)
    {
        NSLog(@"did load thu 1");
        [btnThurs setImage:[UIImage imageNamed:@"thursday_ic_hover.png"] forState:UIControlStateNormal];
        thu=1;
        thuDay=1;
    }
    else if(thu==0)
    {
         NSLog(@"did load thu 0");
        [btnThurs setImage:[UIImage imageNamed:@"thursday_ic.png"] forState:UIControlStateNormal];
        thu=0;
        thuDay=0;
    }
    
    if(fri==1)
    {
        [btnFri setImage:[UIImage imageNamed:@"friday_ic_hover.png"] forState:UIControlStateNormal];
        fri=1;
        friDay=1;
    }
    else if(fri==0)
    {
        [btnFri setImage:[UIImage imageNamed:@"friday_ic.png"] forState:UIControlStateNormal];
        fri=0;
        friDay=0;
    }
    if(sat==1)
    {
        [btnSat setImage:[UIImage imageNamed:@"saturday_ic_hover.png"] forState:UIControlStateNormal];
        sat=1;
        satDay=1;
    }
    else if(sat==0)
    {
        [btnSat setImage:[UIImage imageNamed:@"saturday_ic.png"] forState:UIControlStateNormal];
        sat=0;
        satDay=0;
    }
    if(sun==1)
    {
        [btnSun setImage:[UIImage imageNamed:@"sunday_selected.png"] forState:UIControlStateNormal];
        sun=1;
        sunDay=1;
    }
    else if(sun==0)
    {
        [btnSun setImage:[UIImage imageNamed:@"sunday_ic.png"] forState:UIControlStateNormal];
        sun=0;
        sunDay=0;
    }
    if(mon==1 && tue==1 && wed==1 && thu==1 && fri==1 && sat==1 && sun==1)
    {
        NSLog(@"all selected");
        [btnAll setImage:[UIImage imageNamed:@"allday_ic_hover.png"] forState:UIControlStateNormal];
        [btnSun setImage:[UIImage imageNamed:@"sunday_selected.png"] forState:UIControlStateNormal];
        [btnSat setImage:[UIImage imageNamed:@"saturday_ic_hover.png"] forState:UIControlStateNormal];
        [btnFri setImage:[UIImage imageNamed:@"friday_ic_hover.png"] forState:UIControlStateNormal];
        [btnThurs setImage:[UIImage imageNamed:@"thursday_ic_hover.png"] forState:UIControlStateNormal];
        [btnWed setImage:[UIImage imageNamed:@"wednesday_ic_hover.png"] forState:UIControlStateNormal];
        [btnTues setImage:[UIImage imageNamed:@"tuesday_ic_hover.png"] forState:UIControlStateNormal];
        [btnMon setImage:[UIImage imageNamed:@"monday_ic_hover.png"] forState:UIControlStateNormal];
        all=1;
        mon=1;
        tue=1;
        wed=1;
        thu=1;
        fri=1;
        sat=1;
        sun=1;
        allDay=1;
    }
//    else
//    {
//         NSLog(@"not all selected");
//        [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
//        [btnMon setImage:[UIImage imageNamed:@"monday_ic.png"] forState:UIControlStateNormal];
//        [btnTues setImage:[UIImage imageNamed:@"tuesday_ic.png"] forState:UIControlStateNormal];
//        [btnWed setImage:[UIImage imageNamed:@"wednesday_ic.png"] forState:UIControlStateNormal];
//        [btnThurs setImage:[UIImage imageNamed:@"thursday_ic.png"] forState:UIControlStateNormal];
//        [btnFri setImage:[UIImage imageNamed:@"friday_ic.png"] forState:UIControlStateNormal];
//        [btnSat setImage:[UIImage imageNamed:@"saturday_ic.png"] forState:UIControlStateNormal];
//        [btnSun setImage:[UIImage imageNamed:@"sunday_ic.png"] forState:UIControlStateNormal];
//        all=0;
//        mon=0;
//        tue=0;
//        wed=0;
//        thu=0;
//        fri=0;
//        sat=0;
//        sun=0;
//        allDay=0;
//    }
 [btnTitlePeople setTitle:[NSString stringWithFormat:@"%d",howManyEating] forState:UIControlStateNormal];
 [mTitleTimeBtn setTitle:dateTime forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnHowManyPeopleTap:(id)sender
{
    NSLog(@"how many people tap");
    whichBtn=@"people";
    view1=[[UIView alloc] initWithFrame:CGRectMake(0, 570, 320, 200)];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0) {
        NSLog(@"device 7.0");
        view1=[[UIView alloc] initWithFrame:CGRectMake(0, 410, 320, 160)];
    }
    else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0) {
        view1=[[UIView alloc] initWithFrame:CGRectMake(0, 300, 320, 160)];
        NSLog(@"device 6.0");
    }
    mypicker=[[UIPickerView alloc]
              initWithFrame:CGRectMake(0,0, 320, 165)];
    [UIView beginAnimations:@"slide" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:1.10f];
 //   view1.backgroundColor=[UIColor yellowColor];
    [UIView commitAnimations];
    
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 300, 320, 160)];
//    [imageView setImage:[UIImage imageNamed:@"forgot_pwd_back.png"]];
//    [view1 addSubview:imageView];
    view1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"forgot_pwd_back.png"]];
    
    
    //    mypicker=[[UIPickerView alloc]
    //              initWithFrame:CGRectMake(0,0, 320, 165)];
    mypicker.delegate=(id)self ;
    mypicker.tag=1;
    mypicker.dataSource=(id)self;
    [view1 addSubview:mypicker];
    [mypicker setShowsSelectionIndicator:YES];
    //  [sheet addSubview:mypicker];
    
    [self.view addSubview:view1];

}

- (IBAction)btnTimeTap:(id)sender {
    NSLog(@"how many time tap");
    whichBtn=@"time";
    view1=[[UIView alloc] initWithFrame:CGRectMake(0, 570, 320, 200)];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0) {
        NSLog(@"device 7.0");
        view1=[[UIView alloc] initWithFrame:CGRectMake(0, 410, 320, 160)];
    }
    else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0) {
        view1=[[UIView alloc] initWithFrame:CGRectMake(0, 300, 320, 160)];
        NSLog(@"device 6.0");
    }
    mypicker=[[UIPickerView alloc]
              initWithFrame:CGRectMake(0,0, 320, 165)];
    [UIView beginAnimations:@"slide" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:1.10f];
    //   view1.backgroundColor=[UIColor yellowColor];
    [UIView commitAnimations];
    
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 300, 320, 160)];
    //    [imageView setImage:[UIImage imageNamed:@"forgot_pwd_back.png"]];
    //    [view1 addSubview:imageView];
    view1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"forgot_pwd_back.png"]];
    
    
    //    mypicker=[[UIPickerView alloc]
    //              initWithFrame:CGRectMake(0,0, 320, 165)];
    mypicker.delegate=(id)self ;
    mypicker.tag=1;
    mypicker.dataSource=(id)self;
    [view1 addSubview:mypicker];
    [mypicker setShowsSelectionIndicator:YES];
    //  [sheet addSubview:mypicker];
    
    [self.view addSubview:view1];
}

- (IBAction)btnActivateDanica:(id)sender
{
    [self sendUpdateProfData];
}

-(void)sendUpdateProfData
{
    isSuscriber=1;
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<UpdateProfile xmlns=\"http://danica.com.ar/recetario\">"
                             "<session>%@</session>"
                             "<user>"
                             "<email>%@</email>"
                             "<firstName>%@</firstName>"
                             "<password>%@</password>"
                             "<gplusId xsi:nil=\"true\"/>"
                             "<tweeterId xsi:nil=\"true\"/>"
                             "<userId>%@</userId>"
                             "<lastName>%@</lastName>"
                             "<facebookId xsi:nil=\"true\"/>"
                             "<isSuscriber>%d</isSuscriber>"
                             "<wantsbeefrecipes>%d</wantsbeefrecipes>"
                             "<wantschickenrecipes>%d</wantschickenrecipes>"
                             "<wantsporkrecipes>%d</wantsporkrecipes>"
                             "<wantsfishrecipes>%d</wantsfishrecipes>"
                             "<wantspastarecipes>%d</wantspastarecipes>"
                             "<wantsvegetarianrecipes>%d</wantsvegetarianrecipes>"
                             "<wantschildrenrecipes>%d</wantschildrenrecipes>"
                             "<wantseasyrecipes>%d</wantseasyrecipes>"
                             "<wantsmoderaterecipes>%d</wantsmoderaterecipes>"
                             "<wantscomplexrecipes>%d</wantscomplexrecipes>"
                             
                             "<sendonMon>%d</sendonMon>"
                             "<sendonTue>%d</sendonTue>"
                             "<sendonWed>%d</sendonWed>"
                             "<sendonThu>%d</sendonThu>"
                             "<sendonFri>%d</sendonFri>"
                             "<sendonSat>%d</sendonSat>"
                             "<sendonSun>%d</sendonSun>"
                             
                             "<sendonTimeofDay>%@</sendonTimeofDay>"
                             "<howmanyEating>%d</howmanyEating>"
                             
                             "</user>"
                             "</UpdateProfile>"
                             "</soap:Body>"
                             "</soap:Envelope>",appdel.mUserSession,appdel.mUserLoggedinEmail,@"Antara",@"1234567",appdel.mLoginId,@"Mukherjee",isSuscriber,beefRecipe,chickenRecipe,porkRecipe,fishRecipe,pastaRecipe,vegRecipe,childrenRecipe,easyRecipe,moderateRecipe,complexRecipe,monDay,tueDay,wedDay,thuDay,friDay,satDay,sunDay,dateTime,howManyEating];
    
    
    
    
//    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
//                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
//                             "<soap:Body>"
//                             "<UpdateProfile xmlns=\"http://danica.com.ar/recetario\">"
//                             "<session>%@</session>"
//                             "<user>"
//                             "<email>%@</email>"
//                             "<firstName>%@</firstName>"
//                             "<password>%@</password>"
//                             "<gplusId xsi:nil=\"true\"/>"
//                             "<tweeterId xsi:nil=\"true\"/>"
//                             "<userId>%@</userId>"
//                             "<lastName>%@</lastName>"
//                             "<facebookId xsi:nil=\"true\"/>"
//                             "</user>"
//                             "</UpdateProfile>"
//                             "</soap:Body>"
//                             "</soap:Envelope>",appdel.mUserSession,appdel.mUserLoggedinEmail,@"Antara",@"1234567",appdel.mLoginId,@"Antara"];
    
    
    
    
    NSLog(@"my soapMessage is->%@",soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://danica.com.ar/cgi-bin/soapserver.pl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@">>.....%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://danica.com.ar/recetario/UpdateProfile" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [NSMutableData data] ;
        NSLog(@"Problem in assistant");
    }
    else
    {
        NSLog(@"theConnection in assistant is NULL");
    }
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"ERROR with theConenction");
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"DONE. Received Bytes: %d", [webData length]);
	NSString *theXML = [[NSString alloc] initWithBytes: [webData mutableBytes] length:[webData length] encoding:NSUTF8StringEncoding];
	NSLog(@"----XML response>>%@",theXML);
    
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
	[xmlParser setDelegate: self];
	[xmlParser setShouldResolveExternalEntities: YES];
	[xmlParser parse];
}


- (IBAction)btnSelectMore:(id)sender {
    UIButton *btn=sender;
    if(  btn==btnSelect1)
    {
        NSLog(@"1st menu");
        if(btnVal1==0)
        {
            [btnSelect1 setImage:[UIImage imageNamed:@"beef_active_ic.png"] forState:UIControlStateNormal];
            btnVal1=1;
            beefRecipe=1;
        }
        else if(btnVal1==1)
        {
             [btnSelect1 setImage:[UIImage imageNamed:@"beef_ic.png"] forState:UIControlStateNormal];
            btnVal1=0;
            beefRecipe=0;
        }
    }
    else  if(  btn==btnSelect2)
    {
        NSLog(@"2nd menu");
        if(btnVal2==0)
        {
            [btnSelect2 setImage:[UIImage imageNamed:@"pork_active_ic.png"] forState:UIControlStateNormal];
            btnVal2=1;
            porkRecipe=1;
        }
        else if(btnVal2==1)
        {
            [btnSelect2 setImage:[UIImage imageNamed:@"pork_ic.png"] forState:UIControlStateNormal];
            btnVal2=0;
            porkRecipe=0;
        }
    }
    else  if(  btn==btnSelect3)
    {
        NSLog(@"3rd menu");
        if(btnVal3==0)
        {
            [btnSelect3 setImage:[UIImage imageNamed:@"pollo_active_ic.png"] forState:UIControlStateNormal];
            btnVal3=1;
            chickenRecipe=1;
        }
        else if(btnVal3==1)
        {
            [btnSelect3 setImage:[UIImage imageNamed:@"pollo_ic.png"] forState:UIControlStateNormal];
            btnVal3=0;
            chickenRecipe=0;
        }
    }
    else  if(  btn==btnSelect4)
    {
        NSLog(@"4th menu");
        if(btnVal4==0)
        {
            [btnSelect4 setImage:[UIImage imageNamed:@"fish_new_active_ic.png"] forState:UIControlStateNormal];
            btnVal4=1;
            fishRecipe=1;
        }
        else if(btnVal4==1)
        {
            [btnSelect4 setImage:[UIImage imageNamed:@"fish_new_ic.png"] forState:UIControlStateNormal];
            btnVal4=0;
            fishRecipe=0;
        }
    }
    else  if(  btn==btnSelect5)
    {
        NSLog(@"5th menu");
        if(btnVal5==0)
        {
            [btnSelect5 setImage:[UIImage imageNamed:@"pasta_active_ic.png"] forState:UIControlStateNormal];
            btnVal5=1;
            pastaRecipe=1;
        }
        else if(btnVal5==1)
        {
            [btnSelect5 setImage:[UIImage imageNamed:@"pasta_ic.png"] forState:UIControlStateNormal];
            btnVal5=0;
            pastaRecipe=0;
        }
    }
    else  if(  btn==btnSelect6)
    {
        NSLog(@"6th menu");
        if(btnVal6==0)
        {
        [btnSelect6 setImage:[UIImage imageNamed:@"veg_active_ic.png"] forState:UIControlStateNormal];
            btnVal6=1;
            vegRecipe=1;
        }
        else if(btnVal6==1)
        {
        [btnSelect6 setImage:[UIImage imageNamed:@"veg_ic.png"] forState:UIControlStateNormal];
            btnVal6=0;
            vegRecipe=0;
        }
    }
}

- (IBAction)btnExit:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DescriptionRecipeViewController *des=[[DescriptionRecipeViewController alloc] initWithNibName:@"DescriptionRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:des animated:YES];
    }
    else
    {
//        ViewController *his=[[ViewController alloc] initWithNibName:@"Main_iPhone" bundle:nil];
//        [self.navigationController pushViewController:his animated:YES];
        appdel.mLoginId=nil;
        appdel.mLoginUserName=nil;
        appdel.mUserSession=nil;
        appdel.mUserLoggedinEmail=nil;
        
        NSArray* tempVCA = [self.navigationController viewControllers];
        for(UIViewController *tempVC in tempVCA)
        {
            if([tempVC isKindOfClass:[ViewController class]])
            {
                [self.navigationController popToViewController:tempVC animated:YES];
                return;
            }
        }

    }
}

- (IBAction)btnKids:(id)sender {
    UIButton *btn=sender;
    if(  btn==btnYesKids)
    {
        NSLog(@"yes kids");
        [btnYesKids setImage:[UIImage imageNamed:@"yes_ic_hov.png"] forState:UIControlStateNormal];
        [btnCrossKids setImage:[UIImage imageNamed:@"no_ic.png"] forState:UIControlStateNormal];
        childrenRecipe=1;
    }
    else if(  btn==btnCrossKids)
    {
        NSLog(@"cross kids");
        [btnYesKids setImage:[UIImage imageNamed:@"yes_ic.png"] forState:UIControlStateNormal];
        [btnCrossKids setImage:[UIImage imageNamed:@"no_ic_hov.png"] forState:UIControlStateNormal];
        childrenRecipe=0;
    }
}

- (IBAction)btnDishTap:(id)sender {
    UIButton *btn=sender;
    if(  btn==btnYesDisPrepare)
    {
        NSLog(@"yes dish prepare");
        [btnYesDisPrepare setImage:[UIImage imageNamed:@"yes_ic_hov.png"] forState:UIControlStateNormal];
        [btnNoDishPrepare setImage:[UIImage imageNamed:@"no_ic.png"] forState:UIControlStateNormal];
        complexRecipe=1;
        easyRecipe=0;
        moderateRecipe=0;
    }
    else if(  btn==btnNoDishPrepare)
    {
        NSLog(@"yes dish prepare");
        [btnYesDisPrepare setImage:[UIImage imageNamed:@"yes_ic.png"] forState:UIControlStateNormal];
        [btnNoDishPrepare setImage:[UIImage imageNamed:@"no_ic_hov.png"] forState:UIControlStateNormal];
        complexRecipe=0;
        easyRecipe=1;
        moderateRecipe=1;
    }
}

- (IBAction)btnDays:(id)sender {
    UIButton *btn=sender;
    if(  btn==btnMon)
    {
        NSLog(@"all value is>>%d",all);
        if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            all=0;
            allDay=0;
        }
        NSLog(@"monday");
        if(mon==0)
        {
            [btnMon setImage:[UIImage imageNamed:@"monday_ic_hover.png"] forState:UIControlStateNormal];
            mon=1;
            monDay=1;
        }
        else if(mon==1)
        {
            [btnMon setImage:[UIImage imageNamed:@"monday_ic.png"] forState:UIControlStateNormal];
            mon=0;
            monDay=0;
        }
    }
    else if(  btn==btnTues)
    {
        NSLog(@"Tuesday");
        NSLog(@"all value is>>%d",all);
        if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            all=0;
            allDay=0;
            
        }
        if(tue==0)
        {
            [btnTues setImage:[UIImage imageNamed:@"tuesday_ic_hover.png"] forState:UIControlStateNormal];
            tue=1;
            tueDay=1;
        }
        else if(tue==1)
        {
            [btnTues setImage:[UIImage imageNamed:@"tuesday_ic.png"] forState:UIControlStateNormal];
            tue=0;
            tueDay=0;
        }
    }
    else if(  btn==btnWed)
    {
        NSLog(@"Wednesday");
        NSLog(@"all value is>>%d",all);
        if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            all=0;
            allDay=0;
        }
        if(wed==0)
        {
            [btnWed setImage:[UIImage imageNamed:@"wednesday_ic_hover.png"] forState:UIControlStateNormal];
            wed=1;
            wedDay=1;
        }
        else if(wed==1)
        {
            [btnWed setImage:[UIImage imageNamed:@"wednesday_ic.png"] forState:UIControlStateNormal];
            wed=0;
            wedDay=0;
        }
    }
    else if(  btn==btnThurs)
    {
        NSLog(@"Thursday");
        NSLog(@"all value is>>%d",all);
        if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            all=0;
            allDay=0;
        }
        if(thu==0)
        {
            [btnThurs setImage:[UIImage imageNamed:@"thursday_ic_hover.png"] forState:UIControlStateNormal];
            thu=1;
            thuDay=1;
        }
        else if(thu==1)
        {
            [btnThurs setImage:[UIImage imageNamed:@"thursday_ic.png"] forState:UIControlStateNormal];
            thu=0;
            thuDay=0;
        }
    }
    else if(  btn==btnFri)
    {
        NSLog(@"Friday");
        NSLog(@"all value is>>%d",all);
        if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            all=0;
            allDay=0;
        }
        if(fri==0)
        {
            [btnFri setImage:[UIImage imageNamed:@"friday_ic_hover.png"] forState:UIControlStateNormal];
            fri=1;
            friDay=1;
        }
        else if(fri==1)
        {
            [btnFri setImage:[UIImage imageNamed:@"friday_ic.png"] forState:UIControlStateNormal];
            fri=0;
            friDay=0;
        }
    }
    else if(  btn==btnSat)
    {
        NSLog(@"Saturday");
        NSLog(@"all value is>>%d",all);
        if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            all=0;
            allDay=0;
        }
        if(sat==0)
        {
            [btnSat setImage:[UIImage imageNamed:@"saturday_ic_hover.png"] forState:UIControlStateNormal];
            sat=1;
            satDay=1;
        }
        else if(sat==1)
        {
            [btnSat setImage:[UIImage imageNamed:@"saturday_ic.png"] forState:UIControlStateNormal];
            sat=0;
            satDay=0;
        }
    }
    else if(  btn==btnSun)
    {
        NSLog(@"Sunday");
        NSLog(@"all value is>>%d",all);
        if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            all=0;
            allDay=0;
        }
        if(sun==0)
        {
            [btnSun setImage:[UIImage imageNamed:@"sunday_selected.png"] forState:UIControlStateNormal];
            sun=1;
            sunDay=1;
        }
        else if(sun==1)
        {
            [btnSun setImage:[UIImage imageNamed:@"sunday_ic.png"] forState:UIControlStateNormal];
            sun=0;
            sunDay=0;
        }
    }
    else if(  btn==btnAll)
    {
        NSLog(@"All");
        if(all==0)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic_hover.png"] forState:UIControlStateNormal];
            [btnSun setImage:[UIImage imageNamed:@"sunday_selected.png"] forState:UIControlStateNormal];
            [btnSat setImage:[UIImage imageNamed:@"saturday_ic_hover.png"] forState:UIControlStateNormal];
            [btnFri setImage:[UIImage imageNamed:@"friday_ic_hover.png"] forState:UIControlStateNormal];
            [btnThurs setImage:[UIImage imageNamed:@"thursday_ic_hover.png"] forState:UIControlStateNormal];
            [btnWed setImage:[UIImage imageNamed:@"wednesday_ic_hover.png"] forState:UIControlStateNormal];
            [btnTues setImage:[UIImage imageNamed:@"tuesday_ic_hover.png"] forState:UIControlStateNormal];
            [btnMon setImage:[UIImage imageNamed:@"monday_ic_hover.png"] forState:UIControlStateNormal];
            all=1;
            mon=1;
            tue=1;
            wed=1;
            thu=1;
            fri=1;
            sat=1;
            sun=1;
            
            allDay=1;
        }
        else if(all==1)
        {
            [btnAll setImage:[UIImage imageNamed:@"allday_ic.png"] forState:UIControlStateNormal];
            [btnMon setImage:[UIImage imageNamed:@"monday_ic.png"] forState:UIControlStateNormal];
            [btnTues setImage:[UIImage imageNamed:@"tuesday_ic.png"] forState:UIControlStateNormal];
            [btnWed setImage:[UIImage imageNamed:@"wednesday_ic.png"] forState:UIControlStateNormal];
            [btnThurs setImage:[UIImage imageNamed:@"thursday_ic.png"] forState:UIControlStateNormal];
            [btnFri setImage:[UIImage imageNamed:@"friday_ic.png"] forState:UIControlStateNormal];
            [btnSat setImage:[UIImage imageNamed:@"saturday_ic.png"] forState:UIControlStateNormal];
            [btnSun setImage:[UIImage imageNamed:@"sunday_ic.png"] forState:UIControlStateNormal];
            all=0;
            mon=0;
            tue=0;
            wed=0;
            thu=0;
            fri=0;
            sat=0;
            sun=0;
            
            allDay=0;
        }
    }
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSelection:(id)sender {
    
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please login to access the page" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSLog(@"btnSelection else part");
        appdel.whichBtnClicked=@"btnSelect";
    //    [self.navigationController popViewControllerAnimated:YES];
        // obj=[[GetRecipeViewController alloc] init];
        //        [obj getRecipeDetails];
        //        [self methodXMLParse];
        //        [self.navigationController popToRootViewControllerAnimated:YES];
        NSArray* tempVCA = [self.navigationController viewControllers];
        NSLog(@"navigation array :%@",tempVCA);
//        for(UIViewController *tempVC in tempVCA)
//        {
//            if([tempVC isKindOfClass:[DetailsRecipeViewController class]])
//            {
//                [self.navigationController popToViewController:tempVC animated:YES];
//                return;
//            }
//        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
            [self.navigationController pushViewController:reg animated:YES];
        }
        else
        {
            DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
            [self.navigationController pushViewController:reg animated:YES];
        }
    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
   
    if([whichBtn isEqualToString:@"people"])
    {
        return [mPeopleAry count];
    }
    else if ([whichBtn isEqualToString:@"time"])
    {
        return [mTimeAry count];
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if([whichBtn isEqualToString:@"people"])
    {
        return [mPeopleAry objectAtIndex:row];
    }
    else if ([whichBtn isEqualToString:@"time"])
    {
       return [mTimeAry objectAtIndex:row];
    }
    return 0;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"in action sheet click");
    //            city.text=[cityArray objectAtIndex:[citypicker selectedRowInComponent:0]];
    NSLog(@".....in picker %@",[mPeopleAry objectAtIndex:[mypicker selectedRowInComponent:0]]);
//    mlblWait.text=[mAryWait objectAtIndex:[mypicker selectedRowInComponent:0]];
//    waitTime=[[mAryWait objectAtIndex:[mypicker selectedRowInComponent:0]] intValue];
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if([whichBtn isEqualToString:@"people"])
    {
        NSLog(@".....in picker %@",[mPeopleAry objectAtIndex:[mypicker selectedRowInComponent:0]]);
        //    mlblWait.text=[mAryWait objectAtIndex:[mypicker selectedRowInComponent:0]];
        //    waitTime=[[mAryWait objectAtIndex:[mypicker selectedRowInComponent:0]] intValue];
        if([[mPeopleAry objectAtIndex:[mypicker selectedRowInComponent:0]] isEqualToString:@"Más de 12"])
        {
            str=[NSString stringWithFormat:@"99"];
        }
        else
        {
            str=[mPeopleAry objectAtIndex:[mypicker selectedRowInComponent:0]];
        }
        
        NSLog(@"str is >>%@",str);
        [btnTitlePeople setTitle:str forState:UIControlStateNormal];
        howManyEating=[str intValue];
    }
    else if ([whichBtn isEqualToString:@"time"])
    {
        NSLog(@".....time in picker %@",[mTimeAry objectAtIndex:[mypicker selectedRowInComponent:0]]);
          NSString *str1=[mTimeAry objectAtIndex:[mypicker selectedRowInComponent:0]];
         [mTitleTimeBtn setTitle:str1 forState:UIControlStateNormal];
        dateTime=[mTimeAry objectAtIndex:[mypicker selectedRowInComponent:0]];
    }
    [view1 removeFromSuperview];
}


//Parsing
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
   attributes: (NSDictionary *)attributeDict
{
	if( [elementName isEqualToString:@"UpdateProfileResponse"])
	{
        NSLog(@"UpdateProfileResponse got....");
		if(!soapResults)
		{
			soapResults = [[NSMutableString alloc] init];
		}
		recordResults = TRUE;
	}
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if( recordResults )
	{
		[soapResults appendString: string];
	}
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	if( [elementName isEqualToString:@"UpdateProfileResponse"])
	{
		recordResults = FALSE;
		//greeting.text = soapResults;
        NSLog(@"soap result is >>%@",soapResults);
        if([soapResults isEqualToString:@"OK"])
        {
            NSLog(@"Update profile response OK");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Sus preferencias han sido registradas" delegate:self cancelButtonTitle:@"Continuar" otherButtonTitles:nil];
            [alert show];
            [self insertIntoUserLoginDetails];
            [self methodSelectFromTable];
        }
	}
}

-(void)insertIntoUserLoginDetails
{
    sqlite3 *contactDB;
    sqlite3_stmt *updateStmt;
    NSString *database_Name =@"COOKBOOKAPP.db";
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    NSString *dbpath = [documentsDir stringByAppendingPathComponent:database_Name];
    
    if(sqlite3_open([dbpath UTF8String], &contactDB) == SQLITE_OK)
    {
//        NSString *querySql=[NSString stringWithFormat:@"UPDATE USERLOGINDET SET LAST_NAME='Something' WHERE USER_ID=144"];
        
         NSString *querySql=[NSString stringWithFormat:@"UPDATE USERLOGINDET SET FIRST_NAME='%@',IS_SUBSCRIBER=%d,WANTS_BEEFRECIPES=%d,WANTS_CHICKENRECIPES=%d,WANTS_PORKRECIPES=%d,WANTS_FISHRECIPES=%d,WANTS_VEGRECIPES=%d,WANTS_PASTA=%d,WANTS_CHILDRENRECIPES=%d,WANTS_EASYRECIPES=%d,WANTS_MODERATERECIPES=%d , WANTS_COMPLEXRECIPES=%d , SENDON_MON=%d , SENDON_TUE=%d , SENDON_WED=%d , SENDON_THU=%d , SENDON_FRI=%d , SENDON_SAT=%d , SENDON_SUN=%d , SENDON_TIMEOFDAY='%@' , HOWMANYEATING=%d ,USER_SESSION='%@' WHERE USER_ID=%@",appdel.mLoginUserName,isSuscriber,beefRecipe,chickenRecipe,porkRecipe,fishRecipe,vegRecipe,pastaRecipe,childrenRecipe,easyRecipe,moderateRecipe,complexRecipe,monDay,tueDay,wedDay,thuDay,friDay,satDay,sunDay,dateTime,howManyEating,appdel.mUserSession,appdel.mLoginId];
        
//        NSString *querySql=[NSString stringWithFormat:@"UPDATE USERLOGINDET SET FIRST_NAME='%@' WHERE USER_ID=%@",appdel.mLoginUserName,appdel.mLoginId];
        
        NSLog(@"query send is>>%@",querySql);
        
        const char*sql=[querySql UTF8String];
        if(sqlite3_prepare_v2(contactDB,sql, -1, &updateStmt, NULL) == SQLITE_OK)
        {
            NSLog(@"USERLOGINDET in SQLITE_OK");
            if(SQLITE_DONE != sqlite3_step(updateStmt))
            {
                  NSLog(@"USERLOGINDET in SQLITE_DONE not");
                NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(contactDB));
            }
            else{
                sqlite3_reset(updateStmt);
                NSLog(@"Update done successfully! SQLITE_DONE");
            }
        }
        sqlite3_finalize(updateStmt);
    }
    sqlite3_close(contactDB);
   
//        NSString *docsDir;
//        NSArray *dirPaths;
//        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        
//        docsDir = [dirPaths objectAtIndex:0];
//        
//        // Build the path to the database file
//        databasePath = [[NSString alloc]
//                        initWithString: [docsDir stringByAppendingPathComponent:
//                                         @"COOKBOOKAPP.db"]];
//    
//    NSLog(@"database path is form>>%@",databasePath);
//    
//        const char *dbpath = [databasePath UTF8String];
//        if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
//        {
////            NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO USERLOGINDET (USER_ID  , FIRST_NAME , LAST_NAME ,EMAIL , FACEBOOK_ID , IS_SUBSCRIBER , HAS_MOBILEAPP , WANTS_BEEFRECIPES , WANTS_CHICKENRECIPES , WANTS_PORKRECIPES , WANTS_FISHRECIPES ,  WANTS_VEGRECIPES , WANTS_OTHERRECIPES , WANTS_CHILDRENRECIPES , WANTS_EASYRECIPES , WANTS_MODERATERECIPES , WANTS_COMPLEXRECIPES , SENDON_MON , SENDON_TUE , SENDON_WED , SENDON_THU , SENDON_FRI , SENDON_SAT , SENDON_SUN , SENDON_TIMEOFDAY , HOWMANYEATING ,USER_SESSION  ) VALUES (\"%@\",\"%@\")",mTxtUsername.text,mTxtPassword.text];
////            
////            const char *insert_stmt = [insertSQL UTF8String];
////            sqlite3_prepare_v2(cookLoginDB, insert_stmt,
////                               -1, &statement, NULL);
////            if (sqlite3_step(statement) == SQLITE_DONE)
////            {
////                // status.text = @"Contact added";
////                NSLog(@"Contact added REMEMBERPASSWORD");
////                
////            } else {
////                // status.text = @"Failed to add contact";
////                NSLog(@" Failed to add contact REMEMBERPASSWORD");
////            }
////            sqlite3_finalize(statement);
////            sqlite3_close(cookLoginDB);
//            
//            
//            
//            NSLog(@"database Opened");
//            const char* updateQuery="UPDATE USERLOGINDET SET LAST_NAME='Something' WHERE USER_ID=144";
//           sqlite3_stmt    *statement;
//            if (sqlite3_prepare_v2(cookLoginDB, updateQuery, -1, &statement, NULL)==SQLITE_OK) {
//                NSLog(@"Query Executed");
//            }
//        }
}
-(void)methodSelectFromTable
{
    NSLog(@"in method select");
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"in select database path>>>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        NSLog(@"file not exist");
    }
    else
    {
        NSLog(@"file exist");
    }
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"in select page dbpath USERLOGINDET->>%s",dbpath);
    sqlite3_stmt    *statement;
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
       // NSString *querySQL = [NSString stringWithFormat:@"Select  * from USERLOGINDET where USER_ID=%@",appdel.mLoginId];
        
//         NSString *querySQL = [NSString stringWithFormat:@"Select  WANTS_BEEFRECIPES=,WANTS_CHICKENRECIPES,WANTS_PORKRECIPES,WANTS_FISHRECIPES,WANTS_VEGRECIPES,WANTS_CHILDRENRECIPES,WANTS_EASYRECIPES,WANTS_MODERATERECIPES , WANTS_COMPLEXRECIPES , SENDON_MON , SENDON_TUE , SENDON_WED , SENDON_THU , SENDON_FRI , SENDON_SAT , SENDON_SUN , SENDON_TIMEOFDAY , HOWMANYEATING from USERLOGINDET where USER_ID=%@",appdel.mLoginId];
//        NSLog(@"The Data USERLOGINDET= %@",querySQL);
        
        
     NSString *querySQL = [NSString stringWithFormat:@"Select  WANTS_BEEFRECIPES,WANTS_CHICKENRECIPES,WANTS_PORKRECIPES,WANTS_FISHRECIPES,WANTS_VEGRECIPES,WANTS_CHILDRENRECIPES,WANTS_EASYRECIPES,WANTS_MODERATERECIPES , WANTS_COMPLEXRECIPES , SENDON_MON , SENDON_TUE , SENDON_WED , SENDON_THU , SENDON_FRI , SENDON_SAT , SENDON_SUN,SENDON_TIMEOFDAY, HOWMANYEATING,WANTS_PASTA from USERLOGINDET where USER_ID=%@",appdel.mLoginId];
        
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"show data char is USERLOGINDET-->>>>%s",query_stmt);
        if (sqlite3_prepare_v2(cookLoginDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"111### USERLOGINDET");
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSLog(@"in row USERLOGINDET");
                NSLog(@"select at 0 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]);
                NSLog(@"select at 1 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]);
                NSLog(@"select at 2 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]);
                NSLog(@"select at 3 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)]);
                NSLog(@"select at 4 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)]);
                NSLog(@"select at 5 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)]);
                NSLog(@"select at 6 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)]);
                NSLog(@"select at 7 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)]);
                
                
                
                NSLog(@"select at 8 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)]);
                NSLog(@"select at 9 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)]);
                NSLog(@"select at 10 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)]);
                NSLog(@"select at 11 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)]);
                NSLog(@"select at 12 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)]);
                NSLog(@"select at 13 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 13)]);
                NSLog(@"select at 14 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 14)]);
                NSLog(@"select at 15 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)]);
                NSLog(@"select at 16 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 16)]);
                NSLog(@"select at 17 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 17)]);
                 NSLog(@"select at 18 USERLOGINDET >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 18)]);
                
                btnVal1=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)] intValue];
                btnVal2=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)] intValue];
                btnVal3=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)] intValue];
                btnVal4=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)] intValue];
                btnVal5=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 18)] intValue];
                btnVal6=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)] intValue];
                childrenRecipe=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)] intValue];
                easyRecipe=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)] intValue];
                moderateRecipe=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)] intValue];
                complexRecipe=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)] intValue];
//                monDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)] intValue];
//                tueDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)] intValue];
//                wedDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)] intValue];
//                thuDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)] intValue];
//                friDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 13)] intValue];
//                satDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 14)] intValue];
//                sunDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)] intValue];
//                sunDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)] intValue];
//                sunDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)] intValue];
//                sunDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)] intValue];
                
                
                mon=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)] intValue];
                tue=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)] intValue];
                wed=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)] intValue];
                thu=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)] intValue];
                fri=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 13)] intValue];
                sat=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 14)] intValue];
                sun=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)] intValue];
                dateTime=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 16)] ;
                howManyEating=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 17)] intValue];
               // sunDay=[[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)] intValue];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(cookLoginDB);
}



@end
