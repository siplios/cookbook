//
//  ViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 05/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrationViewController.h"
#import <sqlite3.h>
#import "GetRecipeViewController.h"
#import "ForgotPasswordViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <QuartzCore/QuartzCore.h>
#import <GooglePlus/GooglePlus.h>
@interface ViewController : UIViewController<UITextFieldDelegate,NSXMLParserDelegate,GPPSignInDelegate>
{
    NSInteger val;
      BOOL checked;
     NSMutableData *webData;
    
    NSString *mPasswordStr;
    NSXMLParser *xmlParser;
    NSMutableString *soapResults;
    NSMutableString *soapResultsForgot;
    NSMutableString *soapResultsRegFB;
    BOOL *recordResults;
    NSString *currentElement;
    
    //sqlite(Local store)
    sqlite3 *cookLoginDB;
    NSString        *databasePath;
    NSInteger value;
    
    //REMEMBER PASSWORD
    NSString *remPassword;
    NSString *remLoginSuccess;
    
    //FaceBook userdetails
    NSString *firstName;
    NSString *lastName;
    NSString *userfbEmail;
    NSString *userfbPassword;
    NSString *userfbID;
    GPPSignIn *signIn;
}
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (strong, nonatomic) IBOutlet UITextField *mTxtUsername;
@property (strong, nonatomic) IBOutlet UITextField *mTxtPassword;

@property (strong, nonatomic) NSString *mEncodedPassword;
@property (strong, nonatomic) NSMutableArray *mParseAry;
@property (assign) BOOL isSocial;

- (IBAction)mBtnSignin:(id)sender;
- (IBAction)btnCheckTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckTitle;
- (IBAction)btnForgotPassword:(id)sender;
- (IBAction)btnRegistration:(id)sender;
- (IBAction)btnWithoutRegistration:(id)sender;
- (IBAction)btnLoginFaceBook:(id)sender;
- (IBAction)btnLoginGplus:(id)sender;
- (IBAction)btnLoginTwitter:(id)sender;

@end
