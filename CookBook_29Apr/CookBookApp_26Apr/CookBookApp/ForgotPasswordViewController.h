//
//  ForgotPasswordViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 09/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate,NSXMLParserDelegate,UIAlertViewDelegate>
{
    NSMutableData *webData;
     NSXMLParser *xmlParser;
    NSMutableString *soapResultsForgot;
    BOOL *recordResults;
}
@property (strong, nonatomic) IBOutlet UITextField *mTxtForgotEmail;
@property (strong, nonatomic) NSMutableArray *mParseAry;
- (IBAction)btnForgotPassword:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *mlblForgotAlert;
@property (strong, nonatomic) IBOutlet UIImageView *mImgTxtBack;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleBack;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleSubmit;
- (IBAction)btnBack:(id)sender;
@end
