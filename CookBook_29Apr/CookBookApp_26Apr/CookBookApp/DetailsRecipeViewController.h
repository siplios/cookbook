//
//  DetailsRecipeViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 02/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <QuartzCore/QuartzCore.h>
#import "DescriptionRecipeViewController.h"
#import "FormViewController.h"

@interface DetailsRecipeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    int count;
    int pagecount;
    int pagecountRev;
    int favVal;
    UIButton *spcBtn;
    
    sqlite3 *cookLoginDB;
    NSString        *databasePath;
    
    //loggedin user fav and rate app
    NSString *mFav_RecipeId;
    NSString *mFav_LoginId;
    BOOL fav;
    float mFav_Rate;
    int mCount;
    
    float getRate;
    float selectnRate;
    
    NSString *mimagePath;
}
- (IBAction)btnFavTap:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *mlbl_LoginUserName;
@property (strong, nonatomic) IBOutlet UIView *mViewSingleRecipe;

@property (strong, nonatomic) IBOutlet UILabel *mlbl_RecipeName;
@property (strong, nonatomic) IBOutlet UIView *mViewCategorisedRecipe;
@property (strong, nonatomic) IBOutlet UITableView *mTableCategory;
@property (strong, nonatomic) IBOutlet UILabel *mTableLabel;
@property (strong, nonatomic) IBOutlet UIImageView *mImgBack;

@property (strong, nonatomic) IBOutlet UITextView *mTxtViewDetails;
@property (strong, nonatomic) IBOutlet UITextView *mTxtViewTableDetails;
@property (assign) int index;

@property (strong, nonatomic) NSMutableArray *mRecipeID;
@property (strong, nonatomic) NSMutableArray *mshortDesAry;
@property (strong, nonatomic) NSMutableArray *mlongDesAry;
@property (strong, nonatomic) NSMutableArray *mBigImgURLAry;
@property (strong, nonatomic) NSMutableArray *mCategoryIDAry;
@property (strong, nonatomic) NSMutableArray *mLastUpdateAry;
@property (strong, nonatomic) NSMutableArray *mSmallImgAry;

@property (strong, nonatomic) IBOutlet UIView *mViewCust;
@property (strong, nonatomic) IBOutlet UIButton *mBtnFav;
@property (strong, nonatomic) IBOutlet UIPageControl *mlblPageControl;
- (IBAction)btnPageControl:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnTitle1;
@property (strong, nonatomic) IBOutlet UIButton *btnTitle2;
@property (strong, nonatomic) IBOutlet UIButton *btnTitle3;
@property (strong, nonatomic) IBOutlet UIButton *btnTitle4;
- (IBAction)btn1Tapped:(id)sender;
- (IBAction)btnFirst:(id)sender forEvent:(UIEvent *)event;
- (IBAction)btnSecond:(id)sender forEvent:(UIEvent *)event;
- (IBAction)btnThird:(id)sender forEvent:(UIEvent *)event;
- (IBAction)btnFourth:(id)sender forEvent:(UIEvent *)event;
- (IBAction)btnSelectedRecipes:(id)sender;
- (IBAction)btnAssistant:(id)sender;

@property (assign) CGPoint point;
@property (assign) float f;
@property (assign) float f1;
@property (assign) float f2;
@property (assign) float f3;
@property (assign) float f4;
@property (assign) float totalFloat;
@property (assign) float finalStarValue;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleFav;
@property (strong, nonatomic) IBOutlet UIImageView *mUserImg;
@property (strong, nonatomic) IBOutlet UIImageView *mRecipeImage;
@property (strong, nonatomic) IBOutlet UIImageView *mCatRecipeImg;
@property (strong, nonatomic) IBOutlet UIImageView *mBgImgKeyWord;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleAssistant;
- (IBAction)btnBack:(id)sender;


//For perticular user login fav button and star
@property (strong, nonatomic)NSMutableArray *mSelectedRecipeIdAry;
@property (strong, nonatomic)NSMutableArray *mSelectedRateValueAry;

//Categorised search
@property (strong, nonatomic) NSMutableArray *mMatchedAry;

//info
- (IBAction)btnInfoTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleInfo;

//
@property (strong, nonatomic) NSMutableArray *mSelectionAry;
//touchable image
- (IBAction)mBtnImgsingleRecipe:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *mbtnTitleInfo;
- (IBAction)btnRecImgCatTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *mTitleRecImgCat;
@property (strong, nonatomic) IBOutlet UIImageView *mImgPicNotSet;

@end
