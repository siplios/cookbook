//
//  DetailsRecipeViewController.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 02/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "DetailsRecipeViewController.h"
#import "AppDelegate.h"
AppDelegate *appdel;
#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)

@interface DetailsRecipeViewController ()

@end

@implementation DetailsRecipeViewController
@synthesize mlbl_LoginUserName;
@synthesize mViewSingleRecipe;
@synthesize mlbl_RecipeName;
@synthesize mTxtViewDetails;
@synthesize mViewCategorisedRecipe;
@synthesize mTableLabel;
@synthesize mRecipeID;
@synthesize mViewCust;
@synthesize mTableCategory;
@synthesize mImgBack;
@synthesize mshortDesAry;
@synthesize mlongDesAry;
@synthesize mBigImgURLAry;
@synthesize mTxtViewTableDetails;
@synthesize mCategoryIDAry;
@synthesize mLastUpdateAry;
@synthesize mSmallImgAry;
@synthesize mlblPageControl;
@synthesize btnTitle1;
@synthesize btnTitle2;
@synthesize btnTitle3;
@synthesize btnTitle4;
@synthesize point;
@synthesize f;
@synthesize f1;
@synthesize f2;
@synthesize f3;
@synthesize f4;
@synthesize totalFloat;
@synthesize finalStarValue;
@synthesize btnTitleFav;
@synthesize mSelectedRecipeIdAry;
@synthesize mSelectedRateValueAry;
@synthesize mMatchedAry;
@synthesize btnTitleInfo;
@synthesize mSelectionAry;
@synthesize mUserImg;
@synthesize mRecipeImage;
@synthesize mCatRecipeImg;
@synthesize mbtnTitleInfo;
@synthesize mTitleRecImgCat;
@synthesize mBgImgKeyWord;
@synthesize btnTitleAssistant;
@synthesize mImgPicNotSet;

NSInteger b=0;
NSInteger countTableCell=0;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     appdel=[[UIApplication sharedApplication] delegate];
    mTableCategory.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90));
    mViewSingleRecipe.hidden=TRUE;
    mViewCategorisedRecipe.hidden=TRUE;
    count=0;
    mRecipeID=[[NSMutableArray alloc] init];
    mshortDesAry=[[NSMutableArray alloc] init];
    mlongDesAry=[[NSMutableArray alloc] init];
    mBigImgURLAry=[[NSMutableArray alloc] init];
    mCategoryIDAry=[[NSMutableArray alloc] init];
    mLastUpdateAry=[[NSMutableArray alloc] init];
     mSmallImgAry=[[NSMutableArray alloc] init];
    mSelectedRecipeIdAry=[[NSMutableArray alloc] init];
    mSelectedRateValueAry=[[NSMutableArray alloc] init];
    mMatchedAry=[[NSMutableArray alloc] init];
    mSelectionAry=[[NSMutableArray alloc] init];
    
    pagecount=0;
    favVal=0;
    mCount=0;
    
    mImgPicNotSet.hidden=TRUE;
    
    //[self methodSelectForUserLogin];
   // [self methodSelectFavoStarRate];
    
    
    UIImage * myImage = [UIImage imageWithContentsOfFile: @"../Documents/savedImage.png"];
     [mUserImg setImage:myImage];
    if([appdel.mLoginUserName length]==0)
    {
        mlbl_LoginUserName.text=[NSString stringWithFormat:@"Bienvenido invitado"];
        UIImage *image = [UIImage imageNamed: @"pic.png"];
        [mUserImg setImage:image];
    }
    else
    {
        if([appdel.mImagePath isEqualToString:@""])
        {
            NSLog(@"yes nil");
            UIImage *image = [UIImage imageNamed:@"pic.png"];
            // [mUserImg setImage:image];
            mUserImg.image = image;
            mImgPicNotSet.hidden=FALSE;
        }
       // mImgPicNotSet.hidden=TRUE;
        mlbl_LoginUserName.text=[NSString stringWithFormat:@"Bienvenido %@",appdel.mLoginUserName];
        [self methodSelectForUserLogin];
        UIImage *graphImage = [[UIImage alloc] initWithContentsOfFile: appdel.mImagePath];
        mUserImg.image = graphImage;
    }
    if([appdel.whichBtnClicked isEqualToString:@"btnCategory"])
    {
        NSLog(@"from btnCategory");
        [btnTitleFav setImage:[UIImage imageNamed:@"notFav.png"] forState:UIControlStateNormal];
        NSLog(@"in details recipe ...>>%@",[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_Title"]);
    }
//    else if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
//    {
//        NSLog(@"from btnSelect");
//        if([appdel.mLoginUserName length]!=0)
//        {
//            NSLog(@"btnSelect user logged in name=%@ and id=%@",appdel.mLoginUserName,appdel.mLoginId);
//            
//        }
//        else if([appdel.mLoginUserName length]==0)
//        {
//            NSLog(@"btnSelect user not logged in");
//        }
//        [self methodLoggedInUserFav];
//        mViewCategorisedRecipe.hidden=FALSE;
//        [self.view addSubview:mViewCategorisedRecipe];
//        [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
//    }
	// Do any additional setup after loading the view.
       btnTitle1.frame=CGRectMake(140, -20, 300, 215);
   // [mViewCategorisedRecipe setCenter:CGPointMake(160, 230)];
    
    if([appdel.whichSearch isEqualToString:@"keywordSearch"])
    {
        mViewSingleRecipe.hidden=FALSE;
        mViewCategorisedRecipe.hidden=TRUE;
        NSLog(@"keyword rec id >>%@",[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ID"]);
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"recipe_id_%@.jpg",[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ID"]]];
        
     //   [mRecipeImage setImage:image];
        
        [mbtnTitleInfo setImage:image forState:UIControlStateNormal];
        
//        UIImage *image = [UIImage imageNamed: @"recipe_id_49.jpg"];
//        [mRecipeImage setImage:image];
        
        mlbl_RecipeName.text=[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_Title"];
        
        
        mTxtViewDetails.text=[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ShortDes"];
        
        
        
        NSLog(@"short des..%@",[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ShortDes"]);
        
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
        }
        else
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0)
            {
                NSLog(@"yes 7.0 predict table");
                
                if([[UIScreen mainScreen] bounds].size.height == 568.0)
                {
                    NSLog(@"screen size 568 mViewSingleRecipe");
                    mViewSingleRecipe.frame=CGRectMake(10, 365, 200, 45);
                    CALayer *btnLayer = [mbtnTitleInfo layer];
                    [btnLayer setMasksToBounds:YES];
                    [btnLayer setCornerRadius:5.0f];
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 480.0)
                {
                    NSLog(@"screen size 480 mViewSingleRecipe");
                   mViewSingleRecipe.frame=CGRectMake(10, 298, 200, 105);
                    CALayer *btnLayer = [mbtnTitleInfo layer];
                    [btnLayer setMasksToBounds:YES];
                    [btnLayer setCornerRadius:5.0f];
                }
            }
            else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0)
            {
                
            }
        }
        NSLog(@"single view frame>>%@",NSStringFromCGRect(mViewSingleRecipe.frame));
        
        
        
        
        
        
        
        
        [self.view addSubview:mViewSingleRecipe];
      //  [mViewSingleRecipe setCenter:CGPointMake(160, 430)];
        mFav_RecipeId=[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ID"];
        NSLog(@"recipe id for single view %@",mFav_RecipeId);
        for(int i=0;i<[mSelectedRateValueAry count];i++)
        {
            if([mFav_RecipeId isEqualToString:[NSString stringWithFormat:@"%@",[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_ID"]]])
            {
                NSLog(@"yes fav id matched single view");
                [btnTitleFav setImage:[UIImage imageNamed:@"Fav.png"] forState:UIControlStateNormal];
                getRate=[[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_Rate"] floatValue];
                NSLog(@"getRate value in cellforrow is >>%f",getRate);
                if(getRate==0.50)
                {
                    NSLog(@"rate 0.50 ");
                    [btnTitle1 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==1.00)
                {
                    NSLog(@"rate 1.00");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==1.50)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==2.00)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==2.50)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==3.00)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==3.50)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==4.00)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                }
                 favVal=1;
            }
            else
            {
                NSLog(@"no fav id not matched single view");
                mCount=mCount+1;
            }
        }
        NSLog(@"mcount is %d",mCount);
        //    if(mCount==[mSelectedRecipeIdAry count])
        if(mCount==[mSelectedRateValueAry count])
        {
            favVal=0;
        }
        NSLog(@"fav value is in cellforrow single view.............................##%d",favVal);
    }
    else if([appdel.whichSearch isEqualToString:@"categorySearch"])
    {
         mViewCust.frame=CGRectMake(-40, -40, mViewCust.frame.size.width, mViewCust.frame.size.height);
        mViewCategorisedRecipe.hidden=FALSE;
       
      //  mViewCust.frame=CGRectMake(10,0, 50, 45);
        [self.view addSubview:mViewCategorisedRecipe];
        // [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
        mTableCategory.frame=CGRectMake(15, 20, 270, 115);
        NSLog(@"table frame is >>%@",NSStringFromCGRect(mTableCategory.frame));
         NSLog(@"view frame is >>%@",NSStringFromCGRect(mViewCust.frame));
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
        }
        else
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0)
            {
                NSLog(@"yes 7.0 predict table");
                
                if([[UIScreen mainScreen] bounds].size.height == 568.0)
                {
                    NSLog(@"screen size 568");
                    mViewCategorisedRecipe.frame=CGRectMake(10,335, 300, 65);
                    mlblPageControl.frame=CGRectMake(130, 133, 39, 37);
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 480.0)
                {
                     NSLog(@"screen size 480");
                    mViewCategorisedRecipe.frame=CGRectMake(10,275, 300, 155);
                    mlblPageControl.frame=CGRectMake(130, 125, 39, 37);
                }
            }
            else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0)
            {
               
            }
        }
        
        
    }
    if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
    {
        NSLog(@"in btnSelect");
//        mViewCategorisedRecipe.hidden=FALSE;
//     //   mViewCategorisedRecipe.frame=CGRectMake(10, 340, 300, 145);
//        mViewCategorisedRecipe.frame=CGRectMake(10,265, 300, 145);
//        [self.view addSubview:mViewCategorisedRecipe];
//        mlblPageControl.frame=CGRectMake(130, 141, 39, 37);
//        mTableCategory.frame=CGRectMake(15, 20, 270, 115);
//       // [self.view addSubview:mViewCategorisedRecipe];
//        // [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
        
        
        
        mViewCust.frame=CGRectMake(-40, -40, mViewCust.frame.size.width, mViewCust.frame.size.height);
        mViewCategorisedRecipe.hidden=FALSE;
        
        //  mViewCust.frame=CGRectMake(10,0, 50, 45);
        [self.view addSubview:mViewCategorisedRecipe];
        // [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
        mTableCategory.frame=CGRectMake(15, 20, 270, 115);
        NSLog(@"table frame is >>%@",NSStringFromCGRect(mTableCategory.frame));
        NSLog(@"view frame is >>%@",NSStringFromCGRect(mViewCust.frame));
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
        }
        else
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0)
            {
                NSLog(@"yes 7.0 predict table");
                
                if([[UIScreen mainScreen] bounds].size.height == 568.0)
                {
                    NSLog(@"screen size 568");
                    mViewCategorisedRecipe.frame=CGRectMake(10,335, 300, 165);
                    mlblPageControl.frame=CGRectMake(130, 133, 39, 37);
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 480.0)
                {
                    NSLog(@"screen size 480");
                    mViewCategorisedRecipe.frame=CGRectMake(10,275, 300, 155);
                    mlblPageControl.frame=CGRectMake(130, 125, 39, 37);
                }
            }
            else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0)
            {
                
            }
        }

        
        
    }
    [self categorySelected];
    NSLog(@"appdel.mCategorySearchAry is >>%@",appdel.mCategorySearchAry);
     NSLog(@"mRecipeID is >>%@",mRecipeID);
    NSLog(@"mshortDesAry is >>%@",mshortDesAry);
    [mlblPageControl setNumberOfPages:[mRecipeID count]];
    
     pagecountRev=[mshortDesAry count];
    
   
    
    NSLog(@"mRecipeID array is %@",mRecipeID);
    
    NSLog(@"already fav recipe id =>%@",mSelectedRecipeIdAry);
    NSLog(@"already fav array is mSelectedRateValueAry=>%@",mSelectedRateValueAry);
    
    NSLog(@"Now Matched category wise ##>>>>>>#### %@",mMatchedAry);
     NSLog(@"Now Matched category wise count ##>>>>>>#### %d",[mMatchedAry count]);
    NSLog(@"mSelectionAry ary viewdidload is >>%@",mSelectionAry);
    NSLog(@"mSelectionAry ary viewdidload count is >>%d",[mSelectionAry count]);
    NSLog(@"appdel which button >>%@",appdel.whichBtnClicked);
    NSLog(@"My view frame: %@", NSStringFromCGRect(mViewCategorisedRecipe.frame));
    
    if([appdel.mLoginUserName length]==0)
    {
        [btnTitleAssistant setImage:[UIImage imageNamed:@"help_disable.png"] forState:UIControlStateNormal];
    }
    else
    {
    [btnTitleAssistant setImage:[UIImage imageNamed:@"help.png"] forState:UIControlStateNormal];
    }
//    NSString *homeDirectoryPath = NSHomeDirectory();
//    NSString *imagePath = [homeDirectoryPath stringByAppendingString:@"/Documents/savedImage.png"];
//    NSLog(@"Image for user: %@", imagePath);

}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"in viewWillAppear");
    [mSelectionAry removeAllObjects];
     if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
    {
        NSLog(@"from btnSelect");
        
        if([appdel.mLoginUserName length]!=0)
        {
            NSLog(@"btnSelect user logged in name=%@ and id=%@",appdel.mLoginUserName,appdel.mLoginId);
            
        }
        else if([appdel.mLoginUserName length]==0)
        {
            NSLog(@"btnSelect user not logged in");
        }
        [self methodLoggedInUserFav];
        mViewCategorisedRecipe.hidden=FALSE;
        mViewSingleRecipe.hidden=TRUE;
        [self.view addSubview:mViewCategorisedRecipe];
       // [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
        [mTableCategory reloadData];
    }
    if([appdel.whichSearch isEqualToString:@"keywordSearch"])
    {
        mViewSingleRecipe.hidden=FALSE;
        mViewCategorisedRecipe.hidden=TRUE;
    }
}
-(void)methodLoggedInUserFav
{
    [mSelectionAry removeAllObjects];
    NSLog(@"mSelectedRateValueAry is=>%@",mSelectedRateValueAry);
    for(int i=0;i<[mSelectedRateValueAry count];i++)
    {
        for(int j=0;j<[appdel.mAllRecipeAry count];j++)
        {
            NSMutableDictionary *foundDic=[[NSMutableDictionary alloc] init];
            NSLog(@"mSelectedRateValueAry =%@ and appdel.mAllRecipeAry=%@",[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_UserID"],[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_ID"]);
            if([[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_ID"] isEqualToString:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_ID"]])
            {
                NSLog(@"yes found match from fav");
                
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_Title"] forKey:@"selection_Title"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_BigImgURL"] forKey:@"selection_BigImgURL"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_CategoryId"] forKey:@"selection_CatId"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_ID"] forKey:@"selection_Id"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_ingredients"] forKey:@"selection_ingredients"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_LastUpdate"] forKey:@"selection_Lastupdate"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_LongDes"] forKey:@"selection_LongDes"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_ShortDes"] forKey:@"selection_ShortDes"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_SmallImgURL"] forKey:@"selection_SmallImgURL"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Recipe_steps"] forKey:@"selection_Step"];
              //  [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_Steps"] forKey:@"selection_Step"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_TotalMinutes"] forKey:@"selection_TotalMin"];
                [foundDic setObject:[[appdel.mAllRecipeAry objectAtIndex:j] valueForKey:@"Rec_DefaultPortion"]forKey:@"selection_DefaultPortion"];
                [foundDic setObject:[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_IsFav"] forKey:@"selection_IsFav"];
                [foundDic setObject:[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_Rate"] forKey:@"selection_IsRate"];
                [mSelectionAry addObject:foundDic];
            }
            else
            {
                NSLog(@"not found match from fav");
            }
        }
    }
    NSLog(@"now mSelectionAry array is >>%@",mSelectionAry);
     [mlblPageControl setNumberOfPages:[mSelectionAry count]];
}

-(void)methodSelectForUserLogin
{
  //  [mSelectedRecipeIdAry removeAllObjects];
    [mSelectedRateValueAry removeAllObjects];
    NSLog(@"in method select for perticular user login");
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"in select database path>>>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        NSLog(@"file not exist  perticular user login");
    }
    else
    {
        NSLog(@"file exist  perticular user login");
    }
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"in select page dbpath fav rate->>%s",dbpath);
    sqlite3_stmt    *statement;
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select * from LOGGEDINUSERDETAILS where USER_ID=%@",appdel.mLoginId];
        NSLog(@"The Data = %@",querySQL);
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"show data char is -->>>>%s",query_stmt);
       
        if (sqlite3_prepare_v2(cookLoginDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"111###");
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                 NSMutableDictionary *favDic=[[NSMutableDictionary alloc] init];
                NSLog(@"in row");
                NSLog(@"select at perticular login 0 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]);
                NSLog(@"select at perticular login 1 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]);
                NSLog(@"select at perticular login 2 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]);
                NSLog(@"select at perticular login 3 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)]);
                [mSelectedRecipeIdAry addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]];
                
                [favDic setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)] forKey:@"Fav_Rate"];
                [favDic setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)] forKey:@"Fav_ID"];
                [favDic setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)] forKey:@"Fav_UserID"];
                [favDic setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)] forKey:@"Fav_IsFav"];
                [mSelectedRateValueAry insertObject:favDic atIndex:[mSelectedRateValueAry count]];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(cookLoginDB);
}

-(void)categorySelected
{
    NSLog(@"val category is >>%@",appdel.valCategory);
      NSMutableDictionary *myDic=[[NSMutableDictionary alloc] init];
    for(int i=0;i<[appdel.mAllRecipeAry count];i++)
    {
        if([[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_CategoryId"] isEqualToString:appdel.valCategory])
        {
            NSMutableDictionary *catMatchedDic=[[NSMutableDictionary alloc] init];
            NSLog(@"yes category matched");
            NSLog(@" matched category recipe name>>%@",[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_Title"]);
            count=count+1;
//            [myDic setValue:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_Title"] forKey:@"Recipe_Filter_Title"];
            myDic=[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_Title"];
            
//            if([[appdel.mCategoryAry valueForKey:@"Category_Id"] containsObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_Title"]])
//            {
//                NSLog(@"category mathced...##");
//            }
//            else
//            {
               // NSLog(@"category not mathced...##");
                [appdel.mCategorySearchAry insertObject:myDic atIndex:0];
         //   }
            [mRecipeID insertObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_ID"] atIndex:0];
            NSLog(@"matched short des >>%@",[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_ShortDes"]);
            [mshortDesAry insertObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_ShortDes"] atIndex:0];
            [mlongDesAry insertObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_LongDes"] atIndex:0];
            [mBigImgURLAry insertObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_BigImgURL"] atIndex:0];
            [mCategoryIDAry insertObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_CategoryId"] atIndex:0];
            [mLastUpdateAry insertObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_CategoryId"] atIndex:0];
            [mSmallImgAry insertObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_CategoryId"] atIndex:0];
            
            //add in dictionary
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_Title"] forKey:@"catMatched_Rec_title"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_ID"] forKey:@"catMatched_Rec_ID"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_ShortDes"] forKey:@"catMatched_Rec_ShortDes"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_LongDes"] forKey:@"catMatched_Rec_LongDes"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_BigImgURL"] forKey:@"catMatched_Rec_BigImgURL"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_CategoryId"] forKey:@"catMatched_Rec_CategoryId"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_LastUpdate"] forKey:@"catMatched_Rec_LastUpdate"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_SmallImgURL"] forKey:@"catMatched_Rec_SmallImgURL"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_ingredients"] forKey:@"catMatched_Rec_Ingredients"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Recipe_steps"] forKey:@"catMatched_Rec_Steps"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_TotalMinutes"] forKey:@"catMatched_Rec_TotalMin"];
            [catMatchedDic setObject:[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_DefaultPortion"] forKey:@"catMatched_Rec_DefaultPortion"];


//            [mMatchedAry addObject:catMatchedDic];
            [mMatchedAry insertObject:catMatchedDic atIndex:0];
        }
        else
        {
            NSLog(@"no category not matched");
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([appdel.whichBtnClicked isEqualToString:@"btnCategory"])
    {
        return [mRecipeID count];
    }
    else if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
    {
        return [mSelectionAry count];
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if([appdel.whichBtnClicked isEqualToString:@"btnCategory"])
    {
         NSLog(@"visible cell ...in btnCategory");
        if (cell == nil) {
            NSLog(@"when cell is nil....");
            
            // [self paginationEnable];
            
            //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"mCustCell" owner:self options:nil];
            cell = (UITableViewCell *)[nib objectAtIndex:0];
        }
        cell.contentView.transform=CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        {
//        }
//        else
//        {
//            if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0) {
//                NSLog(@"yes 7.0 predict table");
//                
//                if([[UIScreen mainScreen] bounds].size.height == 568.0)
//                {
//                    NSLog(@"screen size 568");
//                    mTxtViewTableDetails.frame=CGRectMake(100, 100, 130, 70);
//                    btnTitleFav.frame=CGRectMake(120, 175, 15, 15);
//                    mImgBack.frame=CGRectMake(-30, 50, 270, 145);
//                    mlblPageControl.frame=CGRectMake(130, 141, 39, 37);
//                    mTableLabel.frame=CGRectMake(-20, 70, 250, 37);
//                    mTitleRecImgCat.frame=CGRectMake(-20, 110, 100, 80);
//                }
//                else if ([[UIScreen mainScreen] bounds].size.height == 480.0)
//                {
//                    NSLog(@"screen size 480");
//                    NSLog(@"yes 6.0 predict table cellfor row category");
//                    
//                    mTxtViewTableDetails.frame=CGRectMake(100, 100, 130, 70);
//                    btnTitleFav.frame=CGRectMake(120, 175, 15, 15);
//                    mImgBack.frame=CGRectMake(-100, 70, 270, 115);
//                    
//                    mlblPageControl.frame=CGRectMake(130, 141, 39, 37);
//                    mTableLabel.frame=CGRectMake(-20, 70, 250, 37);
//                    mTitleRecImgCat.frame=CGRectMake(-20, 110, 100, 80);
//                }
//            }
//            else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0) {
//                
//            }
//        }
        //mImgBack.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
        //        mImgBack.frame=CGRectMake(-45, 45, 300, 215);
       // mImgBack.frame=CGRectMake(0, 0, 115, 270);
       // mTableLabel.frame=CGRectMake(-40, 110, 250, 37);
        mTableLabel.lineBreakMode = NSLineBreakByCharWrapping;
        // allows the UILabel to display an unlimited number of lines
        mTableLabel.numberOfLines = 3;
       // mTableLabel.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
        // mTableLabel.frame=CGRectMake(-40, -20, 250, 215);
        [mTableLabel setFont:[UIFont boldSystemFontOfSize:13]];
       // mTxtViewTableDetails.frame=CGRectMake(20,80, 100,50);
       // mTxtViewTableDetails.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(20,30, 50, 250)];
//        view.backgroundColor = [UIColor redColor];
//        [cell addSubview:view];
//       // view.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        UITextView *textView = [[ UITextView alloc]initWithFrame:CGRectMake(0,0, 170, 50)];
//        textView.text=[mshortDesAry objectAtIndex:indexPath.row];
//        textView.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        [view addSubview:textView];
        
        //
       
        NSLog(@"TEXT VIEW FRAME IS:%@",NSStringFromCGRect(mTxtViewTableDetails.frame));
        
        mTableLabel.text=[appdel.mCategorySearchAry objectAtIndex:indexPath.row];
        mTxtViewTableDetails.text=[mshortDesAry objectAtIndex:indexPath.row];
        NSLog(@"text to display=%@",[mshortDesAry objectAtIndex:indexPath.row]);
        // btnTitleFav.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
       // btnTitleFav.frame=CGRectMake(0, 125, 15, 15);
       // mTitleRecImgCat.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
       // mTitleRecImgCat.frame=CGRectMake(10, 10, 50, 70);
       // mViewCust.frame=CGRectMake(0, 0, mViewCust.frame.size.width, mViewCust.frame.size.height);
       // mViewCust.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
       
        
        NSLog(@"My view frame cellforrow: %@", NSStringFromCGRect(mlblPageControl.frame));
         //mlblPageControl.frame=CGRectMake(130, 131, 39, 37);
        
        NSLog(@"current cell recipe id>>%@",[mRecipeID objectAtIndex:indexPath.row]);
        mFav_RecipeId=[mRecipeID objectAtIndex:indexPath.row];
        
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"recipe_id_%@.jpg",[mRecipeID objectAtIndex:indexPath.row]]];
       // [mCatRecipeImg setImage:image];
        [mTitleRecImgCat setImage:image forState:UIControlStateNormal];
   //     mTitleRecImgCat.frame=CGRectMake(-20, 110, 100, 80);
        NSLog(@"recipe id btncategory %@",[mRecipeID objectAtIndex:indexPath.row] );
        CALayer *btnLayer = [mTitleRecImgCat layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:5.0f];
        
        mCount=0;
        //    for(int i=0;i<[mSelectedRecipeIdAry count];i++)
        for(int i=0;i<[mSelectedRateValueAry count];i++)
        {
            if([[mRecipeID objectAtIndex:indexPath.row] isEqualToString:[NSString stringWithFormat:@"%@",[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_ID"]]])
            {
                NSLog(@"matched recipe id with selected");
                [btnTitleFav setImage:[UIImage imageNamed:@"Fav.png"] forState:UIControlStateNormal];
                
                getRate=[[[mSelectedRateValueAry objectAtIndex:i] valueForKey:@"Fav_Rate"] floatValue];
                NSLog(@"getRate value in cellforrow is >>%f",getRate);
                if(getRate==0.50)
                {
                    NSLog(@"rate 0.50 ");
                    [btnTitle1 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==1.00)
                {
                    NSLog(@"rate 1.00");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==1.50)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==2.00)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==2.50)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==3.00)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==3.50)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                }
                else   if(getRate==4.00)
                {
                    NSLog(@"rate 1.50");
                    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle4 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                    [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
                }
                //            if(fav==1)
                //            {
                //               favVal=1;
                //            }
                //            else if(fav==0)
                //            {
                //                favVal=0;
                //            }
                favVal=1;
            }
            else
            {
                NSLog(@"not matched recipe id with selected");
                mCount=mCount+1;
            }
            
        }
        NSLog(@"mcount is %d",mCount);
        //    if(mCount==[mSelectedRecipeIdAry count])
        if(mCount==[mSelectedRateValueAry count])
        {
            favVal=0;
        }
        NSLog(@"fav value is in cellforrow.............................##%d",favVal);
        btnTitleFav.tag=indexPath.row;
        btnTitleInfo.tag=indexPath.row;
        mTitleRecImgCat.tag=indexPath.row;
    }
    else if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
    {
         NSLog(@"visible cell ...in btnSelect");
        if (cell == nil) {
            NSLog(@"when cell is nil....");
            
            // [self paginationEnable];
            
            //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"mCustCell" owner:self options:nil];
            cell = (UITableViewCell *)[nib objectAtIndex:0];
        }
        cell.contentView.transform=CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        mImgBack.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        mImgBack.frame=CGRectMake(0, 0, 115, 270);
//        mTableLabel.frame=CGRectMake(-40, 110, 250, 37);
//        mTableLabel.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        mTxtViewTableDetails.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        mTxtViewTableDetails.frame=CGRectMake(15, 100, 45,140);
//        
//        
//        btnTitleFav.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        btnTitleFav.frame=CGRectMake(0, 125, 15, 15);
//        mTitleRecImgCat.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        mTitleRecImgCat.frame=CGRectMake(10, 10, 50, 70);
//        mViewCust.frame=CGRectMake(0, 0, mViewCust.frame.size.width, mViewCust.frame.size.height);
//        mViewCust.transform =CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//        mlblPageControl.frame=CGRectMake(130, 131, 39, 37);
        
        
        
        
        mTableLabel.text=[[mSelectionAry objectAtIndex:indexPath.row] valueForKey:@"selection_Title"];
         mTxtViewTableDetails.text=[[mSelectionAry objectAtIndex:indexPath.row] valueForKey:@"selection_ShortDes"];

       // mViewCust.frame=CGRectMake(0, 0, mViewCust.frame.size.width, mViewCust.frame.size.height);
              mTableLabel.lineBreakMode = NSLineBreakByCharWrapping;
        mTableLabel.numberOfLines = 3;
        [mTableLabel setFont:[UIFont boldSystemFontOfSize:13]];
        
//        btnTitle1.frame=CGRectMake(183, 120, 20, 20);
//        btnTitle2.frame=CGRectMake(213, 120, 20, 20);
//        btnTitle3.frame=CGRectMake(243, 120, 20, 20);
//        btnTitle4.frame=CGRectMake(273, 120, 20, 20);
        
        
     //   mTxtViewTableDetails.frame=CGRectMake(153, 160, 150, 100);
       
        
        
//        mTxtViewTableDetails.frame=CGRectMake(140, 140, 130, 70);
//        btnTitleFav.frame=CGRectMake(165, 218, 15, 15);
//        btnTitleInfo.frame=CGRectMake(193, 270, 20, 20);
//        mCatRecipeImg.frame=CGRectMake(5, 170, 140, 130);
        
        
   //     btnTitleFav.frame=CGRectMake(153, 270, 20, 20);
        
        
       
   //     mTitleRecImgCat.frame=CGRectMake(5, 160, 140, 130);
               CALayer *btnLayer = [mTitleRecImgCat layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:5.0f];
        
        
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"recipe_id_%@.jpg",[[mSelectionAry objectAtIndex:indexPath.row] valueForKey:@"selection_Id"]]];
        //[mCatRecipeImg setImage:image];
         [mTitleRecImgCat setImage:image forState:UIControlStateNormal];
        
        NSLog(@"selection rate is=%@ and isFav=%@",[[mSelectionAry objectAtIndex:indexPath.row] valueForKey:@"selection_IsRate"],[[mSelectionAry objectAtIndex:indexPath.row] valueForKey:@"selection_IsFav"]);
        selectnRate=[[[mSelectionAry objectAtIndex:indexPath.row] valueForKey:@"selection_IsRate"] floatValue];
         [btnTitleFav setImage:[UIImage imageNamed:@"Fav.png"] forState:UIControlStateNormal];
        if(selectnRate==0.50)
        {
            NSLog(@"rate 0.50 ");
            [btnTitle1 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        }
        else   if(selectnRate==1.00)
        {
            NSLog(@"rate 1.00");
            [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        }
        else   if(selectnRate==1.50)
        {
            NSLog(@"rate 1.50");
            [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        }
        else   if(selectnRate==2.00)
        {
            NSLog(@"rate 1.50");
            [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        }
        else   if(selectnRate==2.50)
        {
            NSLog(@"rate 1.50");
            [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
        }
        else   if(selectnRate==3.00)
        {
            NSLog(@"rate 1.50");
            [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
        }
        else   if(selectnRate==3.50)
        {
            NSLog(@"rate 1.50");
            [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
        }
        else   if(selectnRate==4.00)
        {
            NSLog(@"rate 1.50");
            [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
        }
        btnTitleFav.tag=indexPath.row;
        btnTitleInfo.tag=indexPath.row;
        mTitleRecImgCat.tag=indexPath.row;
    }
      return cell;
}

-(void)paginationEnable
{
//    if (countTableCell==[mshortDesAry count])
//    {
//        b=0;
//        [self.mTableCategory setContentOffset:CGPointMake(0, b)];
//        // mPageControl.currentPage=mPageControl.currentPage+1;
//        countTableCell=0;
//        [mlblPageControl setCurrentPage:0];
//        // mViewCustCell.frame =CGRectMake(0, 0, 320, 320);
//        mViewCust.frame=CGRectMake(0, 0, 215  , 302);
//        NSLog(@"11");
//    }
//    else
//    {
//        [self.mTableCategory setContentOffset:CGPointMake(0, b) animated:YES];
//        countTableCell=countTableCell+1;
//        [mlblPageControl setCurrentPage:countTableCell-1];
//        b=b+304;
//        // mViewCustCell.frame =CGRectMake(0, 0, 320, 320);
//        mViewCust.frame=CGRectMake(0, 0, 215, 302);
//        NSLog(@"22");
//    }
    
    NSLog(@"mshortDesAry array count %d",[mshortDesAry count]);
    
    
//    if(pagecount<[mshortDesAry count])
//    {
//        NSLog(@"pagecount is >>%d",pagecount);
//        [mlblPageControl setCurrentPage:pagecount];
//        pagecount=pagecount+1;
//        if(pagecount == [mshortDesAry count]-1)
//        {
//            //pagecount=0;
//        }
//    }
//    else if(pagecountRev==[mshortDesAry count])
//    {
//        NSLog(@"pagecountRev equal to array>>%d",pagecountRev);
//        pagecountRev=pagecountRev-1;
//        NSLog(@"pagecountRev after>>%d",pagecountRev);
//        [ mlblPageControl setCurrentPage:pagecountRev];
//    }
//    else if(pagecountRev<[mshortDesAry count])
//    {
//        NSLog(@"pagecountRev less than to array>>%d",pagecountRev);
//        pagecountRev=pagecountRev-1;
//        NSLog(@"pagecountRev after>>%d",pagecountRev);
//        [ mlblPageControl setCurrentPage:pagecountRev];
//        if([mshortDesAry count]-[mshortDesAry count]-1 )
//        {
//            //pagecountRev=[mshortDesAry count];
//        }
//    }

    
    if(pagecount<[mshortDesAry count])
    {
        NSLog(@"pagecount is >>%d",pagecount);
        [mlblPageControl setCurrentPage:pagecount];
        pagecount=pagecount+1;
        NSLog(@"pagecount after is >>%d",pagecount);
    }
    
//    else if(pagecountRev==[mshortDesAry count])
//    {
//        NSLog(@"pagecountRev equal to array>>%d",pagecountRev);
//        pagecountRev=pagecountRev-1;
//        NSLog(@"pagecountRev after>>%d",pagecountRev);
//        [ mlblPageControl setCurrentPage:pagecountRev];
//    }
    else if(pagecountRev<=[mshortDesAry count])
    {
        NSLog(@"pagecountRev less than to array>>%d",pagecountRev);
        pagecountRev=pagecountRev-1;
        NSLog(@"pagecountRev after>>%d",pagecountRev);
        [ mlblPageControl setCurrentPage:pagecountRev];
        if([mshortDesAry count]-[mshortDesAry count]-1 )
        {
            //pagecountRev=[mshortDesAry count];
        }
    }
     else if(pagecountRev==[mshortDesAry count]-[mshortDesAry count]-1 )
     {
         NSLog(@"####@@@@@@");
         pagecount=0;
     }
    

//   if(pagecount<=[mshortDesAry count]-1)
//   {
//       NSLog(@"pagecount is >>%d",pagecount);
//       [mlblPageControl setCurrentPage:pagecount];
//       pagecount=pagecount+1;
//       if(pagecount == [mshortDesAry count]-1)
//       {
//           //pagecount=0;
//       }
//   }
//    else if(pagecountRev==[mshortDesAry count])
//    {
//        NSLog(@"pagecountRev equal to array>>%d",pagecountRev);
//        pagecountRev=pagecountRev-1;
//        NSLog(@"pagecountRev after>>%d",pagecountRev);
//         [ mlblPageControl setCurrentPage:pagecountRev];
//    }
//    else if(pagecountRev<[mshortDesAry count])
//    {
//        NSLog(@"pagecountRev less than to array>>%d",pagecountRev);
//        pagecountRev=pagecountRev-1;
//        NSLog(@"pagecountRev after>>%d",pagecountRev);
//        [ mlblPageControl setCurrentPage:pagecountRev];
//        if([mshortDesAry count]-[mshortDesAry count]-1 )
//        {
//            //pagecountRev=[mshortDesAry count];
//        }
//    }
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if([appdel.whichBtnClicked isEqualToString:@"btnCategory"])
    {
        NSArray *visible = [mTableCategory indexPathsForVisibleRows];
        NSIndexPath *rowValue = (NSIndexPath*)[visible objectAtIndex:0];
        [mlblPageControl setCurrentPage:rowValue.row];
    }
    else if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
    {
        NSArray *visible = [mTableCategory indexPathsForVisibleRows];
        NSLog(@"btnSelect visible array is %@",visible);
        NSIndexPath *rowValue = (NSIndexPath*)[visible objectAtIndex:0];
        [mlblPageControl setCurrentPage:rowValue.row];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"#####*****######");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnPageControl:(id)sender
{
    NSLog(@"page id >>%d",mlblPageControl.currentPage);
}
- (IBAction)btn1Tapped:(id)sender {
    
    UIButton* myButton = (UIButton*)sender;
    NSLog(@"btn tag..%d",myButton.tag);
    if(myButton.tag==1)
    {
      
    }
}
- (IBAction)btnFirst:(id)sender forEvent:(UIEvent *)event
{
    [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    btnTitle1=(UIButton *)sender;
    NSLog(@"btn width...%f",btnTitle1.frame.size.width);
    NSLog(@"btn frame is ...%f and %f",btnTitle1.frame.origin.x,btnTitle1.frame.origin.y);
    
    NSSet *touches = [event touchesForView:sender];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:sender];
    point=touchPoint;
    NSLog(@"ABCDEFGH........%@", NSStringFromCGPoint(point));
    
    if(touchPoint.x<=5)
    {
        NSLog(@"Banana");
        [btnTitle1 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        f=0.0;
    }
    if(touchPoint.x>5 && (touchPoint.x<(btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Mango");
        [btnTitle1 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
        f=0.5;
    }
    if(touchPoint.x>((btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Apple");
        [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
        f=1.0;
    }
    totalFloat=f;
   // mTotalRate.text=[NSString stringWithFormat:@"%.2f",totalFloat];
    NSLog(@"totalFloat is =%f",totalFloat);
    finalStarValue=totalFloat;
    NSLog(@"Now finalStarValue is%f",finalStarValue);

}
- (IBAction)btnSecond:(id)sender forEvent:(UIEvent *)event {
    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
    btnTitle2=(UIButton *)sender;
    NSLog(@"btn width...%f",btnTitle2.frame.size.width);
    NSLog(@"btn frame is ...%f and %f",btnTitle2.frame.origin.x,btnTitle2.frame.origin.y);
    
    NSSet *touches = [event touchesForView:sender];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:sender];
    point=touchPoint;
    NSLog(@"ABCDEFGH........%@", NSStringFromCGPoint(point));
    //[self loop];
    if(touchPoint.x<=5)
    {
        NSLog(@"Banana");
        [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        f1=0.0;
    }
    if(touchPoint.x>5 && (touchPoint.x<(btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Mango");
        [btnTitle2 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
        f1=0.5;
    }
    if(touchPoint.x>((btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Apple");
        [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
        f1=1.0;
    }
    totalFloat=1.0+f1;
 //   mTotalRate.text=[NSString stringWithFormat:@"%.2f",totalFloat];
     NSLog(@"totalFloat is =%f",totalFloat);
    finalStarValue=totalFloat;
}
- (IBAction)btnThird:(id)sender forEvent:(UIEvent *)event {
    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
    [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    btnTitle3=(UIButton *)sender;
    NSLog(@"btn width...%f",btnTitle3.frame.size.width);
    NSLog(@"btn frame is ...%f and %f",btnTitle3.frame.origin.x,btnTitle3.frame.origin.y);
    
    NSSet *touches = [event touchesForView:sender];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:sender];
    point=touchPoint;
    NSLog(@"ABCDEFGH........%@", NSStringFromCGPoint(point));
    //[self loop];
    if(touchPoint.x<=5)
    {
        NSLog(@"Banana");
        [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        f2=0.0;
    }
    if(touchPoint.x>5 && (touchPoint.x<(btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Mango");
        [btnTitle3 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
        f2=0.5;
    }
    if(touchPoint.x>((btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Apple");
        [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
        f2=1.0;
    }
    totalFloat=2.0+f2;
  //  mTotalRate.text=[NSString stringWithFormat:@"%.2f",totalFloat];
     NSLog(@"totalFloat is =%f",totalFloat);
    finalStarValue=totalFloat;
}
- (IBAction)btnFourth:(id)sender forEvent:(UIEvent *)event {
    [btnTitle3 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
    [btnTitle2 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
    [btnTitle1 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
    btnTitle4=(UIButton *)sender;
    NSLog(@"btn width...%f",btnTitle4.frame.size.width);
    NSLog(@"btn frame is ...%f and %f",btnTitle4.frame.origin.x,btnTitle4.frame.origin.y);
    
    NSSet *touches = [event touchesForView:sender];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:sender];
    point=touchPoint;
    NSLog(@"ABCDEFGH........%@", NSStringFromCGPoint(point));
    //[self loop];
    if(touchPoint.x<=5)
    {
        NSLog(@"Banana");
        [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
        f4=0.0;
    }
    if(touchPoint.x>5 && (touchPoint.x<(btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Mango");
        [btnTitle4 setImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
        f4=0.5;
    }
    if(touchPoint.x>((btnTitle1.frame.size.width)/2))
    {
        NSLog(@"Apple");
        [btnTitle4 setImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];
        f4=1.0;
    }
    totalFloat=3.0+f4;
  //  mTotalRate.text=[NSString stringWithFormat:@"%.2f",totalFloat];
    NSLog(@"totalFloat is =%f",totalFloat);
    finalStarValue=totalFloat;
}

- (IBAction)btnSelectedRecipes:(id)sender {
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please login to access the page" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSLog(@"11111111111");
        appdel.whichBtnClicked=@"btnSelect";
        [self methodLoggedInUserFav];
        mViewCategorisedRecipe.hidden=FALSE;
        mViewSingleRecipe.hidden=TRUE;
        [self.view addSubview:mViewCategorisedRecipe];
        // [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
        [mTableCategory reloadData];
        
      //  mViewCust.frame=CGRectMake(-40, -40, mViewCust.frame.size.width, mViewCust.frame.size.height);
        //  mViewCust.frame=CGRectMake(10,0, 50, 45);
        [self.view addSubview:mViewCategorisedRecipe];
        // [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
        NSLog(@"table frame is >>%@",NSStringFromCGRect(mTableCategory.frame));
        NSLog(@"view frame is >>%@",NSStringFromCGRect(mViewCust.frame));
        
//        mTableCategory.frame=CGRectMake(20, 0, 270, 135);
      //  mTableCategory.frame=CGRectMake(20, -50, 270, 225);
        mViewCust.frame=CGRectMake(-40, -40, mViewCust.frame.size.width, mViewCust.frame.size.height);
        mViewCategorisedRecipe.hidden=FALSE;
        
        //  mViewCust.frame=CGRectMake(10,0, 50, 45);
        [self.view addSubview:mViewCategorisedRecipe];
        // [mViewCategorisedRecipe setCenter:CGPointMake(160, 430)];
        mTableCategory.frame=CGRectMake(15, 20, 270, 115);
        NSLog(@"table frame is >>%@",NSStringFromCGRect(mTableCategory.frame));
        NSLog(@"view frame is >>%@",NSStringFromCGRect(mViewCust.frame));
        
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
        }
        else
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0)
            {
                NSLog(@"yes 7.0 predict table");
                
                if([[UIScreen mainScreen] bounds].size.height == 568.0)
                {
                    NSLog(@"screen size 568");
                    mViewCategorisedRecipe.frame=CGRectMake(10,335, 300, 165);
                    mlblPageControl.frame=CGRectMake(130, 133, 39, 37);
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 480.0)
                {
                    NSLog(@"screen size 480");
                    mViewCategorisedRecipe.frame=CGRectMake(10,275, 300, 155);
                    mlblPageControl.frame=CGRectMake(130, 125, 39, 37);
                }
            }
            else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0)
            {
                
            }
        
        
        }

    }
}

- (IBAction)btnAssistant:(id)sender {
    NSLog(@"assistant tap");
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"PLease Login to access this page." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            FormViewController *form=[[FormViewController alloc] initWithNibName:@"FormViewController~iPad" bundle:nil];
            [self.navigationController pushViewController:form animated:YES];
        }
        else
        {
            FormViewController *reg=[[FormViewController alloc] initWithNibName:@"FormViewController~iPhone" bundle:nil];
            [self.navigationController pushViewController:reg animated:YES];
        }
    }
}

- (IBAction)btnFavTap:(id)sender
{
    if([appdel.mLoginId length]==0)
    {
        NSLog(@"user not logged in");
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please login to add favourite" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSLog(@"user logged in");
        NSLog(@"If loggedin favval is %d",favVal);
        if(favVal==0)
        {
            if([appdel.whichSearch isEqualToString:@"keywordSearch"])
            {
                NSLog(@"btn fav for keyword");
            }
            else if([appdel.whichSearch isEqualToString:@"categorySearch"])
            {
                UIButton *btn=sender;
                NSLog(@"indexpath is >>%d",btn.tag);
                NSLog(@"fav recipe id is ...%@",[mRecipeID objectAtIndex:btn.tag]);
            }
            NSLog(@"favVal for fav is....................... %d",favVal);
            [btnTitleFav setImage:[UIImage imageNamed:@"Fav.png"] forState:UIControlStateNormal];
            favVal=1;
            fav=1;
            mFav_Rate=totalFloat;
            [self methodInsertFav];
            [self methodSelectFavoStarRate];
            [self methodSelectForUserLogin];
            NSLog(@"add already fav recipe id =>%@",mSelectedRecipeIdAry);
            NSLog(@"add already  rate =>%@",mSelectedRateValueAry);
        }
        else if (favVal==1)
        {
            NSLog(@"favVal for not fav is...................... %d",favVal);
            [btnTitleFav setImage:[UIImage imageNamed:@"notFav.png"] forState:UIControlStateNormal];
            [self methodDeleteFav];
            [self methodSelectFavoStarRate];
            [self methodSelectForUserLogin];
             NSLog(@"delete already fav recipe id =>%@",mSelectedRecipeIdAry);
             NSLog(@"deleted already  rate =>%@",mSelectedRateValueAry);
            [btnTitle1 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle2 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle4 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            [btnTitle3 setImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
            favVal=0;
            fav=0;
        }
    }
}
-(void)methodInsertFav
{
    NSLog(@"loginid= %@, recipeid=%@, fav=%d, rate=%f",appdel.mLoginId,mFav_RecipeId,fav,mFav_Rate);
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    sqlite3_stmt    *statement;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSLog(@"100000000");
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO LOGGEDINUSERDETAILS (USER_ID,RECIPE_ID,ISFAV,RATE) VALUES (\"%@\",\"%@\",\"%d\",\"%f\")",appdel.mLoginId,mFav_RecipeId,fav,mFav_Rate];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(cookLoginDB, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Contact added for fav and rate");
            
        } else {
            NSLog(@" Failed to add contact for fav and rate");
        }
        sqlite3_finalize(statement);
        sqlite3_close(cookLoginDB);
    }
}
-(void)methodSelectFavoStarRate
{
    NSLog(@"in method select");
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"in select database path>>>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        NSLog(@"file not exist fav rate");
    }
    else
    {
        NSLog(@"file exist fav rate");
    }
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"in select page dbpath fav rate->>%s",dbpath);
    sqlite3_stmt    *statement;
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select * from LOGGEDINUSERDETAILS"];
        NSLog(@"The Data = %@",querySQL);
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"show data char is -->>>>%s",query_stmt);
        if (sqlite3_prepare_v2(cookLoginDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"111###");
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSLog(@"in row");
                NSLog(@"methodSelectFavoStarRate select at 0 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]);
                NSLog(@"methodSelectFavoStarRate select at 1 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]);
                NSLog(@"methodSelectFavoStarRate select at 2 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]);
                NSLog(@"methodSelectFavoStarRate select at 3 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)]);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(cookLoginDB);
}
-(void)methodDeleteFav
{
    NSLog(@"methodDeleteFav loginid= %@, recipeid=%@, fav=%d, rate=%f",appdel.mLoginId,mFav_RecipeId,fav,mFav_Rate);
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    sqlite3_stmt    *statement;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSLog(@"100000000");
        NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM LOGGEDINUSERDETAILS where RECIPE_ID=%@",mFav_RecipeId];
        
        const char *insert_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(cookLoginDB, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Contact deleted for fav and rate");
            
        } else {
            NSLog(@" Failed to delete contact for fav and rate");
        }
        sqlite3_finalize(statement);
        sqlite3_close(cookLoginDB);
    }
}
//Info button tapped
- (IBAction)btnInfoTapped:(id)sender
{
    UIButton *btn=sender;
    NSLog(@"info indexpath is >>%d",btn.tag);
    if([appdel.whichBtnClicked isEqualToString:@"btnCategory"])
    {
        NSLog(@"name of recipe-->>%@",[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_title"]);
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_title"] forKey:@"details_Title"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_BigImgURL"] forKey:@"details_BigImgURL"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_CategoryId"] forKey:@"details_CatId"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_ID"] forKey:@"details_Id"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_Ingredients"] forKey:@"details_ingredients"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_LastUpdate"] forKey:@"details_Lastupdate"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_LongDes"] forKey:@"details_LongDes"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_ShortDes"] forKey:@"details_ShortDes"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_SmallImgURL"] forKey:@"details_SmallImgURL"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_Steps"] forKey:@"details_Step"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_Steps"] forKey:@"details_Step"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_TotalMin"] forKey:@"details_TotalMin"];
        [appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_DefaultPortion"] forKey:@"details_DefaultPortion"];
        for(int i=0;i<[mSelectedRecipeIdAry count];i++)
        {
            if([[mSelectedRecipeIdAry objectAtIndex:i] isEqualToString:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_ID"]])
            {
                appdel.recipeIsFav=@"yesInFavList";
                NSLog(@"yesInFavList");
            }
            else
            {
                appdel.recipeIsFav=@"notInFavList";
                NSLog(@"notInFavList");
            }
        }

    }
    else if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
    {
        NSLog(@"info tapped for selection");
        // [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_Title"] forKey:@"details_Title"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_Title"] forKey:@"details_Title"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_BigImgURL"] forKey:@"details_BigImgURL"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_CatId"] forKey:@"details_CatId"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_Id"] forKey:@"details_Id"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_ingredients"] forKey:@"details_ingredients"];
        
        
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_Lastupdate"] forKey:@"details_Lastupdate"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_LongDes"] forKey:@"details_LongDes"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_ShortDes"] forKey:@"details_ShortDes"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_SmallImgURL"] forKey:@"details_SmallImgURL"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_Step"] forKey:@"details_Step"];
        //[appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_Steps"] forKey:@"details_Step"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_TotalMin"] forKey:@"details_TotalMin"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_DefaultPortion"] forKey:@"details_DefaultPortion"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_IsFav"] forKey:@"details_IsFav"];
        [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_IsRate"] forKey:@"details_Rate"];

    }
       if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DescriptionRecipeViewController *des=[[DescriptionRecipeViewController alloc] initWithNibName:@"DescriptionRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:des animated:YES];
    }
    else
    {
        DescriptionRecipeViewController *des=[[DescriptionRecipeViewController alloc] initWithNibName:@"DescriptionRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:des animated:YES];
    }
}



- (IBAction)mBtnImgsingleRecipe:(id)sender {
    NSLog(@"info tapped for selection mBtnImgsingleRecipe");
    // [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_Title"] forKey:@"details_Title"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_Title"] forKey:@"details_Title"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_BigImgURL"] forKey:@"details_BigImgURL"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_CategoryId"] forKey:@"details_CatId"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ID"] forKey:@"details_Id"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ingredients"] forKey:@"details_ingredients"];
    
    
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_LastUpdate"] forKey:@"details_Lastupdate"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_LongDes"] forKey:@"details_LongDes"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ShortDes"] forKey:@"details_ShortDes"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_SmallImgURL"] forKey:@"details_SmallImgURL"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Recipe_steps"] forKey:@"details_Step"];
    //[appdel.mDetailDic setObject:[[mMatchedAry objectAtIndex:btn.tag] valueForKey:@"catMatched_Rec_Steps"] forKey:@"details_Step"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_TotalMinutes"] forKey:@"details_TotalMin"];
    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_DefaultPortion"] forKey:@"details_DefaultPortion"];
//    [appdel.mDetailDic setObject:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_DefaultPortion"] forKey:@"details_IsFav"];
//    [appdel.mDetailDic setObject:[[mSelectionAry objectAtIndex:btn.tag] valueForKey:@"selection_IsRate"] forKey:@"details_Rate"];
    
    
    for(int i=0;i<[mSelectedRecipeIdAry count];i++)
    {
        if([[mSelectedRecipeIdAry objectAtIndex:i] isEqualToString:[[appdel.mAllRecipeAry objectAtIndex:appdel.mIndexpath] valueForKey:@"Rec_ID"]])
        {
            appdel.recipeIsFavSingle=@"yesInFavListSingle";
            NSLog(@"yesInFavListSingle");
        }
        else
        {
            appdel.recipeIsFav=@"notInFavList";
            NSLog(@"notInFavList");
        }
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DescriptionRecipeViewController *des=[[DescriptionRecipeViewController alloc] initWithNibName:@"DescriptionRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:des animated:YES];
    }
    else
    {
        DescriptionRecipeViewController *des=[[DescriptionRecipeViewController alloc] initWithNibName:@"DescriptionRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:des animated:YES];
    }

}
- (IBAction)btnRecImgCatTapped:(id)sender {
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
