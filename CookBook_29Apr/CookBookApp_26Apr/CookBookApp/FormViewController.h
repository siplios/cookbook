//
//  FormViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 14/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "ViewController.h"
#import "DetailsRecipeViewController.h"

@interface FormViewController : UIViewController<UIPickerViewDelegate,UIActionSheetDelegate,NSXMLParserDelegate>
{
    int btnVal1;
    int btnVal2;
    int btnVal3;
    int btnVal4;
    int btnVal5;
    int btnVal6;
    int btnKids;
    int yesKids;
    int noKids;
    int mon;
    int tue;
    int wed;
    int thu;
    int fri;
    int sat;
    int sun;
    int all;
    
    int beefRecipe;
    int chickenRecipe;
    int porkRecipe;
    int fishRecipe;
    int pastaRecipe;
    int vegRecipe;
    int childrenRecipe;
    int complexRecipe;
    int easyRecipe;
    int moderateRecipe;
    int howManyEating;
    NSString *dateTime;
    int allDay;
    int sunDay;
    int monDay;
    int tueDay;
    int wedDay;
    int thuDay;
    int friDay;
    int satDay;
    int isSuscriber;
    BOOL *recordResults;
    
    NSMutableData *webData;
    
    sqlite3 *cookLoginDB;
    NSString        *databasePath;
    
    NSString *str;

}
@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *mlblUsernam;
@property (strong, nonatomic)NSMutableArray *mPeopleAry;
@property (strong, nonatomic)NSMutableArray *mTimeAry;
@property (strong, nonatomic)UIView *view1;
@property(strong,nonatomic)UIPickerView *mypicker;
@property (strong, nonatomic) IBOutlet UIButton *btnTitlePeople;
@property (strong, nonatomic)NSString *whichBtn;
@property (strong, nonatomic) IBOutlet UIButton *mTitleTimeBtn;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect1;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect2;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect3;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect4;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect5;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect6;
@property (strong, nonatomic) IBOutlet UIButton *btnYesKids;
@property (strong, nonatomic) IBOutlet UIButton *btnCrossKids;
@property (strong, nonatomic) IBOutlet UIButton *btnYesDisPrepare;
@property (strong, nonatomic) IBOutlet UIButton *btnNoDishPrepare;
@property (strong, nonatomic) IBOutlet UIButton *btnMon;
@property (strong, nonatomic) IBOutlet UIButton *btnTues;
@property (strong, nonatomic) IBOutlet UIButton *btnWed;
@property (strong, nonatomic) IBOutlet UIButton *btnThurs;
@property (strong, nonatomic) IBOutlet UIButton *btnFri;
@property (strong, nonatomic) IBOutlet UIButton *btnSat;
@property (strong, nonatomic) IBOutlet UIButton *btnSun;
@property (strong, nonatomic) IBOutlet UIButton *btnAll;

//Parsing
@property(nonatomic, retain) NSXMLParser *xmlParser;
@property(nonatomic, retain) NSMutableString *soapResults;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleAssistant;
@property (strong, nonatomic) IBOutlet UIImageView *mUserImage;
@property (strong, nonatomic) IBOutlet UIImageView *mImgPicNotSet;

- (IBAction)btnHowManyPeopleTap:(id)sender;
- (IBAction)btnTimeTap:(id)sender;
- (IBAction)btnActivateDanica:(id)sender;
- (IBAction)btnSelectMore:(id)sender;
- (IBAction)btnExit:(id)sender;
- (IBAction)btnKids:(id)sender;
- (IBAction)btnDishTap:(id)sender;
- (IBAction)btnDays:(id)sender;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnSelection:(id)sender;


@end
