//
//  RegistrationViewController.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 28/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "RegistrationViewController.h"
#import "AppDelegate.h"
#import <Twitter/Twitter.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
static NSString * const kClientID =
@"931972229113-34sotf7kgqblo5mbgoskkqg75lab78o4.apps.googleusercontent.com";
AppDelegate *appdel;

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController
@synthesize mTxtEmailid;
@synthesize mTxtName;
@synthesize mTxtPassword;
@synthesize mTxtRepeatPassword;
@synthesize xmlParser;
@synthesize soapResults;
@synthesize mScrollRegi;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [mScrollRegi setContentSize:CGSizeMake(200, 700)];
    appdel=[[UIApplication sharedApplication] delegate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag==2 || textField.tag==3 || textField.tag==4)
    {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
        }
        else
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0)
            {
                NSLog(@"yes 7.0 predict table");
                
                if([[UIScreen mainScreen] bounds].size.height == 568.0)
                {
                    NSLog(@"screen size 568 mViewSingleRecipe");
                    CGRect rc=[textField bounds];
                    rc=[textField convertRect:rc toView:mScrollRegi];
                    CGPoint pt;
                    pt=rc.origin;
                    pt.x=0;
                    pt.y-=30;
                    [mScrollRegi setContentOffset:pt animated:YES];
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 480.0)
                {
                    NSLog(@"screen size 480 mViewSingleRecipe");
                    CGRect rc=[textField bounds];
                    rc=[textField convertRect:rc toView:mScrollRegi];
                    CGPoint pt;
                    pt=rc.origin;
                    pt.x=0;
                    pt.y-=5;
                    [mScrollRegi setContentOffset:pt animated:YES];
                }
            }
            else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0)
            {
                
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
     [mScrollRegi setContentOffset:scroll animated:YES];
    if(textField.tag==1)
    {
        [mTxtEmailid becomeFirstResponder];
         
    }
  else  if(textField.tag==2)
    {
        [self mCheckEmail];
        [mTxtPassword becomeFirstResponder];
    }
  else  if(textField.tag==3)
  {
      [mTxtRepeatPassword becomeFirstResponder];
  }
    [textField resignFirstResponder];
    return 0;
}
-(void)mCheckEmail
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if  ([emailTest evaluateWithObject:mTxtEmailid.text] != YES && [mTxtEmailid.text length]!=0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter valid email address" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
}

- (IBAction)btnRegiSubmit:(id)sender {
    if([mTxtName.text length]<1 || [mTxtEmailid.text length]<1|| [mTxtPassword.text length]<1 || [mTxtRepeatPassword.text length]<1)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please enter all the fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        // alert.tag=1;
        [alert show];
    }
    else
    {
        if([mTxtPassword.text isEqualToString:mTxtRepeatPassword.text])
        {
            [self registerUser];
        } else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Retype password does not matched" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        }
        
   }
-(void)registerUser
{
    NSLog(@"registered called");
    if (self.strFacebookId.length==0) {
        self.strFacebookId=@"true";
    }
    if (self.strTwitterId.length==0) {
        self.strTwitterId=@"true";
    }
    if (self.strGooglePlusId.length==0) {
        self.strGooglePlusId=@"true";
    }
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<Registration xmlns=\"http://danica.com.ar/recetario\">"
                             "<user>"
                             "<email>%@</email>"
                             "<firstName>%@</firstName>"
                             "<password>%@</password>"
                             "<gplusId>%@</gplusId>"
                             "<tweeterId>%@</tweeterId>"
                             "<lastName>%@</lastName>"
                             "<facebookId>%@</facebookId>"
                             "</user>"
                             "</Registration>"
                             "</soap:Body>"
                             "</soap:Envelope>",mTxtEmailid.text,mTxtName.text,mTxtPassword.text,self.strGooglePlusId,self.strTwitterId,@" ",self.strFacebookId];
    
    NSLog(@"my soapMessage is->%@",soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://danica.com.ar/cgi-bin/soapserver.pl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@">>.....%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://danica.com.ar/recetario/Registration" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [NSMutableData data] ;
        NSLog(@"Problem");
        [theConnection start];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"ERROR with theConenction");
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"DONE. Received Bytes: %d", [webData length]);
	NSString *theXML = [[NSString alloc] initWithBytes: [webData mutableBytes] length:[webData length] encoding:NSUTF8StringEncoding];
	NSLog(@"---->>%@",theXML);
    
//    if( xmlParser )
//	{
//		[xmlParser release];
//	}
	
	xmlParser = [[NSXMLParser alloc] initWithData: webData];
	[xmlParser setDelegate: self];
	[xmlParser setShouldResolveExternalEntities: YES];
	[xmlParser parse];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
   attributes: (NSDictionary *)attributeDict
{
	if( [elementName isEqualToString:@"RegistrationResponse"])
	{
        NSLog(@"RegistrationResponse....");
		if(!soapResults)
		{
			soapResults = [[NSMutableString alloc] init];
		}
		recordResults = TRUE;
	}
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if( recordResults )
	{
		[soapResults appendString: string];
	}
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	if( [elementName isEqualToString:@"RegistrationResponse"])
	{
		recordResults = FALSE;
		//greeting.text = soapResults;
        NSLog(@"soap result is >>%@",soapResults);
        NSString *firstStr=[soapResults substringToIndex:2];
        NSLog(@"first substring is >>>>>>>%@",firstStr);
        if([soapResults isEqualToString:@"MINIMUM PASSWORD LENGTH IS 6"])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Minimum password length must be 6" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        if([firstStr isEqualToString:@"OK"])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Success" message:@"Registration successfull" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=1;
            [alert show];
            appdel.mRegiEmail=mTxtEmailid.text;
            appdel.mRegiPass=mTxtPassword.text;
            NSLog(@"appdel.mRegiEmail in reg ok >>%@",appdel.mRegiEmail);
        }
        else if ([firstStr isEqualToString:@"AL"])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This mail id is already registered" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
		soapResults = nil;
	}
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
        mTxtName.text=@" ";
        mTxtEmailid.text=@" ";
        mTxtPassword.text=@" ";
        mTxtRepeatPassword.text=@" ";
    }
}
- (IBAction)btnBacktoStart:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)btnWithoutRegistration:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        GetRecipeViewController *getRcp=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:getRcp animated:YES];
    }
    else
    {
        GetRecipeViewController *reg=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
}
#pragma mark Social Button Action
- (IBAction)btnFacebookClk:(id)sender
{
    //AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
   // [appDelegate openSession];
    
    //self.isSocial=YES;
    [FBSession.activeSession closeAndClearTokenInformation];
    //permission for fetch user information
    [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_location",@"user_birthday",@"user_hometown"]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                      
                                      switch (state) {
                                          case FBSessionStateOpen:
                                          {
                                              [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                                                  if (error) {
                                                      NSLog(@"error:%@",error);
                                                      //Show alert messgae to on facbook status from app setting
                                                  } else {
                                                      // retrive user's details at here as shown below
                                                     
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                      mTxtEmailid.text=[user objectForKey:@"email"];
                                                      mTxtName.text=user.first_name;
                                                      mTxtPassword.text=user.id;
                                                      mTxtRepeatPassword.text=user.id;
                                                       self.strFacebookId=user.id;
                                                      [self registerUser];
                                                      });
//
//
                                                  }
                                              }];
                                          }
                                              break;
                                          case FBSessionStateClosed://If session closed
                                          case FBSessionStateClosedLoginFailed://If session failed
                                              [FBSession.activeSession closeAndClearTokenInformation];//Close and clear token and session
                                              break;
                                          default:
                                              break;
                                      }
                                      
                                  } ];
}
- (IBAction)btnTwitterClk:(id)sender
{
    self.accountStore = [[ACAccountStore alloc] init];//Account  Store
    ACAccountType *accountType = [ self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter]; //Type of account
    [self.accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
     {
         // Did user allow us access?
         if (granted == YES)//If login from Setting
         {
             // Populate array with all available Twitter accounts
             NSArray  *arrayOfAccounts = [self.accountStore accountsWithAccountType:accountType];
             ACAccount *twitterAccount = [arrayOfAccounts objectAtIndex:0];//Account details
             
             NSString *username = twitterAccount.username;
             
             NSDictionary *tempDict = [[NSMutableDictionary alloc] initWithDictionary:
                                       [twitterAccount dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]]];
             NSString *tempUserID = [[tempDict objectForKey:@"properties"] objectForKey:@"user_id"];
             NSString *userFullName = [[tempDict objectForKey:@"properties"] objectForKey:@"fullName"];
             
             NSLog(@"Twitter profile details fullname = %@, userName = %@, userId = %@",userFullName,username,tempUserID);
           
             dispatch_async(dispatch_get_main_queue(), ^{
                 mTxtEmailid.text=username;
                 mTxtName.text=userFullName;
                 mTxtPassword.text=tempUserID;
                 mTxtRepeatPassword.text=tempUserID;
                 self.strTwitterId=tempUserID;
                 [self registerUser];
             });
     
         }else{
             //Give alert for login from setting
         }
     }];
 
}
-(void)myMethod
{
    NSLog(@"1111");
}
- (IBAction)btnGooglePlus:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isFromFacebook=NO;
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    
    //signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    [signIn authenticate];
    [[GPPSignIn sharedInstance] trySilentAuthentication];
}
#pragma mark GoogleApi Delegate

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    if (!error) {
        // Do some error handling here.
        NSLog(@"email %@ ", signIn.authentication.userEmail);
        
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
        plusService.retryEnabled = YES;
        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        GTMLoggerError(@"Error: %@", error);
                    } else {
                        // Retrieve the display name and "about me" text
                        
                        NSString *description = [NSString stringWithFormat:
                                                 @"user details%@\n%@ %@ %@ ", person.displayName,
                                                 person.name,person.identifier,person.nickname];
                        NSLog(@"description = %@",description);
                        dispatch_async(dispatch_get_main_queue(), ^{
                        mTxtEmailid.text=signIn.authentication.userEmail;
                        mTxtName.text=[NSString stringWithFormat:@"%@",person.name];
                        mTxtPassword.text=[NSString stringWithFormat:@"%@",person.name];;
                        mTxtRepeatPassword.text=[NSString stringWithFormat:@"%@",person.identifier];
                        self.strGooglePlusId=[NSString stringWithFormat:@"%@",person.identifier];
                        [self registerUser];
                        });
                    }
                }];
    }
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}
-(void)refreshInterfaceBasedOnSignIn {
    if ([[GPPSignIn sharedInstance] authentication]) {
        // The user is signed in.
        
        // Perform other actions here, such as showing a sign-out button
    } else {
        
        // Perform other actions here
    }
}
- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        NSLog(@"Received error %@", error);
    } else {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
    }
}



@end
