//
//  DescriptionRecipeViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 07/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GetRecipeViewController.h"
#import "FormViewController.h"

@interface DescriptionRecipeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *ary;
    NSArray *mAry;
    int mainInt;
    int hour;
    int minute;
    int second;
    
    
    
    NSTimer *timer1;
    int nowTimer;
    int theTimer;
    int cellRow;
    NSRange someRange;
    NSArray *ary1;
    NSArray *ary2;
    NSMutableArray *neAry;
    NSMutableArray *neFinalAry;
}
@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;
@property (assign) int theIndexPath;
@property (strong, nonatomic) IBOutlet UILabel *mlblUserName;
@property (strong, nonatomic) IBOutlet UILabel *mlblRecipeName;
@property (strong, nonatomic) IBOutlet UITextView *mTxtShort;
@property (strong, nonatomic) IBOutlet UITableView *mTableIngredients;
@property (strong, nonatomic) IBOutlet UILabel *mlblTableIngr;
@property (strong, nonatomic) IBOutlet UILabel *mlblQty;
@property (strong, nonatomic) IBOutlet UILabel *mlblRecipeNameStep;
@property (strong, nonatomic) IBOutlet UITextView *mTxtStep;
@property (strong, nonatomic) IBOutlet UILabel *mlblSteps;
@property (strong, nonatomic) IBOutlet UITableView *mTableSteps;
@property (strong, nonatomic)NSMutableArray *mStep;
@property (strong, nonatomic) IBOutlet UIButton *btnIsFav;
@property (strong, nonatomic) IBOutlet UILabel *mlblCounter;
@property (strong, nonatomic)NSTimer *timer1;
@property (strong, nonatomic) IBOutlet UIImageView *mUserImg;
@property (strong, nonatomic) IBOutlet UIImageView *mImgRecipe;
@property (strong, nonatomic) IBOutlet UITableView *mTableIconImg;
@property (strong, nonatomic) IBOutlet UIImageView *mIconImg1;
@property (strong, nonatomic) IBOutlet UIImageView *mIconImg2;
@property (strong, nonatomic) NSMutableArray *mIconRecAry;
@property (strong, nonatomic) NSMutableArray *mIcon1Ary;
@property (strong, nonatomic) NSMutableArray *mIcon2Ary;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleAssistant;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleSelection;
@property (strong, nonatomic) IBOutlet UILabel *lblQtyPerson;
@property (strong, nonatomic) IBOutlet UIImageView *mImgPicNotSet;


- (IBAction)btnStartTimer:(id)sender;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnPauseTimer:(id)sender;
- (IBAction)btnAssistant:(id)sender;
- (IBAction)btnSelection:(id)sender;

@end
