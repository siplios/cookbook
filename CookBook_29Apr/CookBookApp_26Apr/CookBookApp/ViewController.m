//
//  ViewController.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 05/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonHMAC.h>
#import "AppDelegate.h"
#import <Twitter/Twitter.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
static NSString * const kClientID =
@"931972229113-34sotf7kgqblo5mbgoskkqg75lab78o4.apps.googleusercontent.com";
AppDelegate *appdel;

@interface ViewController ()

@end

@implementation ViewController
@synthesize mTxtPassword;
@synthesize mTxtUsername;
@synthesize btnCheckTitle;
@synthesize mEncodedPassword;
@synthesize mParseAry;

- (void)viewDidLoad
{
    [super viewDidLoad];
    mEncodedPassword=[[NSString alloc] init];
    mParseAry=[[NSMutableArray alloc] init];
    appdel=[[UIApplication sharedApplication] delegate];
    
    NSString *filePathSecond = [[NSBundle mainBundle] pathForResource:@"recipe_id_10" ofType:@"jpg"];
    NSLog(@"recipe image path is >>%@",filePathSecond);
    
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"in didload database path>>>>%@",databasePath);
    
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        NSLog(@"file not exist");
    }
    else
    {
        NSLog(@"file exist");
        [self methodSelectFromTable];
    }
    [self isFirstRun];
	// Do any additional setup after loading the view, typically from a nib.
   // btnCheckTitle=[[UIButton alloc] init];
//    UIImage * buttonImage = [UIImage imageNamed:@"uncheck.png"];
//    [btnCheckTitle setImage:buttonImage forState:UIControlStateNormal];
    
   // [btnCheckTitle setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
     checked = YES;
     val=0;
    
    
}
- (BOOL) isFirstRun
{
    //NSLog(@"in firstrun");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"isFirstRun"])
    {
        NSLog(@"not first run");
        value=1;
        return NO;
    }
    value=0;
    NSLog(@"yes first run");
    NSLog(@"this is in first time");
    [self createTableSession];
    [self createDatabaseinSqlite];
    [self createDatabaseForImage];
    [self createTableSqliteForFavouriteAndStarRating];
    [self createTableSqliteRememberPassword];
    
    [defaults setObject:[NSDate date] forKey:@"isFirstRun"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    if([appdel.mRegiEmail length]==0)
    {
        NSLog(@"appdel.mRegiEmail is null");
    }
    else
    {
        NSLog(@"appdel.mRegiEmail is not null");
        mTxtUsername.text=appdel.mRegiEmail;
        mTxtPassword.text=appdel.mRegiPass;
        NSLog(@"mTxtUsername.text=%@",appdel.mRegiEmail);
        NSLog(@"mTxtPassword.text=%@",appdel.mRegiPass);
    }
    // [btnCheckTitle setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    UIImage * buttonImage = [UIImage imageNamed:@"uncheck.png"];
        [btnCheckTitle setImage:buttonImage forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([mTxtUsername.text length]==0)
    {
        
    }
    else
    {
        if(textField.tag==1)
        {
            [self methodSelectForRemPassword];
        }
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)methodSelectForRemPassword
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"in select database path>>>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        NSLog(@"file not exist REMEMBERPASSWORD");
    }
    else
    {
        NSLog(@"file exist REMEMBERPASSWORD");
    }
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"in select page dbpath REMEMBERPASSWORD->>%s",dbpath);
    sqlite3_stmt    *statement;
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
//        NSString *querySQLRem = [NSString stringWithFormat:@"Select * from REMEMBERPASSWORD where R_USER_NAME=%@",mTxtUsername.text];
         NSString *querySQLRem = [NSString stringWithFormat:@"Select * from REMEMBERPASSWORD "];
        NSLog(@"The Data REMEMBERPASSWORD= %@",querySQLRem);
        const char *query_stmtRem = [querySQLRem UTF8String];
        NSLog(@"show data char is REMEMBERPASSWORD-->>>>%s",query_stmtRem);
        if (sqlite3_prepare_v2(cookLoginDB, query_stmtRem, -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"111###REMEMBERPASSWORD");
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSLog(@"in row REMEMBERPASSWORD");
                NSLog(@"select at 0 REMEMBERPASSWORD>>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]);
                NSLog(@"select at 1 REMEMBERPASSWORD>>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]);
                mTxtPassword.text=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(cookLoginDB);

}

-(NSString *)stringToSha1:(NSString *)hashkey{
    
    NSLog(@"in stringToSha1");
    // Using UTF8Encoding
    const char *s = [hashkey cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    
    // This is the destination
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    // This one function does an unkeyed SHA1 hash of your hash data
    CC_SHA1(keyData.bytes, keyData.length, digest);
    
    // Now convert to NSData structure to make it usable again
    NSData *out = [NSData dataWithBytes:digest
                                 length:CC_SHA1_DIGEST_LENGTH];
    // description converts to hex but puts <> around it and spaces every 4
    // bytes
    NSString *hash = [out description];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    // hash is now a string with just the 40char hash value in it
    
    NSLog(@"hash is >>%@",hash);
    mEncodedPassword=hash;
    return hash;
}

- (IBAction)mBtnSignin:(id)sender {
    userfbEmail=mTxtUsername.text;
    [self userSignin];
   }

-(void)userSignin
{
    NSLog(@"in user login username=%@, password=%@",mTxtUsername.text,mTxtPassword.text);
    self.isSocial=NO;
    mPasswordStr=mTxtPassword.text;
    [self stringToSha1:mPasswordStr];
    NSLog(@"mEncodedPassword is >%@",mEncodedPassword);
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<Login xmlns=\"http://danica.com.ar/recetario\">"
                             "<email>%@</email>"
                             "<digest>%@</digest>"
                             "</Login>"
                             "</soap:Body>"
                             "</soap:Envelope>",userfbEmail,mEncodedPassword];
    
    NSLog(@"my soapMessage is->%@",soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://danica.com.ar/cgi-bin/soapserver.pl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@">>.....%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://danica.com.ar/recetario/Login" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [NSMutableData data] ;
        NSLog(@"No Problem");
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"ERROR with theConenction");
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"DONE. Received Bytes: %d", [webData length]);
	NSString *theXML = [[NSString alloc] initWithBytes: [webData mutableBytes] length:[webData length] encoding:NSUTF8StringEncoding];
	NSLog(@"Response from server is---->>%@",theXML);
    
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
	[xmlParser setDelegate: self];
	[xmlParser setShouldResolveExternalEntities: YES];
	[xmlParser parse];
}
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
   attributes: (NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"LoginResponse"])
	{
        NSLog(@"LoginResponse....");
		if(!soapResults)
		{
			soapResults = [[NSMutableString alloc] init];
            mParseAry= [[NSMutableArray alloc] init];
		}
		recordResults = TRUE;
	}
    if([elementName isEqualToString:@"ForgotPasswordResponse"])
	{
        NSLog(@"ForgotPasswordResponse....");
		if(!soapResultsForgot)
		{
			soapResultsForgot = [[NSMutableString alloc] init];
            mParseAry= [[NSMutableArray alloc] init];
		}
		recordResults = TRUE;
	}
    if([elementName isEqualToString:@"RegistrationResponse"])
	{
        if(!soapResults)
		{
            NSLog(@"RegistrationResponse....");
            soapResultsRegFB = [[NSMutableString alloc] init];
            mParseAry= [[NSMutableArray alloc] init];
        }
        recordResults = TRUE;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSLog(@"The string in foundCharacters >>%@",string);
    NSMutableDictionary *mnsAry=[[NSMutableDictionary alloc] init];
    [mnsAry setObject:string forKey:@"aaa"];
    NSLog(@"mnsAry is >>%@",[mnsAry objectForKey:@"aaa"]);
	if( recordResults )
	{
		[soapResults appendString: string];
        [mParseAry addObject:string];
        [soapResultsForgot appendString: string];
        [soapResultsRegFB appendString: string];
	}
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	if( [elementName isEqualToString:@"LoginResponse"])
	{
		recordResults = FALSE;
		//greeting.text = soapResults;
        NSLog(@"soap result for LoginResponse is >>%@",soapResults);
        NSLog(@"soap result for array is >>%@",mParseAry);
        if([soapResults isEqualToString:@"UNKNOWN"])
        {
            if(!self.isSocial)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Login failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else if (self.isSocial)
            {
                [self methodSendFBRegistration];
            }
        }
        else
        {
            NSString *firstStr=[soapResults substringToIndex:2];
            appdel.mUserSession=[mParseAry objectAtIndex:1];
            NSLog(@"first substring is >>>>>>>%@",firstStr);
            NSLog(@"mParse array count loginresponse>>%d",[mParseAry count]);
            if([firstStr isEqualToString:@"OK"])
            {
                NSLog(@"success in login");
                remLoginSuccess=@"SuccessLogin";
                [self remPasswordMethod];
                [self methodInsertintoTable];
                NSLog(@"user login id is >>%@",appdel.mLoginId);
                
                NSLog(@".....user name=%@ ,logind id=%@ and session=%@ ",appdel.mLoginUserName,appdel.mLoginId,appdel.mUserSession);
                [self insertIntoUserSession];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    GetRecipeViewController *getRcp=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPad" bundle:nil];
                    [self.navigationController pushViewController:getRcp animated:YES];
                }
                else
                {
                    GetRecipeViewController *reg=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPhone" bundle:nil];
                    [self.navigationController pushViewController:reg animated:YES];
                }
            }
        }
        soapResults=nil;
    }
    if( [elementName isEqualToString:@"ForgotPasswordResponse"])
	{
		recordResults = FALSE;
		//greeting.text = soapResults;
        NSLog(@"soap result for ForgotPasswordResponse is >>%@",soapResultsForgot);
        if([soapResultsForgot isEqualToString:@"OK"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"An email with your password has been sent to you. Please check and login again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else if([soapResultsForgot isEqualToString:@"UNKNOWN"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"This email id is not exist" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    if( [elementName isEqualToString:@"RegistrationResponse"])
	{
		recordResults = FALSE;
        NSLog(@"soap result for soapResultsRegFB facebook is >>%@",soapResultsRegFB);
         NSString *firstStr=[soapResultsRegFB substringToIndex:2];
        if([firstStr isEqualToString:@"OK"])
        {
            NSLog(@"success in login RegistrationResponse facebook");
//            remLoginSuccess=@"SuccessLogin";
//            [self remPasswordMethod];
//            [self methodInsertintoTable];
//            NSLog(@"user login id is RegistrationResponse facebook>>%@",appdel.mLoginId);
//            
//            NSLog(@"RegistrationResponse facebook.....user name=%@ ,logind id=%@ and session=%@ ",appdel.mLoginUserName,appdel.mLoginId,appdel.mUserSession);
//            [self insertIntoUserSession];
//            
//            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//            {
//                GetRecipeViewController *getRcp=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPad" bundle:nil];
//                [self.navigationController pushViewController:getRcp animated:YES];
//            }
//            else
//            {
//                GetRecipeViewController *reg=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPhone" bundle:nil];
//                [self.navigationController pushViewController:reg animated:YES];
//            }
          //  [self mathodSendFBLogin];
        }
    }
}
-(void)remPasswordMethod
{
    if([remPassword isEqualToString:@"Rem"] && [remLoginSuccess isEqualToString:@"SuccessLogin"])
    {
        NSLog(@"checked and success login");
        [self insertForRemPassword];
    }
}

-(void)createDatabaseForImage
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"before database path USERLOGINIMAGE>>%@",databasePath);
   // NSFileManager *filemgr = [NSFileManager defaultManager];
    NSLog(@"in createDatabaseinSqlite USERLOGINIMAGE");
//    if ([filemgr fileExistsAtPath: databasePath ] == NO)
//    {
        const char *dbpath = [databasePath UTF8String];
        NSLog(@"dbpath is USERLOGINIMAGE->>%s",dbpath);
        
        if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
        {
            NSLog(@"in row USERLOGINIMAGE");
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS USERLOGINIMAGE (USER_ID INTEGER PRIMARY KEY , USER_NAME TEXT, USER_EMAIL TEXT,URLSTRING TEXT )";
            
            if (sqlite3_exec(cookLoginDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                //                status.text = @"Failed to create table";
                NSLog(@"Failed to create table USERLOGINIMAGE");
            }
            NSLog(@"create table USERLOGINIMAGE");
            sqlite3_close(cookLoginDB);
        }
        else
        {
            // status.text = @"Failed to open/create database";
            NSLog(@"Failed to open/create database USERLOGINIMAGE");
        }
   // }
}


-(void)createDatabaseinSqlite
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"before database path >>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
     NSLog(@"in createDatabaseinSqlite");
    
        const char *dbpath = [databasePath UTF8String];
        NSLog(@"dbpath is->>%s",dbpath);
        
        if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS USERLOGINDET (USER_ID INTEGER PRIMARY KEY , FIRST_NAME TEXT, LAST_NAME TEXT,EMAIL TEXT, FACEBOOK_ID TEXT, IS_SUBSCRIBER TEXT, HAS_MOBILEAPP TEXT, WANTS_BEEFRECIPES TEXT, WANTS_CHICKENRECIPES TEXT, WANTS_PORKRECIPES TEXT, WANTS_FISHRECIPES TEXT,  WANTS_VEGRECIPES TEXT, WANTS_OTHERRECIPES TEXT, WANTS_CHILDRENRECIPES TEXT, WANTS_EASYRECIPES TEXT, WANTS_MODERATERECIPES TEXT, WANTS_COMPLEXRECIPES TEXT, SENDON_MON TEXT, SENDON_TUE TEXT, SENDON_WED TEXT, SENDON_THU TEXT, SENDON_FRI TEXT, SENDON_SAT TEXT, SENDON_SUN TEXT, SENDON_TIMEOFDAY TEXT, HOWMANYEATING INTEGER,USER_SESSION TEXT,WANTS_PASTA TEXT)";
            
            if (sqlite3_exec(cookLoginDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                //                status.text = @"Failed to create table";
                NSLog(@"Failed to create table");
            }
            NSLog(@"create table USERLOGINDET");
            sqlite3_close(cookLoginDB);
            
        } else {
            // status.text = @"Failed to open/create database";
            NSLog(@"Failed to open/create database");
        }
    
}

-(void)createTableSqliteForFavouriteAndStarRating
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"before database path for fav and srar rate>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSLog(@"in createDatabaseinSqlite for fav and srar rate");
    if ([filemgr fileExistsAtPath: databasePath ] == YES)
    {
        const char *dbpath = [databasePath UTF8String];
        NSLog(@"dbpath is for fav and star rate->>%s",dbpath);
        
        if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS LOGGEDINUSERDETAILS (USER_ID INTEGER  ,RECIPE_ID INTEGER ,ISFAV BOOL NOT NULL DEFAULT 0, RATE FLOAT)";
            
            if (sqlite3_exec(cookLoginDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                //                status.text = @"Failed to create table";
                NSLog(@"Failed to create table LOGGEDINUSERDETAILS");
            }
            NSLog(@"create table LOGGEDINUSERDETAILS");
            sqlite3_close(cookLoginDB);
            
        } else {
            // status.text = @"Failed to open/create database";
            NSLog(@"Failed to open/create database");
        }
    }
}


-(void)createTableSession
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"before database path createTableSession>>%@",databasePath);
//    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSLog(@"in createDatabaseinSqlite for createTableSession");
   
        const char *dbpath = [databasePath UTF8String];
        NSLog(@"dbpath is for createTableSession->>%s",dbpath);
        
        if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS LOGGEDINUSERSESSION (USER_ID INTEGER ,USER_NAME TEXT ,USER_SESSION TEXT)";
            
            if (sqlite3_exec(cookLoginDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                //                status.text = @"Failed to create table";
                NSLog(@"Failed to create table LOGGEDINUSERSESSION");
            }
            NSLog(@"create table LOGGEDINUSERSESSION");
            sqlite3_close(cookLoginDB);
            
        } else {
            // status.text = @"Failed to open/create database";
            NSLog(@"Failed to open/create database LOGGEDINUSERSESSION");
        }
    
}






-(void)createTableSqliteRememberPassword
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"before database path for fav and srar rate>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSLog(@"in createDatabaseinSqlite for fav and srar rate");
    if ([filemgr fileExistsAtPath: databasePath ] == YES)
    {
        const char *dbpath = [databasePath UTF8String];
        NSLog(@"dbpath is for fav and star rate->>%s",dbpath);
        
        if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS REMEMBERPASSWORD (R_USER_NAME TEXT ,R_PASSWORD TEXT)";
            
            if (sqlite3_exec(cookLoginDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                //                status.text = @"Failed to create table";
                NSLog(@"Failed to create table REMEMBERPASSWORD");
            }
            NSLog(@"create table REMEMBERPASSWORD");
            sqlite3_close(cookLoginDB);
            
        } else {
            // status.text = @"Failed to open/create database";
            NSLog(@"Failed to open/create database REMEMBERPASSWORD");
        }
    }
}
-(void)insertForRemPassword
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    sqlite3_stmt    *statement;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
         NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO REMEMBERPASSWORD (R_USER_NAME,R_PASSWORD ) VALUES (\"%@\",\"%@\")",mTxtUsername.text,mTxtPassword.text];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(cookLoginDB, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            // status.text = @"Contact added";
            NSLog(@"Contact added REMEMBERPASSWORD");
            
        } else {
            // status.text = @"Failed to add contact";
            NSLog(@" Failed to add contact REMEMBERPASSWORD");
        }
        sqlite3_finalize(statement);
        sqlite3_close(cookLoginDB);

    }
    
}

-(void)insertIntoUserSession
{
    NSLog(@".... in insertIntoUserSession");
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    sqlite3_stmt    *statement;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO LOGGEDINUSERSESSION (USER_ID,USER_NAME,USER_SESSION ) VALUES (\"%@\",\"%@\",\"%@\")",appdel.mLoginId,appdel.mLoginUserName,appdel.mUserSession];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(cookLoginDB, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            // status.text = @"Contact added";
            NSLog(@"Contact added LOGGEDINUSERSESSION");
            
        } else {
            // status.text = @"Failed to add contact";
            NSLog(@" Failed to add contact LOGGEDINUSERSESSION");
        }
        sqlite3_finalize(statement);
        sqlite3_close(cookLoginDB);
    }
}



-(void)methodInsertintoTable
{
//     NSLog(@"mParseAry at 24 >>%@",[mParseAry objectAtIndex:24]);
//     NSLog(@"mParseAry at 25 >>%@",[mParseAry objectAtIndex:25]);
     //NSLog(@"mParseAry at 26 >>%@",[mParseAry objectAtIndex:26]);
    
    NSLog(@"mParseAry count is >>%d",[mParseAry count]);
     appdel.mLoginId=[mParseAry objectAtIndex:2];
     appdel.mLoginUserName=[mParseAry objectAtIndex:3];
    appdel.mUserLoggedinEmail=[mParseAry objectAtIndex:5];
    
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    sqlite3_stmt    *statement;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSLog(@"100000000");
//        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO USERLOGINDET (USER_ID,FIRST_NAME,LAST_NAME,EMAIL,  IS_SUBSCRIBER, HAS_MOBILEAPP, WANTS_BEEFRECIPES, WANTS_CHICKENRECIPES, WANTS_PORKRECIPES, WANTS_FISHRECIPES, WANTS_VEGRECIPES,WANTS_CHILDRENRECIPES,WANTS_EASYRECIPES,WANTS_MODERATERECIPES, WANTS_COMPLEXRECIPES,SENDON_MON, SENDON_TUE, SENDON_WED,SENDON_THU, SENDON_FRI, SENDON_SAT, SENDON_SUN, SENDON_TIMEOFDAY, HOWMANYEATING) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",[mParseAry objectAtIndex:2], [mParseAry objectAtIndex:3],[mParseAry objectAtIndex:4],[mParseAry objectAtIndex:5],[mParseAry objectAtIndex:6],[mParseAry objectAtIndex:7],[mParseAry objectAtIndex:8],[mParseAry objectAtIndex:9],[mParseAry objectAtIndex:10],[mParseAry objectAtIndex:11],[mParseAry objectAtIndex:12],[mParseAry objectAtIndex:13],[mParseAry objectAtIndex:14],[mParseAry objectAtIndex:15],[mParseAry objectAtIndex:16],[mParseAry objectAtIndex:17],[mParseAry objectAtIndex:18],[mParseAry objectAtIndex:19],[mParseAry objectAtIndex:20],[mParseAry objectAtIndex:21],[mParseAry objectAtIndex:22],[mParseAry objectAtIndex:23],[mParseAry objectAtIndex:24],[mParseAry objectAtIndex:25]];
        
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO USERLOGINDET (USER_ID,FIRST_NAME,LAST_NAME,EMAIL,  IS_SUBSCRIBER, HAS_MOBILEAPP, WANTS_BEEFRECIPES, WANTS_CHICKENRECIPES, WANTS_PORKRECIPES, WANTS_FISHRECIPES,WANTS_PASTA,WANTS_VEGRECIPES,WANTS_CHILDRENRECIPES,WANTS_EASYRECIPES,WANTS_MODERATERECIPES, WANTS_COMPLEXRECIPES,SENDON_MON, SENDON_TUE, SENDON_WED,SENDON_THU, SENDON_FRI, SENDON_SAT, SENDON_SUN, SENDON_TIMEOFDAY, HOWMANYEATING) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",[mParseAry objectAtIndex:2], [mParseAry objectAtIndex:3],[mParseAry objectAtIndex:4],[mParseAry objectAtIndex:5],[mParseAry objectAtIndex:6],[mParseAry objectAtIndex:7],[mParseAry objectAtIndex:8],[mParseAry objectAtIndex:9],[mParseAry objectAtIndex:10],[mParseAry objectAtIndex:11],[mParseAry objectAtIndex:12],[mParseAry objectAtIndex:13],[mParseAry objectAtIndex:14],[mParseAry objectAtIndex:15],[mParseAry objectAtIndex:16],[mParseAry objectAtIndex:17],[mParseAry objectAtIndex:18],[mParseAry objectAtIndex:19],[mParseAry objectAtIndex:20],[mParseAry objectAtIndex:21],[mParseAry objectAtIndex:22],[mParseAry objectAtIndex:23],[mParseAry objectAtIndex:24],[mParseAry objectAtIndex:25]];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(cookLoginDB, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            // status.text = @"Contact added";
            NSLog(@"Contact added");
        } else {
            // status.text = @"Failed to add contact";
            NSLog(@" Failed to add contact");
        }
        sqlite3_finalize(statement);
        sqlite3_close(cookLoginDB);
    }
}
-(void)methodSelectFromTable
{
    NSLog(@"in method select");
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"in select database path>>>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        NSLog(@"file not exist");
    }
    else
    {
         NSLog(@"file exist");
    }
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"in select page dbpath ->>%s",dbpath);
    sqlite3_stmt    *statement;
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select * from USERLOGINDET"];
        NSLog(@"The Data = %@",querySQL);
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"show data char is -->>>>%s",query_stmt);
        if (sqlite3_prepare_v2(cookLoginDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"111###");
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSLog(@"in row");
                NSLog(@"select at 0 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]);
                NSLog(@"select at 1 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]);
                NSLog(@"select at 2 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]);
                 NSLog(@"select at 3 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)]);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(cookLoginDB);
}

- (IBAction)btnCheckTapped:(id)sender
{
    if (!checked)
    {
        [btnCheckTitle setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        checked = YES;
        remPassword=@"notRem";
    }
    else if (checked)
    {
        [btnCheckTitle setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        checked = NO;
        remPassword=@"Rem";
    }
}
- (IBAction)btnForgotPassword:(id)sender {
//    if([mTxtUsername.text length]==0)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
//    else
//    {
//        [self forgotPasswordWebservice];
//    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        ForgotPasswordViewController *reg=[[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        ForgotPasswordViewController *reg=[[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }

}
-(void)forgotPasswordWebservice
{
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<ForgotPassword xmlns=\"http://danica.com.ar/recetario\">"
                             "<email>%@</email>"
                             "</ForgotPassword>"
                             "</soap:Body>"
                             "</soap:Envelope>",mTxtUsername.text];
    
    NSLog(@"my soapMessage is->%@",soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://danica.com.ar/cgi-bin/soapserver.pl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@">>.....%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://danica.com.ar/recetario/ForgotPassword" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [NSMutableData data] ;
        NSLog(@"No Problem forgotpassword");
    }
    else
    {
        NSLog(@"theConnection is NULL forgotpassword");
    }
}

- (IBAction)btnRegistration:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
       RegistrationViewController *reg=[[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        RegistrationViewController *reg=[[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
}

- (IBAction)btnWithoutRegistration:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        GetRecipeViewController *getRcp=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:getRcp animated:YES];
    }
    else
    {
        GetRecipeViewController *reg=[[GetRecipeViewController alloc] initWithNibName:@"GetRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
}

- (IBAction)btnLoginFaceBook:(id)sender {
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate openSession];
   
    self.isSocial=YES;
    [FBSession.activeSession closeAndClearTokenInformation];
    //permission for fetch user information
    [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_location",@"user_birthday",@"user_hometown"]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                      
                                      switch (state) {
                                          case FBSessionStateOpen:
                                          {
                                              [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                                                  if (error) {
                                                      NSLog(@"error:%@",error);
                                                      //Show alert messgae to on facbook status from app setting
                                                  } else {
                                                      // retrive user's details at here as shown below
                                                      NSLog(@"FB user first name:%@",user.first_name);
                                                      
                                                      NSLog(@"FB user last name:%@",user.last_name);
                                                      NSLog(@"FB user birthday:%@",user.birthday);
                                                      NSLog(@"FB user location:%@",user.location);
                                                      NSLog(@"FB user username:%@",user.username);
                                                      NSLog(@"FB user id:%@",user.id);
                                                      NSLog(@"FB user gender:%@",[user objectForKey:@"gender"]);
                                                      NSLog(@"email id:%@",[user objectForKey:@"email"]);
                                                      NSLog(@"location:%@", [NSString stringWithFormat:@"Location: %@\n\n",
                                                                             user.location[@"name"]]);
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          
//                                                          mTxtUsername.text=username;
//                                                          mTxtPassword.text=tempUserID;
                                                          
                                                          
                                                          firstName=user.first_name;
                                                          lastName=user.last_name;
                                                          userfbEmail=[user objectForKey:@"email"];
                                                          userfbPassword=user.id;
                                                          userfbID=user.id;
                                                          //[self methodSendFBRegistration];
                                                          [self userSignin];
                                                      });
                                                   }
                                              }];
                                          }
                                              break;
                                          case FBSessionStateClosed://If session closed
                                          case FBSessionStateClosedLoginFailed://If session failed
                                              [FBSession.activeSession closeAndClearTokenInformation];//Close and clear token and session
                                              break;
                                          default:
                                              break;
                                      }
                                      
                                  } ];

}

-(void)methodSendFBRegistration
{
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<Registration xmlns=\"http://danica.com.ar/recetario\">"
                             "<user>"
                             "<email>%@</email>"
                             "<firstName>%@</firstName>"
                             "<password>%@</password>"
                             "<gplusId xsi:nil=\"true\"/>"
                             "<tweeterId xsi:nil=\"true\"/>"
                             "<lastName>%@</lastName>"
                             "<facebookId>%@</facebookId>"
                             "</user>"
                             "</Registration>"
                             "</soap:Body>"
                             "</soap:Envelope>",userfbEmail,firstName,userfbPassword,lastName,userfbID];
    
    NSLog(@"my soapMessage is->%@",soapMessage);
    appdel.mLoginThrough=@"facebook";
    NSURL *url = [NSURL URLWithString:@"http://danica.com.ar/cgi-bin/soapserver.pl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@">>.....%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://danica.com.ar/recetario/Registration" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [NSMutableData data] ;
        NSLog(@"Problem");
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
    self.isSocial=NO;
}
-(void)mathodSendFBLogin
{
    mPasswordStr=userfbPassword;
    [self stringToSha1:mPasswordStr];
    NSLog(@"mEncodedPassword is >%@",mEncodedPassword);
    
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<Login xmlns=\"http://danica.com.ar/recetario\">"
                             "<email>%@</email>"
                             "<digest>%@</digest>"
                             "</Login>"
                             "</soap:Body>"
                             "</soap:Envelope>",userfbEmail,mEncodedPassword];
    
    NSLog(@"my soapMessage is->%@",soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://danica.com.ar/cgi-bin/soapserver.pl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@">>.....%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://danica.com.ar/recetario/Login" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [NSMutableData data] ;
        NSLog(@"No Problem");
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }

}

- (IBAction)btnLoginGplus:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isFromFacebook=NO;
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    
    //signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = kClientID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    [signIn authenticate];
    [[GPPSignIn sharedInstance] trySilentAuthentication];
}

- (IBAction)btnLoginTwitter:(id)sender {
//   AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    appDelegate.isFromFacebook=NO;
    self.accountStore = [[ACAccountStore alloc] init];//Account  Store
    ACAccountType *accountType = [ self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter]; //Type of account
    [self.accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
     {
         // Did user allow us access?
         if (granted == YES)//If login from Setting
         {
             // Populate array with all available Twitter accounts
             NSArray  *arrayOfAccounts = [self.accountStore accountsWithAccountType:accountType];
             ACAccount *twitterAccount = [arrayOfAccounts objectAtIndex:0];//Account details
             
             NSString *username = twitterAccount.username;
             
             NSDictionary *tempDict = [[NSMutableDictionary alloc] initWithDictionary:
                                       [twitterAccount dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]]];
             NSString *tempUserID = [[tempDict objectForKey:@"properties"] objectForKey:@"user_id"];
             NSString *userFullName = [[tempDict objectForKey:@"properties"] objectForKey:@"fullName"];
             
             NSLog(@"Twitter profile details fullname = %@, userName = %@, userId = %@",userFullName,username,tempUserID);
             
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 mTxtUsername.text=username;
                 userfbEmail=username;
                 mTxtPassword.text=tempUserID;
                 [self userSignin];
                
             });
         }else{
            //Give alert for login from setting
         }
     }];

}
#pragma mark GoogleApi Delegate

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    if (!error) {
        // Do some error handling here.
        NSLog(@"email %@ ", signIn.authentication.userEmail);
        
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
        plusService.retryEnabled = YES;
        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        GTMLoggerError(@"Error: %@", error);
                    } else {
                        // Retrieve the display name and "about me" text
                        
                        NSString *description = [NSString stringWithFormat:
                                                 @"user details%@\n%@ %@ %@ ", person.displayName,
                                                 person.name,person.identifier,person.nickname];
                        NSLog(@"description = %@",description);
                    }
                }];
    }
    
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
}
-(void)refreshInterfaceBasedOnSignIn {
    if ([[GPPSignIn sharedInstance] authentication]) {
        // The user is signed in.
        
        // Perform other actions here, such as showing a sign-out button
    } else {
        
        // Perform other actions here
    }
}
- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        NSLog(@"Received error %@", error);
    } else {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
    }
}

//Signout user if needed
- (void)signOut {
    [[GPPSignIn sharedInstance] signOut];
}




@end
