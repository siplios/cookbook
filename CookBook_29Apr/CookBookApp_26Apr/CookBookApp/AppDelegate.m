//
//  AppDelegate.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 05/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>
#import "ViewController.h"
#import "LogoutViewController.h"
static NSString * const kClientID =
@"931972229113-34sotf7kgqblo5mbgoskkqg75lab78o4.apps.googleusercontent.com";
@implementation AppDelegate
@synthesize navController;
@synthesize mLogout;
@synthesize viewController;
@synthesize mLoginId;
@synthesize mLoginUserName;
@synthesize mAllRecipeAry;
@synthesize whichSearch;
@synthesize mCategoryAry;
@synthesize mCategorySearchAry;
@synthesize mIDarray;
@synthesize valCategory;
@synthesize mDetailDic;
@synthesize recipeIsFav;
@synthesize whichBtnClicked;
@synthesize recipeIsFavSingle;
@synthesize mUserLoggedinEmail;
@synthesize mImagePath;
@synthesize mUserSession;
@synthesize mFromPage;
@synthesize mLoginThrough;
@synthesize mRegiEmail;
@synthesize mRegiPass;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
     [GPPSignIn sharedInstance].clientID = kClientID;
//    NSLog(@"The FBSession.activeSession.state : %d",FBSession.activeSession.state);
//    NSLog(@"The FBSessionStateCreatedTokenLoaded : %d",FBSessionStateCreatedTokenLoaded);
//    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
//        self.mLogout = [[LogoutViewController alloc] initWithNibName:@"LogoutViewController" bundle:nil];
//        
////        NSLog(@"FBSessionStateCreatedTokenLoaded....");
////        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
////        UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"Home"];
////        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
////        //[self presentViewController:vc animated:YES completion:NULL];
////        UINavigationController *obj =[[UINavigationController alloc] initWithRootViewController:vc];
////        self.window.rootViewController=obj;
//        
//      
//        
//        [self openSession];
//        self.window.rootViewController = self.mLogout;
//    } else {
//        [self showLoginView];
//    }
    [self showLoginView];
    [self.window makeKeyAndVisible];
    
    mLoginId=[[NSString alloc] init];
    mLoginUserName=[[NSString alloc] init];
    mAllRecipeAry=[[NSMutableArray alloc] init];
    mCategoryAry=[[NSMutableArray alloc] init];
    mCategorySearchAry=[[NSMutableArray alloc] init];
    mIDarray=[[NSMutableArray alloc] init];
    valCategory=[[NSString alloc] init];
    mDetailDic=[[NSMutableDictionary alloc] init];
    recipeIsFav=[[NSString alloc] init];
    whichBtnClicked=[[NSString alloc] init];
    recipeIsFavSingle=[[NSString alloc] init];
    mUserLoggedinEmail=[[NSString alloc] init];
    mImagePath=[[NSString alloc] init];
    mUserSession=[[NSString alloc] init];
    mFromPage=[[NSString alloc] init];
    mLoginThrough=[[NSString alloc] init];
    mRegiPass=[[NSString alloc] init];
    mRegiEmail=[[NSString alloc] init];

    return YES;
}
- (void)showLoginView
{
    NSLog(@"in showLoginView");
//    viewController = [[ViewController alloc] initWithNibName:@"Main_iPhone.storyboard" bundle:nil];
//     viewController = [[ViewController alloc] initWithNibName:@"Main_iPhone.storyboard" bundle:nil];
//   
//   
//    UINavigationController *obj =[[UINavigationController alloc] initWithRootViewController:self.viewController];
//    self.window.rootViewController=obj;
    
//   viewController = [UIStoryboard instantiateViewControllerWithIdentifier:@"Home"];
//    [self presentModalViewController:viewController animated:YES];
    
//    
//    ViewController * addViewController = (ViewController *)[UIStoryboard instantiateViewControllerWithIdentifier:@"Home"];
//    [addViewController setDelegate:self];
//    [addViewController setModalPresentationStyle:UIModalPresentationFormSheet];
//    [self presentViewController:addViewController animated:YES completion:NULL];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"Home"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
   //[self presentViewController:vc animated:YES completion:NULL];
    UINavigationController *obj =[[UINavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController=obj;
}

-(void) logout {
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    FBSession.activeSession=nil;
    [self showLoginView];
    
}


- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            NSLog(@"FBSessionStateOpen....###");
        {
//            LogoutViewController *obj1 = [[LogoutViewController alloc] initWithNibName:@"LogoutViewController" bundle:nil];
//            UINavigationController *obj =[[UINavigationController alloc] initWithRootViewController:obj1];
//            self.window.rootViewController=obj;
        }
            break;
        case FBSessionStateClosed:
            break;
        case FBSessionStateClosedLoginFailed:
            NSLog(@"FBSessionStateClosedLoginFailed ");
            [self showLoginView];
            break;
        default:
            break;
    }
    if (error) {
        NSLog(@"The error is : %@",error.localizedDescription);
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)openSession
{
    [FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler: ^(FBSession *session,
                                                                                              FBSessionState state, NSError *error)
    {
        [self sessionStateChanged:session state:state error:error];
        NSLog(@"The session is : %@",session);
        [[FBRequest requestForMe] startWithCompletionHandler: ^(FBRequestConnection *connection,NSDictionary<FBGraphUser> *user,NSError *error)
         {
             if (!error)
             {
                 NSLog(@"The details of user is :%@ and The Connection is :%@",user,connection);
                 
             }
         }
         ];
    }
     ];
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    if (self.isFromFacebook) {
        return [FBSession.activeSession handleOpenURL:url];
    }else{
     return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    
}


							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
//new
@end
