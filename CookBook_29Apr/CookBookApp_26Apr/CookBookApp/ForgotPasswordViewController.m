//
//  ForgotPasswordViewController.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 09/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController
@synthesize mTxtForgotEmail;
@synthesize mParseAry;
@synthesize mlblForgotAlert;
@synthesize mImgTxtBack;
@synthesize btnTitleBack;
@synthesize btnTitleSubmit;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    btnTitleBack.hidden=TRUE;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return 0;
}

-(void)forgotPasswordWebservice
{
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<ForgotPassword xmlns=\"http://danica.com.ar/recetario\">"
                             "<email>%@</email>"
                             "</ForgotPassword>"
                             "</soap:Body>"
                             "</soap:Envelope>",mTxtForgotEmail.text];
    
    NSLog(@"my soapMessage is->%@",soapMessage);
    
    NSURL *url = [NSURL URLWithString:@"http://danica.com.ar/cgi-bin/soapserver.pl"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@">>.....%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://danica.com.ar/recetario/ForgotPassword" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [NSMutableData data] ;
        NSLog(@"No Problem forgotpassword");
    }
    else
    {
        NSLog(@"theConnection is NULL forgotpassword");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"ERROR with theConenction");
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"DONE. Received Bytes: %d", [webData length]);
	NSString *theXML = [[NSString alloc] initWithBytes: [webData mutableBytes] length:[webData length] encoding:NSUTF8StringEncoding];
	NSLog(@"Response from server is---->>%@",theXML);
    
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
	[xmlParser setDelegate: self];
	[xmlParser setShouldResolveExternalEntities: YES];
	[xmlParser parse];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
   attributes: (NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"ForgotPasswordResponse"])
	{
        NSLog(@"ForgotPasswordResponse....");
		if(!soapResultsForgot)
		{
			soapResultsForgot = [[NSMutableString alloc] init];
            mParseAry= [[NSMutableArray alloc] init];
		}
		recordResults = TRUE;
	}
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSLog(@"The string in foundCharacters >>%@",string);
    NSMutableDictionary *mnsAry=[[NSMutableDictionary alloc] init];
    [mnsAry setObject:string forKey:@"aaa"];
    NSLog(@"mnsAry is >>%@",[mnsAry objectForKey:@"aaa"]);
	if( recordResults )
	{
        [mParseAry addObject:string];
        [soapResultsForgot appendString: string];
	}
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if( [elementName isEqualToString:@"ForgotPasswordResponse"])
	{
		recordResults = FALSE;
		//greeting.text = soapResults;
        NSLog(@"soap result for ForgotPasswordResponse is >>%@",soapResultsForgot);
        if([soapResultsForgot isEqualToString:@"OK"])
        {
            mTxtForgotEmail.hidden=TRUE;
            mImgTxtBack.hidden=TRUE;
            mlblForgotAlert.frame=CGRectMake(15, 260, 286, 55);
            mlblForgotAlert.text=@"Hecho!Hemos enviado tu clave a:nombre @mailalco.puntocom";
            btnTitleSubmit.hidden=TRUE;
            mlblForgotAlert.textAlignment=NSTextAlignmentCenter;
            btnTitleBack.frame=CGRectMake(202 , 318, 87, 31);
            btnTitleBack.hidden=FALSE;
        }
        else if([soapResultsForgot isEqualToString:@"UNKNOWN"])
        {
            btnTitleBack.hidden=FALSE;
            mTxtForgotEmail.text=@"";
            mTxtForgotEmail.placeholder=@"Direccion de correo electronico";
            mlblForgotAlert.text=@"La dirección de correo no corresponde a mingun usuraris registrado.Por favor ingresa una dirección valida.";
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1)
    {
        NSLog(@"in alertview ");
        [self.navigationController popViewControllerAnimated:YES];
        mTxtForgotEmail.text=@" ";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnForgotPassword:(id)sender {
    [self forgotPasswordWebservice];
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
