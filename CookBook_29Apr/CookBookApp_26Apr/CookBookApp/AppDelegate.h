//
//  AppDelegate.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 05/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "ViewController.h"
#import "LogoutViewController.h"
@class GTMOAuth2Authentication;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) LogoutViewController *mLogout;

@property (strong, nonatomic) UINavigationController* navController;
@property (strong, nonatomic) NSString *mLoginId;
@property (strong, nonatomic) NSString *mLoginUserName;
@property (strong, nonatomic)NSString *whichSearch;

@property (strong, nonatomic) NSMutableArray *mAllRecipeAry;
@property (assign) int mIndexpath;
@property (strong, nonatomic) NSMutableArray *mCategoryAry;
@property (strong, nonatomic) NSMutableArray *mCategorySearchAry;
@property (strong, nonatomic) NSMutableArray *mIDarray;
@property (strong, nonatomic)NSString *valCategory;
@property (strong, nonatomic)NSMutableDictionary *mDetailDic;
@property (strong, nonatomic)NSString *recipeIsFav;
@property (strong, nonatomic)NSString *recipeIsFavSingle;
@property (strong, nonatomic)NSString *whichBtnClicked;
@property (strong, nonatomic)NSString *mUserLoggedinEmail;
@property (strong, nonatomic)NSString *mImagePath;
@property (strong, nonatomic)NSString *mUserSession;
@property (strong, nonatomic)NSString *mFromPage;
@property (strong, nonatomic)NSString *mLoginThrough;
@property (strong, nonatomic)NSString *mRegiEmail;
@property (strong, nonatomic)NSString *mRegiPass;

@property(assign)BOOL isFromFacebook;
- (void)openSession;
-(void) logout;

//24
@end
