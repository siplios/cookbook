//
//  LogoutViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 07/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogoutViewController : UIViewController
{
   
}

- (IBAction)btnLogout:(id)sender;
@end
