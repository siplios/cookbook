//
//  GetRecipeViewController.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 31/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailsRecipeViewController.h"

#import "Recipe.h"
#import "FormViewController.h"
#import "IngredientData.h"

@interface GetRecipeViewController : UIViewController<NSXMLParserDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSXMLParser *xmlParser;
    NSString *textData;
    NSData * mData;
    BOOL ref;
    BOOL ref1;
    BOOL ref2;
    int searchTap;
    NSMutableString *recipeNameResult;
    NSString *resultElement;
    Recipe *objRecipe;
    NSMutableString *mTitleString;
    
    NSString *strCategoryFound;
    NSString *strRecipeFound;
    NSString *strValue;
   
     NSMutableString *mStrRecipeMatch;
    
    NSMutableDictionary *mDicRetVal;
     NSString *shortDes;
    BOOL isingredients;
    NSMutableString *recipeId;
    NSMutableString *mRecipeTitleString;
    NSMutableString *mRecipeLastUpdate;
    NSMutableString *mRecipeShortDes;
    NSMutableString *mRecipeLongDes;
    NSMutableString *mRecipeSmallImg;
    NSMutableString *mRecipeBigImg;
    NSMutableString *mRecipeIngredient;
    NSMutableString *mRecipeQuantity;
    NSMutableString *mRecipeUnits;
    NSMutableString *mRecipeIcon;
    NSMutableString *mRecipeStep;
    NSMutableString *mRecipeCategoryId;
     NSMutableString *mCategoryId;
    NSMutableString *mCategoryVersion;
    NSMutableString *mCategoryTitle;
    NSMutableString *mCategoryShortDes;
    NSMutableString *mCategoryLongDes;
    NSMutableString *mCategoryImage;
     NSMutableString *mRecipeTotalMin;
     NSMutableString *mRecipeDefaultPortion;
    
    NSMutableArray *mIngredient_Name;
    NSMutableDictionary *dicIng_Name;
    NSString *filePathImg;
    
    NSData * imageData;
    
    sqlite3 *cookLoginDB;
    NSString        *databasePath;
    NSString *savedImagePath;
    
}
@property(strong,nonatomic)IngredientData *data;
@property (strong, nonatomic) IBOutlet UILabel *mlblUserName;
@property (strong, nonatomic) NSMutableArray *mTitleAry;
@property (strong, nonatomic) NSMutableArray *arrIngrediants;

@property (strong, nonatomic) NSMutableArray *mTitleAryFinal;
@property (strong, nonatomic) NSMutableArray *mRecipeTitleAry;
@property (strong, nonatomic)NSMutableString *currentElementValue;
@property (strong, nonatomic) IBOutlet UITextField *mTxtSearch;
@property (strong, nonatomic) NSString *mSearchStr;
@property (strong, nonatomic) NSMutableArray *mFilterAry;
@property (strong, nonatomic) IBOutlet UITableView *mTableSearch;
@property (strong, nonatomic) IBOutlet UILabel *mlblSearchCell;
@property (strong, nonatomic) NSString *mselectedRecipe;
@property (strong, nonatomic) NSMutableDictionary *aDic;
@property (strong, nonatomic) NSMutableArray *mRecipeAry;
@property (strong, nonatomic) NSMutableDictionary *ingredientDic;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *mSpinnerSearch;
@property (strong, nonatomic) NSMutableDictionary *mcategorisedDic;
@property (strong, nonatomic) NSMutableDictionary *mOnlyCategoryDic;
@property (strong, nonatomic) NSMutableArray *mRecipeTableAry;
@property (strong, nonatomic) IBOutlet UIImageView *mUserImg;
@property (strong, nonatomic) IBOutlet UILabel *mlblLoading;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleAssistant;
- (IBAction)btnBack:(id)sender;

@property (strong, nonatomic) NSMutableArray *mAllIngriAry;


- (IBAction)btnCategory1:(id)sender;
- (IBAction)btnCategory2:(id)sender;
- (IBAction)btnCategory3:(id)sender;
- (IBAction)btnCategory4:(id)sender;
- (IBAction)btnCategory5:(id)sender;
- (IBAction)btnCategory6:(id)sender;
- (IBAction)btnSearchRecipe:(id)sender;
- (IBAction)btnSelectedRecipes:(id)sender;
- (IBAction)btnAssistant:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnTitleCat1;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleCat2;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleCat3;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleCat4;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleCat5;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleCat6;


@property (strong, nonatomic) IBOutlet UILabel *mlblCat1;
@property (strong, nonatomic) IBOutlet UILabel *mlblCat2;
@property (strong, nonatomic) IBOutlet UILabel *mlblCat3;
@property (strong, nonatomic) IBOutlet UILabel *mlblCat4;
@property (strong, nonatomic) IBOutlet UILabel *mlblCat5;
@property (strong, nonatomic) IBOutlet UILabel *mlblCat6;

- (IBAction)btnSelectImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTitleSelectImg;

@property (strong, nonatomic) IBOutlet UIImageView *mImageNotSetPic;





@end
