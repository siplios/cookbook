//
//  Recipe.h
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 01/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject
@property (strong, nonatomic) NSString *recipe_Name;
@property (strong, nonatomic) NSString *category_Name;
@end
