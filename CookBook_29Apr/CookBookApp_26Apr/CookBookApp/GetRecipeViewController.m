//
//  GetRecipeViewController.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 31/03/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "GetRecipeViewController.h"
#import "AppDelegate.h"
#import "Recipe.h"
#import "IngredientData.h"
AppDelegate *appdel;
Recipe *obj_offer;

@interface GetRecipeViewController ()

@end

@implementation GetRecipeViewController
@synthesize mlblUserName;
@synthesize mTitleAry;
@synthesize currentElementValue;
@synthesize mTitleAryFinal;
@synthesize mRecipeTitleAry;
@synthesize mTxtSearch;
@synthesize mSearchStr;
@synthesize mFilterAry;
@synthesize mTableSearch;
@synthesize mlblSearchCell;
@synthesize mselectedRecipe;
@synthesize aDic;
@synthesize mRecipeAry;
@synthesize ingredientDic;
@synthesize mSpinnerSearch;
@synthesize mOnlyCategoryDic;
@synthesize mcategorisedDic;
@synthesize mRecipeTableAry;
@synthesize mAllIngriAry;
@synthesize mUserImg;
@synthesize btnTitleCat1;
@synthesize btnTitleCat2;
@synthesize btnTitleCat3;
@synthesize btnTitleCat4;
@synthesize btnTitleCat5;
@synthesize btnTitleCat6;
@synthesize mlblCat1;
@synthesize mlblCat2;
@synthesize mlblCat4;
@synthesize mlblCat3;
@synthesize mlblCat5;
@synthesize mlblCat6;
@synthesize mlblLoading;
@synthesize btnTitleAssistant;
@synthesize btnTitleSelectImg;
@synthesize mImageNotSetPic;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    searchTap=0;
    mImageNotSetPic.hidden=TRUE;
    
    appdel=[[UIApplication sharedApplication] delegate];
    mTitleAry=[[NSMutableArray alloc] init];
    mRecipeAry=[[NSMutableArray alloc] init];
    mRecipeTitleAry=[[NSMutableArray alloc] init];
    mTitleAryFinal=[[NSMutableArray alloc] init];
    mSearchStr=[[NSString alloc] init];
    mFilterAry=[[NSMutableArray alloc] init];
    mselectedRecipe=[[NSString alloc] init];
    mcategorisedDic=[[NSMutableDictionary alloc] init];
    mRecipeTableAry=[[NSMutableArray alloc] initWithObjects:@"Arrollado de Atún",@"Canastitas Caprese",@"Cheesecake",@"Carne a la Strogonoff",@"Anillo de 3 Citrus",@"Vitel Thone de Pollo",@"Empanadas agridulces de pollo",@"Brownie de café con lunares de chocolate",@"Tarta Cuatro Quesos",@"Torta golosa de chocolate y crema",@"Arrollado de verduras",@"Budín de bananas",@"Delicias de Durazno",@"Empanadas con anudado liso",@"Budín de manzana y canela para la hora del té",@"Dulzuras de Coco",@"Nachos con guacamole",@"Cookies de coco para la merienda",@"Manzana crocante",@"Strudel de Papas",@"Canastitas de atún",@"Cookies Dos Chocolates",@"Torta Delicia de Chocolate Nevado y Banana",@"Canastitas de Pollo y Panceta",@"Canastitas de Salmón",@"Canastitas de zanahoria",@"Empanadas de hongos y provolone",@"Empanadas de ricotta y espinaca",@"Galletitas de queso azul",@"Mini empanadas árabes",@"Empanadas con repulgue piquitos",@"Repulgue para Rolls",@"Rolls Orientales",@"Cupcakes Banana Split",@"Cupcakes ChipChoco",@"Cupcakes de almendra",@"Cupcakes Tentaciones Rojas",@"Mini budines de chocolate",@"Tarta de ricotta y miel",@"Tentaciones de banana",@"Torta fácil de cumpleaños",@"Ensalada con aderezo de Salmón",@"Aderezo del Mediterráneo",@"Calzone napolitano",@"Carne a la BBQ",@"Agnolotis con Crema de Parmesano",@"Lasagna casera",@"Sorrentinos mediterráneos",@"Tarta acaramelada de cebolla y calabaza",@"Tarta de atún a la crema",@"Tarta de Calabaza",@"Tarta de cebolla y rúcula",@"Tarta de Champignon y Espinacas",@"Tarta de Puerros, Jamón y Champignón",@"Tarta de Salmón",@"Tarta rústica de vegetales",@"Tarta enrejada de calabaza y zapallitos",@"Bocaditos de Queso al Verdeo",@"Chipá al oreganato",@"Pan de 3 Semillas",@"Cupcakes de Brownie",@"Cupcakes de Dulce de Leche",@"Muffins de Vainilla",@"Blinis",@"Budín de arroz colorido",@"Costillas de cerdo en salsa Barbacoa",@"Discos nutritivos",@"Galletas",@"Lomo marinado",@"Pan de Campo",@"Pan Multicereal",@"Papas rellenas a la criolla",@"Pasta a las hierbas",@"Revuelto nutritivo",@"Salteado de Pollo y Pimientos",@"Salteado de Verduras",@"Supremas rellenas con Panceta",@"Chicken Pie",@"Pudding de vegetales",@"Cazuela rápida de humita",@"Tarta de brócoli y queso",@"Solomillo a la pimienta",@"Colita de cuadril a las hierbas",@"Budín de espinaca y morrones",@"Budín de chocolate",@"Torrejas de miel",@"Omelette de espinaca y queso",@"Manzanas acarameladas con helado de crema",@"Salteado de pollo y champignones",@"Cebollas asadas al pimentón",@"Bifes a la criolla",@"Noquis de sémola a las hierbas",@"Papines al verdeo",@"Pasta Frola a la Margarina",@"Apio al Roquefort",@"Baguetín con Pavita y Naranja",@"Canastitas de Zucchinis y Pistachos",@"Canelones de Atún",@"Colita de Cuadril a la Naranja",@"Crepes",@"Crepes de Salmón y Marisco",@"Cupcakes de Zanahoria y Nueces",@"Empanadas Argentinas",@"Empanadas de Pollo al Curry",@"Lasagna de Berenjenas y Atún",@"Lasagna de Pollo",@"Masa de Pizza Casera",@"Pan de Especias",@"Pan Lactal",@"Pastel de Choclo Criollo",@"Pescado a la Crema",@"Pizza Carbonara",@"Pollo al Limón con Papas Hervidas",@"Pollo de Campo con Manteca de Hierbas",@"Pollo Mareado",@"Risotto con Tomate y Albahaca",@"Roulé de Espinacas y Champignones",@"Sancocho Colombiano",@"Sandwich de Lomo con Pan de Mostaza y Eneldo",@"Tarte Tatin de Tomates",@"Trucha a la Margarina Negra", nil];
    
    
    NSLog(@"mRecipeTableAry count %d",[mRecipeTableAry count]);
    
    if([appdel.mLoginUserName length]==0)
    {
        mlblUserName.text=[NSString stringWithFormat:@"Bienvenido invitado"];
        [btnTitleAssistant setImage:[UIImage imageNamed:@"help_disable.png"] forState:UIControlStateNormal];
        UIImage *image = [UIImage imageNamed: @"pic.png"];
        [mUserImg setImage:image];
    }
    else
    {
        [self methodSelectFromTable];
        NSLog(@"the img path:%@",appdel.mImagePath);
        if([appdel.mImagePath isEqualToString:@""])
        {
            NSLog(@"yes nil");
            UIImage *image = [UIImage imageNamed:@"pic.png"];
           // [mUserImg setImage:image];
             mUserImg.image = image;
            mImageNotSetPic.hidden=FALSE;
        }
       //  mImageNotSetPic.hidden=TRUE;
        UIImage *graphImage = [[UIImage alloc] initWithContentsOfFile: appdel.mImagePath];
        mUserImg.image = graphImage;
    [btnTitleAssistant setImage:[UIImage imageNamed:@"help.png"] forState:UIControlStateNormal];
         mlblUserName.text=[NSString stringWithFormat:@"Bienvenido %@",appdel.mLoginUserName];
    }
   	// Do any additional setup after loading the view.
    mTableSearch.hidden=TRUE;
    
    //get recipe xml data to parse
    
//    [self getRecipeDetails];
//    [self methodXMLParse];
    
    NSLog(@"mTitleAry count is >>%d",[mTitleAry count]);
    // NSLog(@"mRecipeTitleAry is >>%@",mRecipeTitleAry);
    
    for(int i=0;i<[mTitleAry count];i++)
    {
        NSLog(@"1st mTitleAry is >>\n%@",[mTitleAry objectAtIndex:i]);
        NSString *myStr=[mTitleAry objectAtIndex:i];
        NSArray *mAry=[myStr componentsSeparatedByString:@"\n"];
        NSLog(@"1st line of mAry is >>%@",[mAry objectAtIndex:0]);
        [mTitleAryFinal addObject:[mAry objectAtIndex:0]];
    }
    NSLog(@"mTitleAryFinal is >>%@", mTitleAryFinal);
    NSLog(@"mTitleAryFinal count is >>%d", [mTitleAryFinal count]);
    
   // NSLog(@"adic  is >>%@", aDic);
    
    NSLog(@"appdel.mAllRecipeAry is >>%@",appdel.mAllRecipeAry);
    
    NSLog(@"appdel.mCategoryAry is >>%@ and count >>%d",appdel.mCategoryAry,[appdel.mCategoryAry count]);
    mlblLoading.hidden=TRUE;
//    for(int i=0;i<[appdel.mCategoryAry count];i++)
//    {
//        if([[appdel.mCategoryAry valueForKey:@"Category_Id"] containsObject:[[appdel.mCategoryAry objectAtIndex:i] valueForKey:@"Category_Id"]])
//        {
//            NSLog(@"yes category contains");
//        }
//        else
//        {
//            NSLog(@"category not contains");
//        }
//    }
    
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    filePathImg = [NSString stringWithFormat:@"%@/savedImage.png", docDirectory];
    NSLog(@"filePathImg in viewdidload >>%@",filePathImg);
}
-(void)getRecipeDetails
{
    NSString *filePathSecond = [[NSBundle mainBundle] pathForResource:@"recipe" ofType:@"xml"];
    NSLog(@"filePathSecond is >>%@",filePathSecond);
    textData = [NSString stringWithContentsOfFile:filePathSecond encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"Some text from a file of %@", textData);
    
    NSLog(@"The length of the string is %lu", (unsigned long)[textData length]);
   mData  = [textData dataUsingEncoding:NSUTF8StringEncoding];
}

-(void)getRetvalData
{
    NSString *filePathSecond = [[NSBundle mainBundle] pathForResource:@"recipe" ofType:@"xml"];
    NSLog(@"filePathSecond is >>%@",filePathSecond);
    textData = [NSString stringWithContentsOfFile:filePathSecond encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"The length of the string is %lu", (unsigned long)[textData length]);
    mData  = [textData dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *retVal=[[NSString alloc]initWithData:mData encoding:NSUTF8StringEncoding];
    retVal = [retVal stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"retval is >>%@",retVal);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)methodXMLParse
{
    xmlParser = [[NSXMLParser alloc] initWithData: mData];
	[xmlParser setDelegate: self];
	[xmlParser setShouldResolveExternalEntities: YES];
	[xmlParser parse];
}
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
   attributes: (NSDictionary *)attributeDict
{
    resultElement=elementName;
    
     if([resultElement isEqualToString:@"recipe"])
     {
         NSLog(@"recipe found didStartElement");
         aDic=[[NSMutableDictionary alloc] init];
         mRecipeTitleString=[[NSMutableString alloc] init];
         recipeId=[[NSMutableString alloc] init];
         mRecipeLastUpdate=[[NSMutableString alloc] init];
         mRecipeShortDes=[[NSMutableString alloc] init];
         mRecipeLongDes=[[NSMutableString alloc] init];
         mRecipeSmallImg=[[NSMutableString alloc] init];
         mRecipeBigImg=[[NSMutableString alloc] init];
         mRecipeIngredient=[[NSMutableString alloc] init];
          mRecipeQuantity=[[NSMutableString alloc] init];
         mRecipeUnits=[[NSMutableString alloc] init];
         mRecipeIcon=[[NSMutableString alloc] init];
         mRecipeStep=[[NSMutableString alloc] init];
         mRecipeCategoryId=[[NSMutableString alloc] init];
         mAllIngriAry=[[NSMutableArray alloc] init];
         self.arrIngrediants = [[NSMutableArray alloc]init];
         mRecipeTotalMin=[[NSMutableString alloc] init];
         mRecipeDefaultPortion=[[NSMutableString alloc] init];
         
         mIngredient_Name=[[NSMutableArray alloc] init];
         dicIng_Name=[[NSMutableDictionary alloc] init];
         ref=0;
        
     }
    if([resultElement isEqualToString:@"category"])
    {
        NSLog(@"category found didStartElement");
        mCategoryId=[[NSMutableString alloc] init];
        mOnlyCategoryDic=[[NSMutableDictionary alloc] init];
        mCategoryVersion=[[NSMutableString alloc] init];
        mCategoryTitle=[[NSMutableString alloc] init];
         mCategoryShortDes=[[NSMutableString alloc] init];
        mCategoryLongDes=[[NSMutableString alloc] init];
         mCategoryImage=[[NSMutableString alloc] init];
        ref1=0;
    }
    if ([elementName isEqualToString:@"ingredients"]) {
        isingredients = YES;
    }
    else if ([elementName isEqualToString:@"ingredient"]){
//        NSDictionary *dic = [[NSDictionary alloc]init];
//        [dic setValue:[attributeDict objectForKey:@"ingredientName"] forKey:@"name"];
//        [dic setValue:[attributeDict objectForKey:@"quantity"] forKey:@"quantity"];
//        [dic setValue:[attributeDict objectForKey:@"units"] forKey:@"units"];
//        [dic setValue:[attributeDict objectForKey:@"icon"] forKey:@"icon"];
//        [dic setValue:[attributeDict objectForKey:@"url"] forKey:@"url"];
//        
//        [mAllIngriAry addObject:dic];
        
        
//
        self.data = [[IngredientData alloc]init];
    }
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(ref==0)
    {
         if([resultElement isEqualToString:@"title"])
         {
            resultElement=nil;
             [mRecipeTitleString appendString:string];
         }
        if([resultElement isEqualToString:@"ingredientName"])
        {
            resultElement=@"";
           // [mRecipeTitleString appendString:string];
            self.data.stringredientName=string;
        }
        if([resultElement isEqualToString:@"quantity"])
        {
            resultElement=@"";
            // [mRecipeTitleString appendString:string];
            self.data.stringredientQuantity=string;
        }

        if([resultElement isEqualToString:@"units"])
        {
            resultElement=@"";
            // [mRecipeTitleString appendString:string];
            self.data.stringredientUnits=string;
        }

        if([resultElement isEqualToString:@"icon"])
        {
            resultElement=@"";
            // [mRecipeTitleString appendString:string];
            self.data.stringredientIcon=string;
        }
        if([resultElement isEqualToString:@"url"])
        {
            resultElement=@"";
            // [mRecipeTitleString appendString:string];
            self.data.stringredientUrl=string;
        }


        if([resultElement isEqualToString:@"recipeId"])
        {
           
             resultElement=@"";
            [recipeId appendString:string];
        }
        if([resultElement isEqualToString:@"lastUpdate"])
        {
          
             resultElement=@"";
            [mRecipeLastUpdate appendString:string];
        }
        if([resultElement isEqualToString:@"shortDescription"])
        {
             resultElement=@"";
            [mRecipeShortDes appendString:string];
        }
        if([resultElement isEqualToString:@"longDescription"])
        {
           
            [mRecipeLongDes appendString:string];
        }
        if([resultElement isEqualToString:@"smallImageURL"])
        {
           
             resultElement=@"";
            [mRecipeSmallImg appendString:string];
        }
        if([resultElement isEqualToString:@"bigImageURL"])
        {
           
             resultElement=@"";
            [mRecipeBigImg appendString:string];
        }
        if([resultElement isEqualToString:@"ingredientName"])
        {
           
             resultElement=@"";
            [mRecipeIngredient appendString:string];
        }
        if([resultElement isEqualToString:@"quantity"])
        {
             resultElement=@"";
           
            [mRecipeQuantity appendString:string];
        }
        if([resultElement isEqualToString:@"units"])
        {
             resultElement=@"";
            [mRecipeUnits appendString:string];
        }
        if([resultElement isEqualToString:@"icon"])
        {
             resultElement=@"";
            [mRecipeIcon appendString:string];
        }
        if([resultElement isEqualToString:@"step"])
        {
             resultElement=@"";
            [mRecipeStep appendString:string];
        }
        if([resultElement isEqualToString:@"categoryId"])
        {
           resultElement=@"";
            [mRecipeCategoryId appendString:string];
        }
        if([resultElement isEqualToString:@"totalMinutes"])
        { resultElement=@"";
           
            [mRecipeTotalMin appendString:string];
        }
        if([resultElement isEqualToString:@"defaultPortions"])
        {
            resultElement=@"";
            [mRecipeDefaultPortion appendString:string];
        }
    }
    if(ref1==0)
    {
        if([resultElement isEqualToString:@"categoryId"])
        {
            NSLog(@"foundcharater categoryId in ref1");
            [mCategoryId appendString:string];
        }
        if([resultElement isEqualToString:@"version"])
        {
            NSLog(@"foundcharater version in ref1");
            [mCategoryVersion appendString:string];
        }
        if([resultElement isEqualToString:@"title"])
        {
           
            [mCategoryTitle appendString:string];
             NSLog(@"foundcharater title in ref1...%@",mCategoryTitle);
        }
        if([resultElement isEqualToString:@"shortDescription"])
        {
            [mCategoryShortDes appendString:string];
        }
        if([resultElement isEqualToString:@"longDescription"])
        {
            [mCategoryLongDes appendString:string];
        }
        if([resultElement isEqualToString:@"image"])
        {
            [mCategoryImage appendString:string];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    if(ref==0)
    {
        if ([elementName isEqualToString:@"ingredient"]) {
            //isingredients=NO;
            [self.arrIngrediants addObject:self.data];
            NSLog(@"Id Array count is :%d",[self.arrIngrediants count]);
             NSLog(@"Id Array count is :%@",self.data);
        }
//        if ([elementName isEqualToString:@"ingredients"]) {
//            //isingredients=NO;
//            [mAllIngriAry addObject:self.data];
//           
//        }
       if([elementName isEqualToString:@"categoryId"])
       {
           NSLog(@"END category............");
           NSLog(@"mRecipeTitleString in didEndElement is>>%@",mRecipeTitleString);
           NSArray *ary=[mRecipeTitleString componentsSeparatedByString:@"\n"];
           [mTitleAry addObject:mRecipeTitleString];
           [aDic setObject:[ary objectAtIndex:0] forKey:@"Rec_Title"];
           NSArray *mID=[recipeId componentsSeparatedByString:@"\n"];
           [aDic setObject:[mID objectAtIndex:0] forKey:@"Rec_ID"];
           [aDic setObject:mRecipeLastUpdate forKey:@"Rec_LastUpdate"];
           [aDic setObject:mRecipeShortDes forKey:@"Rec_ShortDes"];
           [aDic setObject:mRecipeLongDes forKey:@"Rec_LongDes"];
           [aDic setObject:mRecipeSmallImg forKey:@"Rec_SmallImgURL"];
           [aDic setObject:mRecipeBigImg forKey:@"Rec_BigImgURL"];
            NSLog(@"didend mRecipeIngredienty  \n>>%@",mRecipeIngredient);
           NSString *str;
           str=[mRecipeIngredient stringByReplacingOccurrencesOfString:@"\n" withString:@"+"];
           NSLog(@"str is >>%@",str);
           
           NSArray *ingAry=[str componentsSeparatedByString:@"+                                "];
           NSLog(@"ingAry is >>%@",ingAry);
           NSMutableArray *mIngAry=[[NSMutableArray alloc] init];
           [mIngAry addObjectsFromArray:ingAry];
           
           [mIngAry removeLastObject];
           ingredientDic =[[NSMutableDictionary alloc] init];
//           for(int i=0;i<[mIngAry count];i++)
//                          {
//                               NSLog(@"ingAry objectatindex is >>%@",[mIngAry objectAtIndex:i]);
//                              [ingredientDic setObject:[mIngAry objectAtIndex:i] forKey:@"ingredi"];
//                              
//                              [aDic setObject:[mIngAry objectAtIndex:i] forKey:[NSString stringWithFormat:@"ingredient%d",i]];
//                          }
          //  [aDic setObject:mIngAry forKey:[NSString stringWithFormat:@"ingredient_name"]];
//           NSLog(@"ingredientDic dic is %@",ingredientDic);
//           
//           NSLog(@"didend mRecipeQuantity  \n>>%@",mRecipeQuantity);
//           NSString *str1;
//           str1=[mRecipeQuantity stringByReplacingOccurrencesOfString:@"\n" withString:@"+"];
//           NSLog(@"str1 is >>%@",str1);
//           NSArray *ingQtyAry=[str1 componentsSeparatedByString:@"+                                "];
//           NSLog(@"ingQtyAry is >>%@",ingQtyAry);
//           for(int i=0;i<[ingQtyAry count];i++)
//           {
//               [aDic setObject:[ingQtyAry objectAtIndex:i] forKey:[NSString stringWithFormat:@"ing_Qty_%d",i]];
//           }
          //  [aDic setObject:ingQtyAry forKey:[NSString stringWithFormat:@"ing_Qty"]];
           
          // NSLog(@"didend mRecipeUnits  >>\n%@",mRecipeUnits);
         //  NSString *strUnit=[[NSString alloc] init];
          // strUnit=[mRecipeUnits stringByReplacingOccurrencesOfString:@"\n" withString:@"+"];
          // NSLog(@"strUnit is >>%@",strUnit);
         //  NSArray *ingUnitAry=[strUnit componentsSeparatedByString:@"+                                "];
          // NSLog(@"ingUnitAry is >>%@",ingUnitAry);
//           for(int i=0;i<[ingUnitAry count];i++)
//           {
//               [aDic setObject:[ingUnitAry objectAtIndex:i] forKey:[NSString stringWithFormat:@"ing_Unit_%d",i]];
//           }
           // [aDic setObject:ingUnitAry forKey:[NSString stringWithFormat:@"ing_Unit"]];
           
          // NSLog(@"didend mRecipeIcon  >>\n%@",mRecipeIcon);
          // NSString *strIcon;
          // strIcon=[mRecipeIcon stringByReplacingOccurrencesOfString:@"\n" withString:@"+"];
         //  NSLog(@"strIcon is >>%@",strIcon);
          // NSArray *ingIconAry=[strIcon componentsSeparatedByString:@"+                                "];
          // NSLog(@"ingIconAry is >>%@",mIngAry);
//           for(int i=0;i<[ingIconAry count];i++)
//           {
//               [aDic setObject:[ingIconAry objectAtIndex:i] forKey:[NSString stringWithFormat:@"ing_Icon_%d",i]];
//           }
           // [aDic setObject:ingIconAry forKey:[NSString stringWithFormat:@"ing_Icon"]];
           
           
//           NSLog(@"mIngAry count = %d ingQtyAry=%d ingUnitAry=%d,ingIconAry=%d " ,[mIngAry count],[ingQtyAry count],[ingUnitAry count],[ingIconAry count] );
          for(int i=0;i<[self.arrIngrediants count];i++)
           {
               NSMutableDictionary *aIngDic=[[NSMutableDictionary alloc] init];
               IngredientData *objIngrediants = [self.arrIngrediants objectAtIndex:i];
               [aIngDic setObject:objIngrediants.stringredientName forKey:@"dicIng_name"];
               [aIngDic setObject:objIngrediants.stringredientQuantity forKey:@"dicIng_Qty"];
              [aIngDic setObject:objIngrediants.stringredientUnits forKey:@"dicIng_Unit"];
               [aIngDic setObject:objIngrediants.stringredientIcon forKey:@"dicIng_Icon"];
               [aIngDic setObject:objIngrediants.stringredientUrl forKey:@"url"];
               //NSLog(@"aIngDic is >>%@",aIngDic);
               [mAllIngriAry addObject:aIngDic];
           }
         
          [aDic setObject:mAllIngriAry forKey:@"Rec_ingredients"];

           
         //  NSLog(@"mrecipe step >>%@",mRecipeStep);
           NSString *strStep=[[NSString alloc] init];
           strStep=[mRecipeStep stringByReplacingOccurrencesOfString:@"\n" withString:@"+"];
           NSLog(@"strStep is >>%@",strStep);
         //  NSArray *ingStepAry=[strStep componentsSeparatedByString:@"+                            "];
           
           
           
           NSString *strStepComma=[strStep stringByReplacingOccurrencesOfString:@"+                            " withString:@","];
           NSLog(@"strStepComma is >>%@",strStepComma);
           NSString *mexceptComma=[strStepComma substringFromIndex:1];
           NSArray *stepAry=[mexceptComma componentsSeparatedByString:@","];
           NSLog(@"stepAry is >>%@",stepAry);
//           for(int i=0;i<[stepAry count];i++)
//           {
//               [aDic setObject:[stepAry objectAtIndex:i] forKey:[NSString stringWithFormat:@"step_%d",i]];
//           }
            [aDic setObject:stepAry forKey:[NSString stringWithFormat:@"Recipe_steps"]];

           NSLog(@"category id in didend >>%@",mRecipeCategoryId);
           NSArray *categoId=[mRecipeCategoryId componentsSeparatedByString:@"\n"];
           [aDic setObject:[categoId objectAtIndex:0] forKey:@"Rec_CategoryId"];
           
           NSLog(@"total min didend %@",mRecipeTotalMin);
           [aDic setObject:mRecipeTotalMin forKey:@"Rec_TotalMinutes"];
           NSLog(@"total min default portion %@",mRecipeDefaultPortion);
           [aDic setObject:mRecipeDefaultPortion forKey:@"Rec_DefaultPortion"];
           
//           [mRecipeAry addObject:aDic];
          if([[appdel.mAllRecipeAry valueForKey:@"Rec_Title"] containsObject:[aDic objectForKey:@"Rec_Title"]])
          {
              NSLog(@"rec title contains");
          }
           else
           {
                NSLog(@"rec title not contains");
                [appdel.mAllRecipeAry addObject:aDic];
           }
          
       }
    }
    if(ref1==0)
    {
        if([elementName isEqualToString:@"category"])
        {
           NSLog(@"END only category............");
            NSArray *arycategory=[mCategoryId componentsSeparatedByString:@"\n"];
            [mOnlyCategoryDic setObject:[arycategory objectAtIndex:0] forKey:@"Category_Id"];
            NSArray *aryVersion=[mCategoryVersion componentsSeparatedByString:@"\n"];
            [mOnlyCategoryDic setObject:[aryVersion objectAtIndex:0] forKey:@"Category_Version"];
            NSLog(@"mCategoryTitle is >>%@",mCategoryTitle);
            NSArray *ary=[mCategoryTitle componentsSeparatedByString:@"\n"];
            [mOnlyCategoryDic setObject:[ary objectAtIndex:0] forKey:@"Category_Title"];
            
             NSArray *aryshoetDes=[mCategoryShortDes componentsSeparatedByString:@"\n"];
            [mOnlyCategoryDic setObject:[aryshoetDes objectAtIndex:0] forKey:@"Category_ShortDes"];
            
            NSArray *aryLongDes=[mCategoryLongDes componentsSeparatedByString:@"\n"];
            [mOnlyCategoryDic setObject:[aryLongDes objectAtIndex:0] forKey:@"Category_LongDes"];
            
             NSArray *aryImage=[mCategoryImage componentsSeparatedByString:@"\n"];
            [mOnlyCategoryDic setObject:[aryImage objectAtIndex:0] forKey:@"Category_Image"];
            
                if([[appdel.mCategoryAry valueForKey:@"Category_Id"] containsObject:[mOnlyCategoryDic objectForKey:@"Category_Id"]])
                {
                    NSLog(@"yes category contains");
                }
                else
                {
                    NSLog(@"category not contains");
                    [appdel.mCategoryAry addObject:mOnlyCategoryDic];
                }
            [mSpinnerSearch stopAnimating];
            btnTitleCat1.hidden=FALSE;
            btnTitleCat2.hidden=FALSE;
            btnTitleCat3.hidden=FALSE;
            btnTitleCat4.hidden=FALSE;
            btnTitleCat5.hidden=FALSE;
            btnTitleCat6.hidden=FALSE;
            
            mlblCat1.hidden=FALSE;
            mlblCat2.hidden=FALSE;
            mlblCat3.hidden=FALSE;
            mlblCat4.hidden=FALSE;
            mlblCat5.hidden=FALSE;
            mlblCat6.hidden=FALSE;
             mlblLoading.hidden=TRUE;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(mTxtSearch.text.length==2)
    {
        [mFilterAry removeAllObjects];
        mSearchStr=mTxtSearch.text;
        NSLog(@"the search text %@",mSearchStr);
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] %@",mSearchStr];
        NSArray *results = [mRecipeTableAry filteredArrayUsingPredicate:predicate];
        [mFilterAry addObjectsFromArray:results];
        NSLog(@"mfilter array is >>%@",mFilterAry);
        [mTableSearch reloadData];
        mTableSearch.hidden=FALSE;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
        }
        else
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] ==7.0)
            {
                NSLog(@"yes 7.0 predict table");
                
                if([[UIScreen mainScreen] bounds].size.height == 568.0)
                {
                    NSLog(@"screen size 568 shouldChangeCharactersInRange");
                    mTableSearch.frame=CGRectMake(104, 269, 158, 98);
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 480.0)
                {
                    NSLog(@"screen size 480 shouldChangeCharactersInRange");
                   mTableSearch.frame=CGRectMake(104, 231, 158, 98);
                }
            }
            else if ([[[UIDevice currentDevice] systemVersion] floatValue] >=6.0)
            {
                
            }
        }

        
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    
    
    [textField resignFirstResponder];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [mFilterAry count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        NSLog(@"when cell is nil....");
        //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"View" owner:self options:nil];
        cell = (UITableViewCell *)[nib objectAtIndex:0];
    }
    mlblSearchCell.text=[mFilterAry objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    mTxtSearch.text=[mFilterAry objectAtIndex:indexPath.row];
    mselectedRecipe=[mFilterAry objectAtIndex:indexPath.row];
    NSLog(@"mselectedRecipe is >>%@",mselectedRecipe);
    mTableSearch.hidden=TRUE;
}
-(void)methodCategorySearch
{
    [self getRecipeDetails];
    [self methodXMLParse];
}
-(void)viewWillAppear:(BOOL)animated
{
    btnTitleCat1.hidden=FALSE;
    btnTitleCat2.hidden=FALSE;
    btnTitleCat3.hidden=FALSE;
    btnTitleCat4.hidden=FALSE;
    btnTitleCat5.hidden=FALSE;
    btnTitleCat6.hidden=FALSE;
    
    mlblCat1.hidden=FALSE;
    mlblCat2.hidden=FALSE;
    mlblCat3.hidden=FALSE;
    mlblCat4.hidden=FALSE;
    mlblCat5.hidden=FALSE;
    mlblCat6.hidden=FALSE;
    
    mlblLoading.hidden=TRUE;
    [mSpinnerSearch stopAnimating];
}

-(void)methodAllHidden
{
    btnTitleCat1.hidden=TRUE;
    btnTitleCat2.hidden=TRUE;
    btnTitleCat3.hidden=TRUE;
    btnTitleCat4.hidden=TRUE;
    btnTitleCat5.hidden=TRUE;
    btnTitleCat6.hidden=TRUE;
    mlblCat1.hidden=TRUE;
    mlblCat2.hidden=TRUE;
    mlblCat3.hidden=TRUE;
    mlblCat4.hidden=TRUE;
    mlblCat5.hidden=TRUE;
    mlblCat6.hidden=TRUE;
}
-(void)methodSearchCat1
{
    NSLog(@"in methodSearchCat1");
    if(searchTap==0)
    {
        [self getRecipeDetails];
        [self methodXMLParse];
        searchTap=1;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    
    //    if(searchTap==0)
    //    {
    ////        [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(methodCategorySearch) userInfo:nil repeats:NO];
    //        [self getRecipeDetails];
    //        [self methodXMLParse];
    //        [mSpinnerSearch startAnimating];
    //        searchTap=1;
    //    }
    
    appdel.valCategory=@"1";
    NSLog(@"mID array is >>%@ and count>>%d",appdel.mIDarray,[appdel.mIDarray count]);
    appdel.whichSearch=@"categorySearch";
    NSLog(@"appdel.mAllRecipeAry is in catbtn 1 >>%@",appdel.mAllRecipeAry);
}


- (IBAction)btnCategory1:(id)sender {
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(methodSearchCat1) userInfo:nil repeats:NO];
    //   [self performSelectorInBackground:@selector(methodSearch) withObject:self];
    [mSpinnerSearch startAnimating];
    mlblLoading.hidden=FALSE;
    NSLog(@"btn cat 1");
    appdel.whichBtnClicked=@"btnCategory";
    [self methodAllHidden];
}
-(void)methodSearchCat2
{
    if(searchTap==0)
    {
        [self getRecipeDetails];
        [self methodXMLParse];
        searchTap=1;
    }
    
    NSLog(@"mID array is >>%@ and count>>%d",appdel.mIDarray,[appdel.mIDarray count]);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    appdel.whichSearch=@"categorySearch";
}

- (IBAction)btnCategory2:(id)sender {
     [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(methodSearchCat2) userInfo:nil repeats:NO];
     [mSpinnerSearch startAnimating];
    mlblLoading.hidden=FALSE;
     appdel.valCategory=@"2";
     appdel.whichBtnClicked=@"btnCategory";
     [self methodAllHidden];
}
-(void)methodSearchCat3
{
    if(searchTap==0)
    {
        [self getRecipeDetails];
        [self methodXMLParse];
        searchTap=1;
    }
    
    NSLog(@"mID array is >>%@ and count>>%d",appdel.mIDarray,[appdel.mIDarray count]);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    appdel.whichSearch=@"categorySearch";

}
- (IBAction)btnCategory3:(id)sender {
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(methodSearchCat3) userInfo:nil repeats:NO];
    [mSpinnerSearch startAnimating];
    mlblLoading.hidden=FALSE;
    appdel.valCategory=@"3";
     appdel.whichBtnClicked=@"btnCategory";
    [self methodAllHidden];
}
-(void)methodSearchCat4
{
    if(searchTap==0)
    {
        [self getRecipeDetails];
        [self methodXMLParse];
        searchTap=1;
    }
    
    NSLog(@"mID array is >>%@ and count>>%d",appdel.mIDarray,[appdel.mIDarray count]);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    appdel.whichSearch=@"categorySearch";

}

- (IBAction)btnCategory4:(id)sender {
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(methodSearchCat4) userInfo:nil repeats:NO];
    [mSpinnerSearch startAnimating];
    mlblLoading.hidden=FALSE;
    appdel.valCategory=@"4";
     appdel.whichBtnClicked=@"btnCategory";
     [self methodAllHidden];
}
-(void)methodSearchCat5
{
    if(searchTap==0)
    {
        [self getRecipeDetails];
        [self methodXMLParse];
        searchTap=1;
    }
    
    NSLog(@"mID array is >>%@ and count>>%d",appdel.mIDarray,[appdel.mIDarray count]);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    appdel.whichSearch=@"categorySearch";
 
}

- (IBAction)btnCategory5:(id)sender {
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(methodSearchCat5) userInfo:nil repeats:NO];
    [mSpinnerSearch startAnimating];
    mlblLoading.hidden=FALSE;
    appdel.valCategory=@"5";
     appdel.whichBtnClicked=@"btnCategory";
    [self methodAllHidden];
}
-(void)methodSearchCat6
{
        [self getRecipeDetails];
        [self methodXMLParse];
        searchTap=1;
    appdel.valCategory=@"6";
    NSLog(@"mID array is >>%@ and count>>%d",appdel.mIDarray,[appdel.mIDarray count]);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    appdel.whichSearch=@"categorySearch";
}

- (IBAction)btnCategory6:(id)sender {
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(methodSearchCat6) userInfo:nil repeats:NO];
    [mSpinnerSearch startAnimating];
     mlblLoading.hidden=FALSE;
    NSLog(@"cat 6");
     appdel.whichBtnClicked=@"btnCategory";
    [self methodAllHidden];
}
-(void)methodSearchCat
{
    NSLog(@"in methodSearchCat");
    [self getRecipeDetails];
    [self methodXMLParse];
}


- (IBAction)btnSearchRecipe:(id)sender
{
   
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(methodSearch) userInfo:nil repeats:NO];
 //   [self performSelectorInBackground:@selector(methodSearch) withObject:self];
    [mSpinnerSearch startAnimating];
    appdel.whichSearch=@"keywordSearch";
    [self methodAllHidden];
    mlblLoading.hidden=FALSE;
    
}
-(void)methodSearch
{
    if(searchTap==0)
    {
        [self getRecipeDetails];
        [self methodXMLParse];
        searchTap=1;
    }
    for(int i=0;i<[appdel.mAllRecipeAry count];i++)
    {
        if([[[appdel.mAllRecipeAry objectAtIndex:i] valueForKey:@"Rec_Title"] isEqualToString:[NSString stringWithFormat:@"%@",mselectedRecipe]])
        {
            NSLog(@"yes matched in search button...%d",i);
            appdel.mIndexpath=i;
        }
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
}
-(void)btnSelectSpinner
{
    appdel.whichBtnClicked=@"btnSelect";
    [self getRecipeDetails];
    [self methodXMLParse];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPad" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else
    {
        DetailsRecipeViewController *reg=[[DetailsRecipeViewController alloc] initWithNibName:@"DetailsRecipeViewController~iPhone" bundle:nil];
        [self.navigationController pushViewController:reg animated:YES];
    }
}

- (IBAction)btnSelectedRecipes:(id)sender {
    NSLog(@"select recipe");
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please login to access the page" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(btnSelectSpinner) userInfo:nil repeats:NO];
        //   [self performSelectorInBackground:@selector(methodSearch) withObject:self];
        [self methodAllHidden];
        [mSpinnerSearch startAnimating];
        mlblLoading.hidden=FALSE;
    }
}

- (IBAction)btnAssistant:(id)sender {
    NSLog(@"assistant tap");
    appdel.mFromPage=@"getNotParse";
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"PLease Login to access this page." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            FormViewController *form=[[FormViewController alloc] initWithNibName:@"FormViewController~iPad" bundle:nil];
            [self.navigationController pushViewController:form animated:YES];
        }
        else
        {
            FormViewController *reg=[[FormViewController alloc] initWithNibName:@"FormViewController~iPhone" bundle:nil];
            [self.navigationController pushViewController:reg animated:YES];
        }
    }
}


- (IBAction)btnBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)btnSelectImage:(id)sender {
    NSLog(@"select image");
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please login to set image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }
   
    //Deprecated In IOS6[self presentModalViewController:picker animated:YES];
}
//- (void)imagePickerController:(UIImagePickerController *)picker
//        didFinishPickingImage:(UIImage *)image
//                  editingInfo:(NSDictionary *)editingInfo
//{
//    NSLog(@"in didFinishPickingImage");
//   // imageView.image = image;
//    mUserImg.image=image;
////    [[picker parentViewController] dismissModalViewControllerAnimated:YES];
//    [picker dismissModalViewControllerAnimated: YES];
//}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"in the ....didFinishPickingMediaWithInfo");
    UIImage  *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
    imageData = UIImagePNGRepresentation(image);
    mUserImg.image=image;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
//    savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];
     savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",appdel.mLoginUserName]];
    [imageData writeToFile:savedImagePath atomically:NO];
    NSLog(@"saved path is >>%@",savedImagePath);
    appdel.mImagePath=savedImagePath;
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"username=%@, email=%@, userid=%@ in didFinishPickingMediaWithInfo",appdel.mLoginUserName,appdel.mUserLoggedinEmail,appdel.mLoginId);
   // [self SaveImagesToSql:imageData :savedImagePath];
    [self methodInsertImageData];
    
//    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
//    NSString *docDirectory = [sysPaths objectAtIndex:0];
//   filePathImg = [NSString stringWithFormat:@"%@/savedImage.png", docDirectory];
//    
//    [self LoadImagesFromSql:filePathImg];
    
}
-(void)methodInsertImageData
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    sqlite3_stmt    *statement;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSLog(@"insert in USERLOGINIMAGE");
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO USERLOGINIMAGE (USER_ID,USER_NAME,USER_EMAIL,URLSTRING ) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")",appdel.mLoginId,appdel.mLoginUserName,appdel.mUserLoggedinEmail,savedImagePath];
       
        
        const char *insert_stmt = [insertSQL UTF8String];
         NSLog(@"the insertSQL string is >>%s",insert_stmt);
        sqlite3_prepare_v2(cookLoginDB, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            // status.text = @"Contact added";
            NSLog(@"Contact added USERLOGINIMAGE");
            
        } else {
            // status.text = @"Failed to add contact";
            NSLog(@" Failed to add contact USERLOGINIMAGE");
        }
        sqlite3_finalize(statement);
        sqlite3_close(cookLoginDB);
    }
}

-(void)methodSelectFromTable
{
    NSLog(@"in method select");
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"COOKBOOKAPP.db"]];
    NSLog(@"in select database path USERLOGINIMAGE>>>>%@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        NSLog(@"file not exist");
    }
    else
    {
        NSLog(@"file exist");
    }
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"in select page dbpath ->>%s",dbpath);
    sqlite3_stmt    *statement;
    if (sqlite3_open(dbpath, &cookLoginDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select * from USERLOGINIMAGE where USER_ID=%@",appdel.mLoginId];
        NSLog(@"The Data USERLOGINIMAGE= %@",querySQL);
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"show data char is USERLOGINIMAGE-->>>>%s",query_stmt);
        if (sqlite3_prepare_v2(cookLoginDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"111### USERLOGINIMAGE");
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSLog(@"in row");
                NSLog(@"select at 0 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]);
                NSLog(@"select at 1 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]);
                NSLog(@"select at 2 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]);
                NSLog(@"select at 3 >>%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)]);
                appdel.mImagePath=[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(cookLoginDB);
    
}



@end
