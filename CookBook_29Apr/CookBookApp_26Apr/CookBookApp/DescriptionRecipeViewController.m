//
//  DescriptionRecipeViewController.m
//  CookBookApp
//
//  Created by Sanjay Chaudhary on 07/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import "DescriptionRecipeViewController.h"
#import "AppDelegate.h"
AppDelegate *appdel;

@interface DescriptionRecipeViewController ()

@end

@implementation DescriptionRecipeViewController
@synthesize mlblUserName;
@synthesize mlblRecipeName;
@synthesize mTxtShort;
@synthesize mTableIngredients;
@synthesize mlblTableIngr;
@synthesize mlblQty;
@synthesize mlblRecipeNameStep;
@synthesize mTxtStep;
@synthesize mlblSteps;
@synthesize mTableSteps;
@synthesize mStep;
@synthesize btnIsFav;
@synthesize mlblCounter;
@synthesize timer1;
@synthesize mUserImg;
@synthesize mImgRecipe;
@synthesize mTableIconImg;
@synthesize mIconImg1;
@synthesize mIconImg2;
@synthesize mIconRecAry;
@synthesize theIndexPath;
@synthesize mIcon1Ary;
@synthesize mIcon2Ary;
@synthesize btnTitleAssistant;
@synthesize btnTitleSelection;
@synthesize lblQtyPerson;
@synthesize mImgPicNotSet;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
      self.scrollView.contentSize=CGSizeMake(320, 580);
    mImgPicNotSet.hidden=TRUE;
	// Do any additional setup after loading the view.
    appdel=[[UIApplication sharedApplication] delegate];
    mIconRecAry=[[NSMutableArray alloc] init];
    mStep=[[NSMutableArray alloc] init];
//    mainInt=[[appdel.mDetailDic objectForKey:@"details_TotalMin"] intValue];
    mlblCounter.text=[NSString stringWithFormat:@"%i",[[appdel.mDetailDic objectForKey:@"details_TotalMin"] intValue]];
    
    NSLog(@"name of recipe description >>%@",[appdel.mDetailDic objectForKey:@"details_Title"]);
    if([appdel.mLoginUserName length]==0)
    {
        mlblUserName.text=[NSString stringWithFormat:@"Bienvenido invitado"];
        UIImage *image = [UIImage imageNamed: @"pic.png"];
        [mUserImg setImage:image];
         [btnTitleAssistant setImage:[UIImage imageNamed:@"help_disable.png"] forState:UIControlStateNormal];
    }
    else
    {
        if([appdel.mImagePath isEqualToString:@""])
        {
            NSLog(@"yes nil");
            UIImage *image = [UIImage imageNamed:@"pic.png"];
            // [mUserImg setImage:image];
            mUserImg.image = image;
            mImgPicNotSet.hidden=FALSE;
        }
      //  mImgPicNotSet.hidden=TRUE;
        
        
        UIImage *graphImage = [[UIImage alloc] initWithContentsOfFile: appdel.mImagePath];
        mUserImg.image = graphImage;
        
        mlblUserName.text=[NSString stringWithFormat:@"Bienvenido %@",appdel.mLoginUserName];
        [btnTitleAssistant setImage:[UIImage imageNamed:@"help.png"] forState:UIControlStateNormal];
        NSLog(@"is fav or not string single >>%@",appdel.recipeIsFavSingle);
        if([appdel.recipeIsFav isEqualToString:@"yesInFavList"])
        {
            NSLog(@"fav img is on");
            [btnIsFav setImage:[UIImage imageNamed:@"Fav.png"] forState:UIControlStateNormal];
        }
        if([appdel.recipeIsFavSingle isEqualToString:@"yesInFavListSingle"])
        {
            NSLog(@"fav img is on yesInFavListSingle");
            [btnIsFav setImage:[UIImage imageNamed:@"Fav.png"] forState:UIControlStateNormal];
        }
        else     if([appdel.whichBtnClicked isEqualToString:@"btnSelect"])
        {
            [btnIsFav setImage:[UIImage imageNamed:@"Fav.png"] forState:UIControlStateNormal];
        }
    }
    NSLog(@"description page recipe id >>%@",[appdel.mDetailDic objectForKey:@"details_Id"]);
    UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"recipe_id_%@.jpg",[appdel.mDetailDic objectForKey:@"details_Id"]]];
    [mImgRecipe setImage:image];
    
    lblQtyPerson.text=[appdel.mDetailDic objectForKey:@"details_DefaultPortion"];
    mlblRecipeName.text=[appdel.mDetailDic objectForKey:@"details_Title"];
    mTxtShort.text=[appdel.mDetailDic objectForKey:@"details_ShortDes"];
    NSLog(@"ingredients is >>%@",[appdel.mDetailDic objectForKey:@"details_ingredients"]);
    ary=[appdel.mDetailDic objectForKey:@"details_ingredients"];
   // NSLog(@"ary count %d \n array name 1st>>%@",[ary count],[[ary objectAtIndex:1] valueForKey:@"dicIng_name"]);
   
    NSLog(@"steps is >>%@",[appdel.mDetailDic objectForKey:@"details_Step"]);
    mAry=[appdel.mDetailDic objectForKey:@"details_Step"];
    NSLog(@"mAry is %@",mAry);
     mlblRecipeNameStep.text=[NSString stringWithFormat:@"%@",[mAry objectAtIndex:0]];
    [mStep addObjectsFromArray:mAry];
    [mStep removeObjectAtIndex:0];
    NSLog(@"mstep array %@",mStep);
    
    int myTime=[[appdel.mDetailDic objectForKey:@"details_TotalMin"] intValue];
    int mSecond=myTime*60;
    NSLog(@"total time in second is >%d",mSecond);
    theTimer=mSecond;
    
    
    NSMutableArray *mIconAry=[appdel.mDetailDic objectForKey:@"details_ingredients"];
    NSLog(@"mIconAry is >>%@",mIconAry);
    NSLog(@"mIconAry is count >>%d",[mIconAry count]);
    for(int i=0;i<[mIconAry count];i++)
    {
        NSLog(@"recipe icon name >>%@",[[mIconAry objectAtIndex:i] valueForKey:@"dicIng_Icon"]);
        if([[[mIconAry objectAtIndex:i] valueForKey:@"dicIng_Icon"] length]==0)
        {
            NSLog(@"icon nil");
        }
        else if([[[mIconAry objectAtIndex:i] valueForKey:@"dicIng_Icon"] length]!=0)
        {
             NSLog(@"icon not nil");
            [mIconRecAry addObject:[[mIconAry objectAtIndex:i] valueForKey:@"dicIng_Icon"]];
        }
    }
    NSLog(@"mIconRecAry array is >>%@",mIconRecAry);
    
    int cellno;
    cellno=[mIconRecAry count]%2;
    NSLog(@"icon array count >>%d",cellno);
    if(cellno==0)
    {
        cellRow=[mIconRecAry count]/2;
    }
    else
    {
        int row=cellRow=[mIconRecAry count]/2;
        cellRow=row+1;
    }
    ary1=[[NSArray alloc] init];
    ary2=[[NSArray alloc] init];
//    for(int i=0;i<cellRow;i++)
//    {
//        [ary1 addObject:[mIconRecAry objectAtIndex:i]];
//    }
//    int val=[mIconRecAry count]-cellRow;
//    NSLog(@"val is %d",val);
//    for(int j=cellRow; j<val;j++)
//    {
//        [ary2 addObject:[mIconRecAry objectAtIndex:j]];
//    }
//    NSLog(@"ary1 is >>%@",ary1);
//    NSLog(@"ary2 is >>%@",ary2);
    
    someRange.location = 0;
    someRange.length = [mIconRecAry count] / 2;
    
    ary1 = [mIconRecAry subarrayWithRange:someRange];
    
    someRange.location = someRange.length;
    someRange.length = [mIconRecAry count] - someRange.length;
    
    ary2 = [mIconRecAry subarrayWithRange:someRange];
    
    NSLog(@"ary1 is >>%@",ary1);
    neAry=[ary1 mutableCopy];
    [neAry insertObject:@"" atIndex:0];
    NSLog(@"neAry is >>%@",neAry);
    NSLog(@"ary2 is >>%@",ary2);
    NSArray *myAry=[neAry mutableCopy];
    neFinalAry=[[myAry reverseObjectEnumerator] allObjects];
    [mTableIconImg setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==mTableIngredients)
    {
         return [ary count];
    }
    else if (tableView==mTableSteps)
    {
        return [mStep count];
    }
    else if (tableView==mTableIconImg)
    {
        
        
        
        return cellRow;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if(tableView==mTableIngredients)
    {
    if (cell == nil) {
        NSLog(@"when cell is nil....");
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"custTable" owner:self options:nil];
        cell = (UITableViewCell *)[nib objectAtIndex:0];
    }
    mlblTableIngr.text=[[ary objectAtIndex:indexPath.row] valueForKey:@"dicIng_name"];
    mlblQty.text=[[ary objectAtIndex:indexPath.row] valueForKey:@"dicIng_Qty"];
    }
    else if (tableView==mTableSteps)
    {
        if (cell == nil) {
            NSLog(@"when cell is nil....");
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"custStep" owner:self options:nil];
            cell = (UITableViewCell *)[nib objectAtIndex:0];
        }
        mlblSteps.text=[NSString stringWithFormat:@"%d. %@",indexPath.row+1,[mStep objectAtIndex:indexPath.row]];
    }
    else if (tableView==mTableIconImg)
    {
        if (cell == nil) {
            NSLog(@"when cell is nil....");
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"custIcon" owner:self options:nil];
            cell = (UITableViewCell *)[nib objectAtIndex:0];
        }
        
        NSLog(@"cell for row indexpath %d",[[mIconRecAry objectAtIndex:indexPath.row] intValue]);
        theIndexPath=[[mIconRecAry objectAtIndex:indexPath.row] intValue];
        int secondIndex=theIndexPath+1;
        NSLog(@"theIndexPath=%d and secondIndex=%d",theIndexPath,secondIndex);
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"%@.png",[ary2 objectAtIndex:indexPath.row]]];
        [mIconImg1 setImage:image];
        UIImage *image1 = [UIImage imageNamed: [NSString stringWithFormat:@"%@.png",[neFinalAry objectAtIndex:indexPath.row]]];
        [mIconImg2 setImage:image1];
    }
    return cell;
}

- (IBAction)btnStartTimer:(id)sender {
    NSLog(@"start count");
    mainInt=theTimer;
   // timer1=[NSTimer timerWithTimeInterval:1 target:nil selector:@selector(methodCountdown) userInfo:nil repeats:YES];
    timer1=[NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(methodCountdown) userInfo:nil repeats:YES];
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)methodCountdown
{
    NSLog(@"inmethodcount");
    mainInt =mainInt-1;
   //
    
    
    hour = mainInt / 3600;
    minute = (mainInt % 3600) / 60;
    second = (mainInt %3600) % 60;
   // myCounterLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
    NSLog(@"in the counter time is >>%02d:%02d:%02d",hour,minute,second);
    mlblCounter.text=[NSString stringWithFormat:@"%02d:%02d",minute,second];
    
    
    NSLog(@"counter %i",mainInt);
    nowTimer=mainInt;
    if(mainInt==0)
    {
        [timer1 invalidate];
    }
}


- (IBAction)btnPauseTimer:(id)sender {
    theTimer=mainInt;
    [timer1 invalidate];
}
//Grid View
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (IBAction)btnSelection:(id)sender {
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please login to access the page" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        appdel.whichBtnClicked=@"btnSelect";
        [self.navigationController popViewControllerAnimated:YES];
      // obj=[[GetRecipeViewController alloc] init];
//        [obj getRecipeDetails];
//        [self methodXMLParse];
//        [self.navigationController popToRootViewControllerAnimated:YES];

    }
}
- (IBAction)btnAssistant:(id)sender
{
    NSLog(@"assistant tap");
    
    if([appdel.mLoginUserName length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"PLease Login to access this page." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            FormViewController *form=[[FormViewController alloc] initWithNibName:@"FormViewController~iPad" bundle:nil];
            [self.navigationController pushViewController:form animated:YES];
        }
        else
        {
            FormViewController *reg=[[FormViewController alloc] initWithNibName:@"FormViewController~iPhone" bundle:nil];
            [self.navigationController pushViewController:reg animated:YES];
        }
    }
}
@end
