

#import <UIKit/UIKit.h>

@class GridView;

@protocol GridViewDataSource <NSObject>
@required
- (NSInteger)numberOfCellsInGridView:(GridView *)gridView;
- (NSInteger)numberOfColumnsInGridView:(GridView *)gridView;
- (float)heightOfCellsInGridView:(GridView *)gridView;
- (UIView *)gridView:(GridView *)gridView viewForCellAtIndex:(NSInteger)index;
@end

@protocol GridViewDelegate <NSObject>
@optional
- (void)gridView:(GridView *)gridView userDidSelectCellAtIndex:(NSInteger)index;
@end

@interface GridView : UIView 
@property (nonatomic, retain) IBOutlet id <GridViewDelegate> delegate;
@property (nonatomic, retain) IBOutlet id <GridViewDataSource> dataSource;
- (id)init;
- (void)reloadData;
- (void)selectCell:(id)sender;
- (CGSize)cellSize;
@end
