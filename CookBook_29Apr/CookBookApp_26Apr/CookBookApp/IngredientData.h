//
//  ingredientData.h
//  Danica
//
//  Created by user5 on 25/04/14.
//  Copyright (c) 2014 Supertron Infotech Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IngredientData : NSObject
@property(nonatomic,strong)NSString *stringredientName;
@property(nonatomic,strong)NSString *stringredientIcon;
@property(nonatomic,strong)NSString *stringredientUrl;
@property(nonatomic,strong)NSString *stringredientUnits;
@property(nonatomic,strong)NSString *stringredientQuantity;

@end
